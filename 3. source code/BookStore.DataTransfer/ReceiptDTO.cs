﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class ReceiptDTO
    {
        public int ID { get; set; }
        public int PublisherID { get; set; }
        public int EmployeeID { get; set; }
        public string Description { get; set; }
        public double TotalPrice { get; set; }

        public ReceiptDTO()
        {

        }

        public ReceiptDTO(Receipt receipt)
        {
            ID = receipt.ID;
            PublisherID = receipt.PublisherID;
            EmployeeID = receipt.EmployeeID;
            Description = receipt.Description;
            TotalPrice = receipt.TotalPrice;
        }
    }
}
