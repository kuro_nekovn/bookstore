﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class RateViewDTO
    {
        public int ID { get; set; }
        public string CustomerID { get; set; }
        public string BookID { get; set; }
        public double Rating { get; set; }
        public string Title { get; set; }
    }
}
