﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BookImageDTO
    {
        public int ID { get; set; }
        public int BookID { get; set; }
        public string BookUrl { get; set; }
        public BookImageDTO()
        {

        }
        public BookImageDTO(BookImage book)
        {
            ID = book.ID;
            BookID = book.BookID;
            BookUrl = book.ImageUrl;
        }
    }
  
}
