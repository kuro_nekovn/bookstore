﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BlogDTO
    {
        public int ID { get; set; }
        public string BlogImg { get; set; }
        public string UserCreateName { get; set; }
        public string Sumary { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public BlogDTO()
        {

        }

        public BlogDTO(Blog blog)
        {
            ID = blog.ID;
            BlogImg = blog.BlogImg;
            UserCreateName = blog.Employee.FirstName + " " + blog.Employee.LastName;
            Sumary = blog.Sumary;
            Title = blog.Title;
            Content = blog.Content;
        }
    }
}
