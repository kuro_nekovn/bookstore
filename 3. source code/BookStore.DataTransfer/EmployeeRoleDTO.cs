﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class EmployeeRoleDTO
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImgUrl { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsHr { get; set; }
        public bool IsSaleman { get; set; }
        public bool IsStockKeeper { get; set; }
        public EmployeeRoleDTO()
        {

        }
       
    }
}
