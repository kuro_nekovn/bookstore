﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BookDTO
    {
        public int ID { get; set; }
        public double Price { get; set; }
        public int Total { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }
        public int Status { get; set; }
        public string Sumary { get; set; }
        public string ImgUrl { get; set; }
        public int Author { get; set; }
        public int Publisher { get; set; }
        public string Content { get; set; }
        public int GenreID { get; set; }

        public BookDTO()
        {

        }

        public BookDTO(Book book)
        {
            ID = book.ID;
            Price = book.Price;
            Total = book.Total;
            Rating = book.Rating == null ? 0 : (int)book.Rating;
            Status = book.Status == null ? 0 : (int)book.Status;
            Sumary = book.Sumary;
            ImgUrl = book.ImgUrl;
            Author = book.Author == null ? 0 : (int)book.AuthorID; ;
            Publisher = book.Publisher == null ? 0 : (int)book.PublisherID; ;
            Content = book.Content;            
            Sumary = book.Sumary;
            Name = book.Name;
            GenreID = book.GenreID;
        }
    }
}
