﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class RateShopDTO
    {
        public int ID { get; set; }
        public string CustomerName { get; set; }
        public string BookName { get; set; }
        public double Rating { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public RateShopDTO()
        {

        }
        public RateShopDTO(Rate rate)
        {
            ID = rate.ID;
            BookName = rate.Book.Name;
            CustomerName = rate.Customer.LastName + " " + rate.Customer.FirstName;
            Rating = rate.Rating;
            Title = rate.Title;
            Content = rate.Content;
        }
    }
}
