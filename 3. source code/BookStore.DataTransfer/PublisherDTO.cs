﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class PublisherDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }

        public PublisherDTO(Publisher publisher)
        {
            ID = publisher.ID;
            Name = publisher.Name;
            Address = publisher.Address;
            Phone = publisher.Phone;
            ImgUrl = publisher.ImgUrl;
            DateCreate = publisher.DateCreate;
        }

        public PublisherDTO()
        {
        }
    }


}
