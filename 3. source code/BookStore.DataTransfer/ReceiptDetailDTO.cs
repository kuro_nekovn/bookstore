﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.DataTransfer
{
    public class ReceiptDetailDTO
    {
        public int ReceiptID { get; set; }
        public string BookName { get; set; }
        public int BookID { get; set; }
        public string ImgUrl { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double TotalPrice { get; set; }
        public int Total { get; set; }

        public ReceiptDetailDTO(ReceiptDetail receiptDetail)
        {
            BookID = receiptDetail.BookID;
            BookName = receiptDetail.Book.Name;
            ImgUrl = receiptDetail.Book.ImgUrl;
            ReceiptID = receiptDetail.ReceiptID;
            Price = receiptDetail.UnitPrice;
            Quantity = receiptDetail.Quantity;
            TotalPrice = receiptDetail.TotalPrice;
            Total = receiptDetail.Book.Total;
        }

        public ReceiptDetailDTO()
        {

        }
    }
}