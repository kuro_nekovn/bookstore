﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class RateDTO
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public int BookID { get; set; }
        public double Rating { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }

        public RateDTO()
        {

        }

        public RateDTO(Rate rate)
        {
            ID = rate.ID;
            CustomerID = rate.CustomerID;
            BookID = rate.BookID;
            Rating = rate.Rating;
            Content = rate.Content;
            Title = rate.Title;
        }
    }
}
