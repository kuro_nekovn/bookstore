﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class ReceiptViewDTO
    {
        public int ID { get; set; }
        public string PublisherID { get; set; }
        public string EmployeeID { get; set; }
        public DateTime ReceiptDate { get; set; }
        public double TotalPrice { get; set; }
    }
}
