﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class ComplainViewDTO
    {
        public int ID { get; set; }
        public string EmployeeID { get; set; }
        public string CustomerID { get; set; }
        public int OrderID { get; set; }
        public string Status { get; set; }
    }
}
