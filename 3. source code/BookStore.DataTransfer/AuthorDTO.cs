﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class AuthorDTO
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> Sex { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string Sumary { get; set; }
        public string Content { get; set; }

        public AuthorDTO()
        {

        }

        public AuthorDTO(Author author)
        {
            ID = author.ID;
            FirstName = author.FirstName;
            LastName = author.LastName;
            Phone = author.Phone;
            Address = author.Address;
            ImgUrl = author.ImgUrl;
            Sex = author.Sex;
            Birthday = author.Birthday;
            Content = author.Content;            
            Sumary = author.Sumary;
        }
    }
}
