﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class ItemDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
