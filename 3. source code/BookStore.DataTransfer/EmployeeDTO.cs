﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class EmployeeDTO
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleID { get; set; }
        public string Address { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> Sex { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }


        public EmployeeDTO()
        {

        }

        public EmployeeDTO(Employee employee)
        {
            ID = employee.ID;
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            Phone = employee.Phone;
            Address = employee.Address;
            ImgUrl = employee.ImgUrl;
            Sex = employee.Sex;
            Birthday = employee.Birthday;
            Email = employee.Email;
            Password = employee.Password;
        }
    }
}
