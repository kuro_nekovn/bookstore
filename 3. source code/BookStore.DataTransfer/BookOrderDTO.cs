﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BookOrderDTO
    {
        public int ID { get; set; }
        public double Price { get; set; }
        public int Total { get; set; }
        public int Discount { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }

        public BookOrderDTO()
        {

        }

        public BookOrderDTO(Book book)
        {
            ID = book.ID;
            Price = book.Price;
            Total = book.Total;
            ImgUrl = book.ImgUrl;           
            Name = book.Name;
            Discount = book.Discount;
        }
    }
}
