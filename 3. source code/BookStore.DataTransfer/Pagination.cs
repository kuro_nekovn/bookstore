﻿namespace BookStore.Entities
{
    public class Pagination
    {
        /// <summary>
        /// Total pages
        /// </summary>
        public int TotalItems { get; set; } = 0;

        /// <summary>
        /// Current page
        /// </summary>
        public int CurrentPage { get; set; } = 1;

        /// <summary>
        /// Total items per page
        /// </summary>
        public int PageSize { get; set; } = 5;

        /// <summary>
        /// 
        /// </summary>
        public Pagination()
        {
            TotalItems = 0;
            CurrentPage = 1;
            PageSize = 5;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        public Pagination(int currentPage, int pageSize)
        {
            this.CurrentPage = currentPage > 0 ? currentPage : 1;
            this.PageSize = pageSize > 0 ? pageSize : 10;
        }
    }
}
