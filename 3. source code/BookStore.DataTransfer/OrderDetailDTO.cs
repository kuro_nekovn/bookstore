﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.DataTransfer
{
    public class OrderDetailDTO
    {
        public int OrderID { get; set; }
        public string BookName { get; set; }
        public int BookID { get; set; }
        public string ImgUrl { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double TotalPrice { get; set; }
        public int Discount { get; set; }
        public int Total { get; set; }

        public OrderDetailDTO(OrderDetail orderDetail)
        {
            BookID = orderDetail.BookID;
            BookName = orderDetail.Book.Name;
            ImgUrl = orderDetail.Book.ImgUrl;
            OrderID = orderDetail.OrderID;
            Price = orderDetail.UnitPrice;
            Quantity = orderDetail.Quantity;
            Discount = orderDetail.Discount;
            TotalPrice = orderDetail.TotalPrice == null ? 0 : (double)orderDetail.TotalPrice;
            Total = orderDetail.Book.Total;
        }

        public OrderDetailDTO()
        {

        }
    }
}