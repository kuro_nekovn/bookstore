﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class OrderDTO
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public int EmployeeID { get; set; }
        public string Address { get; set; }
        public double TotalPrice { get; set; }

        public OrderDTO()
        {

        }

        public OrderDTO(Order order)
        {
            ID = order.ID;
            CustomerID = order.CustomerID;
            EmployeeID = order.EmployeeID;
            Address = order.Address;
            TotalPrice = order.TotalPrice == null ? 0 : (double)order.TotalPrice;
        }
    }
}
