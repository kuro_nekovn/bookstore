﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class CustomerDTO
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string ImgUrl { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Nullable<bool> Sex { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public int Point { get; set; }
        public CustomerDTO()
        {

        }

        public CustomerDTO(Customer customer)
        {
            ID = customer.ID;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Phone = customer.Phone;
            Address = customer.Address;
            ImgUrl = customer.ImgUrl;
            Sex = customer.Sex;
            Email = customer.Email;
            Birthday = customer.Birthday;
            Point = customer.Point;
            Password = customer.Password;
        }
    }
}
