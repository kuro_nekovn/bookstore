﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class AdvertisementDTO
    {
        public int ID { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public System.DateTime DateExpire { get; set; }
        public System.DateTime DateRelease { get; set; }
        public string Description { get; set; }

        public AdvertisementDTO()
        {

        }

        public AdvertisementDTO(Advertisement genre)
        {
            ID = genre.ID;
            ImgUrl = genre.ImgUrl;
            DateCreate = genre.DateCreate;
            DateExpire = genre.DateExpire;
            DateRelease = genre.DateRelease;
            Description = genre.Description;
        }
    }
}
