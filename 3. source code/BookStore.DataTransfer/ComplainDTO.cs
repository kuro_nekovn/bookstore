﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class ComplainDTO
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public int OrderID { get; set; }
        public string Status { get; set; }
        public string Content { get; set; }

        public ComplainDTO()
        {

        }

        public ComplainDTO(Complaint complain)
        {
            ID = complain.ID;
            EmployeeID = (int)complain.EmployeeID;
            CustomerID = complain.CustomerID;
            OrderID = (int)complain.OrderID;
            Status = complain.Status;
            Content = complain.Content;
        }
    }
}
