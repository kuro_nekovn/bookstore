﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Entities
{
    public class BlogShopModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string BlogImg { get; set; }
        public string UserCreateName { get; set; }
        public string Sumary { get; set; }
        public DateTime? CreateDate { get; set; }
        public int TotalComment { get; set; }
        public BlogShopModel(Blog blog)
        {
            ID = blog.ID;
            Title = blog.Title;
            BlogImg = blog.BlogImg;
            UserCreateName = blog.Employee.LastName + " " + blog.Employee.FirstName;
            Sumary = blog.Sumary;
            CreateDate = blog.DateCreate;
            TotalComment = blog.BlogReviews.Count();
        }
        public BlogShopModel()
        {

        }
    }
}