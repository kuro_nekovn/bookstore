﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class CustomerViewDTO
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string ImgUrl { get; set; }
        public string Email { get; set; }
        public Nullable<bool> Sex { get; set; }
        public int Point { get; set; }
    }
}
