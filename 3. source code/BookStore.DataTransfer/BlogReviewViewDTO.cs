﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BlogReviewViewDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string BlogName { get; set; }
        public DateTime DateCreate { get; set; }
        public string Email { get; set; }
    }
}
