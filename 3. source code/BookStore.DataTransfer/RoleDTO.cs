﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class RoleDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public RoleDTO(Role role)
        {
            ID = role.ID;
            Name = role.Name;
            Description = role.Description;
        }
        public RoleDTO()
        {

        }
    }
}
