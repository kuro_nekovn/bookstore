﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BookViewDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public string Sumary { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
        public int Total { get; set; }
        public double Price { get; set; }
    }
}
