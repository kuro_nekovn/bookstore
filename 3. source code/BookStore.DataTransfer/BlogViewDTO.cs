﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BlogViewDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string BlogImg { get; set; }
        public string UserCreateName { get; set; }
        public string Sumary { get; set; }
    }
}
