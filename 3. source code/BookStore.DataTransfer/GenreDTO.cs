﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class GenreDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public GenreDTO()
        {

        }

        public GenreDTO(Genre genre)
        {
            ID = genre.ID;
            Name = genre.Name;
            Description = genre.Description;
        }
    }
}
