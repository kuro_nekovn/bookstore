﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataTransfer
{
    public class BlogReviewDTO
    {
        public int ID { get; set; }
        public string BlogName { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string Content { get; set; }
        public DateTime? CreateDate { get; set; }

        public BlogReviewDTO()
        {

        }

        public BlogReviewDTO(BlogReview blogReview)
        {
            ID = blogReview.ID;
            BlogName = blogReview.Blog.Title;
            Title = blogReview.Title;
            Email = blogReview.Email;
            Content = blogReview.Content;
            CreateDate = blogReview.DateCreate;
        }
    }
}
