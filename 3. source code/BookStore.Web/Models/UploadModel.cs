﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class UploadModel
    {
        public dynamic Data { get; set; }
        public string Option { get; set; }
    }
}