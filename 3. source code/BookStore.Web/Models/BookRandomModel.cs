﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class BookRandomModel
    {
        public int BookID { get; set; }
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string Summary { get; set; }
        public double Rating { get; set; }
        public double Price { get; set; }
        public string BookImg { get; set; }

        public BookRandomModel(Book book)
        {
            BookID = book.ID;
            BookName = book.Name;
            AuthorName = book.Author.LastName + " " + book.Author.FirstName;
            Summary = book.Sumary;
            Rating = book.Rating == null ? 0 : (double)book.Rating;
            Price = book.Price;
            BookImg = book.ImgUrl;
        }
    }
}