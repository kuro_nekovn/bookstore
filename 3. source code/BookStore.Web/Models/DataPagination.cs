﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class DataPagination<T>
    {
        public Pagination Page { get; set; }
        public List<T> Items { get; set; }
    }
}