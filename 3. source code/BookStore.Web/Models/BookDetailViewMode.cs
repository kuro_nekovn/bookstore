﻿using BookStore.DataTransfer;
using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class BookDetailViewMode
    {
        public BookDetailDTO BookDetail { get; set; }
        public BookImageDTO[] BookImages { get; set; }
    }

    public class BookDetailDTO : BookDTO
    {
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
        public string GenreName { get; set; }
        public int TotalReview { get; set; }

        public BookDetailDTO(Book book): base(book)
        {
            AuthorName = book.Author.LastName + " " + book.Author.FirstName;
            PublisherName = book.Publisher.Name;
            GenreName = book.Genre.Name;
            TotalReview = book.Rates.Count;
        }
    }
}