﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class OrderShopModel
    {
        public double? ID { get; set; }
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string BookImg { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public OrderShopModel()
        {

        }
        public OrderShopModel(OrderDetail order)
        {
            BookName = order.Book.Name;
            AuthorName = order.Book.Author.LastName + " " + order.Book.Author.FirstName;
            BookImg = order.Book.ImgUrl;
            Quantity = order.Quantity;
            ID = order.TotalPrice;
            Price = order.UnitPrice;
        }
    }
}