﻿using BookStore.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class AuthorAndBookViewModel
    {
        public AuthorViewDTO AuthorInfomation { get; set; }
        public List<BookViewDTO> BookInformation { get; set; }
        public AuthorAndBookViewModel()
        {
            AuthorInfomation = new AuthorViewDTO();
            BookInformation = new List<BookViewDTO>();
        }
    }
}