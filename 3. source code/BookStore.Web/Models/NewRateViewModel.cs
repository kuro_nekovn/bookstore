﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Models
{
    public class NewRateViewModel
    {
        public string BookImg { get; set; }
        public string BookName { get; set; }
        public string CustomerName { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public double Rating { get; set; }
        public int ID { get; set; }
        public NewRateViewModel()
        {

        }
        public NewRateViewModel(Rate rate)
        {
            BookImg = rate.Book.ImgUrl;
            BookName = rate.Book.Name;
            ID = rate.BookID;
            AuthorName = rate.Book.Author.LastName + " " + rate.Book.Author.FirstName;
            CustomerName = rate.Customer.LastName + " " + rate.Customer.FirstName;
            Title = rate.Title;
            Content = rate.Content;
            Rating = rate.Rating;
        }
    }
}