﻿using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BookStore.Web
{  
    public class Startup
    {
        public static readonly string Root = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string adminRoot = Path.Combine(Root, @"Areas\Admin\wwwroot");
        public static readonly string wwwroot = Path.Combine(Root, "WWWRoot");
        public static readonly string shared = Path.Combine(Root, "Shared");
        public void Configuration(IAppBuilder app)
        {
            ConfigureStaticFiles(app);
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
        }
        private void ConfigureStaticFiles(IAppBuilder app)
        {
            var rootFileServerOptions = new FileServerOptions()
            {
                EnableDefaultFiles = false,
                EnableDirectoryBrowsing = false,
                RequestPath = new PathString(string.Empty),
                FileSystem = Directory.Exists(wwwroot) ? new PhysicalFileSystem(wwwroot) : null
            };

            rootFileServerOptions.StaticFileOptions.ServeUnknownFileTypes = false;

            app.UseFileServer(rootFileServerOptions);

            //admin

            var adminRootFileServerOptions = new FileServerOptions()
            {
                EnableDefaultFiles = false,
                EnableDirectoryBrowsing = false,
                RequestPath = new PathString("/admin"),
                FileSystem = Directory.Exists(adminRoot) ? new PhysicalFileSystem(adminRoot) : null,
            };
            adminRootFileServerOptions.StaticFileOptions.ServeUnknownFileTypes = false;
            app.UseFileServer(adminRootFileServerOptions);

            //shared
            var fileUploadsOptions = new FileServerOptions()
            {
                RequestPath = new PathString("/shared"),
                FileSystem = Directory.Exists(shared) ? new PhysicalFileSystem(shared) : null
            };

            app.UseFileServer(fileUploadsOptions);

            var fileSharedOptions = new FileServerOptions()
            {
                RequestPath = new PathString("/admin/shared"),
                FileSystem = Directory.Exists(shared) ? new PhysicalFileSystem(shared) : null
            };

            app.UseFileServer(fileSharedOptions);
        }
    }
}