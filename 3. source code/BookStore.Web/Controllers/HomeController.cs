﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Web.Controllers
{
    public class HomeController : Controller
    {
        public static readonly string IndexFileName = "~/wwwroot/index.html";

        public ActionResult Index()
        {
            return new FilePathResult(IndexFileName, "text/html");
        }
    }
}
