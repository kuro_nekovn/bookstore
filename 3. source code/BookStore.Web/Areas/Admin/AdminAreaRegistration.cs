﻿using BookStore.Web.Utilities;
using System.Web.Http;
using System.Web.Mvc;

namespace BookStore.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                name: "Admin_DefaultApi",
                routeTemplate: "admin/api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            context.MapRoute(
                name: "Admin_default",
                url: "Admin/{*.}",
                defaults: new { Controller = "Home", action = "Index" },
                namespaces: new[] { "BookStore.Web.Areas.Admin.Controllers" }
            );

            
        }
    }
}