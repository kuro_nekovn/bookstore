﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Web.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        public static readonly string AdminIndexFileName = "~/Areas/Admin/wwwroot/index.html";

        public ActionResult Index()
        {
            try
            {
                var result = new FilePathResult(AdminIndexFileName, "text/html");
                ViewBag.test = result;
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            
        }
    }
}