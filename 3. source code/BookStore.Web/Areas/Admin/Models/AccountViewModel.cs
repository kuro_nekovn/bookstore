﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Areas.Admin.Models
{
    public class AccountViewModel
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}