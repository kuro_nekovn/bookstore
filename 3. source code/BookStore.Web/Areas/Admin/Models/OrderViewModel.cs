﻿using BookStore.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Areas.Admin.Models
{
    public class OrderViewModel
    {
        public int OrderID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public string Address { get; set; }
        public double TotalPrice { get; set; }
        public OrderDetailDTO[] OrderDetailModel { get; set; }
    }
}