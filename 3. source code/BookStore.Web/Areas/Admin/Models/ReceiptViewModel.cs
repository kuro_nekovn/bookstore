﻿using BookStore.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Areas.Admin.Models
{
    public class ReceiptViewModel
    {
        public int ReceiptID { get; set; }
        public int EmployeeID { get; set; }
        public int PublisherID { get; set; }
        public string Description { get; set; }
        public double TotalPrice { get; set; }
        public ReceiptDetailDTO[] ReceiptDetailModel { get; set; }
    }
}