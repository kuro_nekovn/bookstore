﻿using BookStore.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Areas.Admin.Models
{
    public class BookViewModel
    {
        public BookDTO BookDetail { get; set; }
        public BookImageDTO[] BookImages { get; set; }
    }
}