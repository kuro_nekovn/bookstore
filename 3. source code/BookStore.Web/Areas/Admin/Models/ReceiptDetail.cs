﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Web.Areas.Admin.Models
{
    public class ReceiptDetailViewModel
    {
        public int ReceiptID { get; set; }
        public string BookName { get; set; }
        public int BookID { get; set; }
        public string ImgUrl { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double TotalPrice { get; set; }
    }
}