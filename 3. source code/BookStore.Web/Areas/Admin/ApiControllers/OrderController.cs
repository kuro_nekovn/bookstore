﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles="Admin, Saleman")]
    public class OrderController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(OrderController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IOrderBUS orderBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOrderDetailBUS orderDetailBUS;

        /// <summary>
        /// 
        /// </summary>
        public OrderController()
        {
            orderBUS = new OrderBUS();
            orderDetailBUS = new OrderDetailBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderBUS"></param>
        public OrderController(IOrderBUS orderBUS, IOrderDetailBUS orderDetailBUS)
        {
            this.orderBUS = orderBUS;
            this.orderDetailBUS = orderDetailBUS;
        }

        #endregion Contructors

        #region Get

        // GET admin/api/order/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = orderBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/order/Orders
        [HttpGet]
        public HttpResponseMessage Orders(int CurrentPage = 1, int PageSize = 5, string employeeID = null, string customerID = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                employeeID = employeeID == "undefined" ? null : employeeID;
                customerID = customerID == "undefined" ? null : customerID;
                List<OrderViewDTO> result = orderBUS.OrderPage(ref page, employeeID, customerID);
                var pagingData = new DataPagination<OrderViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/order/getOrder/id
        [HttpGet]
        public HttpResponseMessage GetOrder(int id)
        {
            try
            {
                var order = orderBUS.GetByID(id);
                if (ValidationHelpers.IsValid(order))
                {
                    OrderViewModel result = new OrderViewModel();
                    result.OrderID = order.ID;
                    result.TotalPrice = (double)order.TotalPrice;
                    result.CustomerID = order.CustomerID;
                    result.Address = order.Address;
                    result.EmployeeID = order.EmployeeID;
                    result.OrderDetailModel = order.OrderDetails.Select(x => new OrderDetailDTO()
                    {
                        BookName = x.Book.Name + " - " + x.Book.Author.FirstName + " " + x.Book.Author.LastName + " - " + x.Book.Publisher.Name,
                        BookID = x.BookID,
                        Discount = x.Discount,
                        ImgUrl = x.Book.ImgUrl,
                        OrderID = x.OrderID,
                        Price = x.UnitPrice,
                        Total = x.Book.Total,
                        Quantity = x.Quantity,
                        TotalPrice = (double)x.TotalPrice
                    }).ToArray();
                    return Request.CreateResponse(HttpStatusCode.OK,
                        result, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your order is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/employee/GetItems
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = orderBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
        #endregion Get

        #region Edit

        // GET admin/api/order/getOrder/id
        [HttpPost]
        public HttpResponseMessage Edit(OrderViewModel order)
        {
            try
            {
                OrderDTO orderDTO = new OrderDTO();
                orderDTO.Address = order.Address;
                orderDTO.CustomerID = order.CustomerID;
                orderDTO.EmployeeID = order.EmployeeID;
                orderDTO.TotalPrice = order.TotalPrice;
                orderDTO.ID = order.OrderID;
                orderDTO = orderBUS.Edit(orderDTO);
                if (!ValidationHelpers.IsValid(orderDTO))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new { Message = "Edit order have been failed" },
                        Configuration.Formatters.JsonFormatter);                    
                }
                var result = orderDetailBUS.Edit(order.OrderDetailModel, order.OrderID);
                return Request.CreateResponse(HttpStatusCode.OK,
                        order, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Edit

        #region Delete

        // admin/api/order/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = orderBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete order have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Order ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // admin/api/order/DeleteDetail
        [HttpGet]
        public HttpResponseMessage DeleteDetail(int OrderID, int BookID)
        {
            try
            {
                if (OrderID > 0 && BookID > 0)
                {
                    bool result = orderDetailBUS.Delete(OrderID, BookID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            true,
                            Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete order detail have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Order ID or Book ID is not valid." },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(OrderViewModel order)
        {
            try
            {
                OrderDTO orderDTO = new OrderDTO();
                orderDTO.Address = order.Address;
                orderDTO.CustomerID = order.CustomerID;
                orderDTO.EmployeeID = order.EmployeeID;
                orderDTO.TotalPrice = order.TotalPrice;
                orderDTO = orderBUS.Create(orderDTO);
                if (orderDTO == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        new { Message = "Creating order have been failed" },
                        Configuration.Formatters.JsonFormatter);
                }
                order.OrderID = orderDTO.ID;
                var result = orderDetailBUS.Create(order.OrderDetailModel, orderDTO.ID);
                return Request.CreateResponse(HttpStatusCode.OK, order,
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create
    }
}