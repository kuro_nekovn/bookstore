﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, Salesman")]
    public class CustomerController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(CustomerController));

        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBUS customerBUS;

        /// <summary>
        /// 
        /// </summary>
        public CustomerController()
        {
            customerBUS = new CustomerBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBUS"></param>
        public CustomerController(ICustomerBUS customerBUS)
        {
            this.customerBUS = customerBUS;
        }

		  #endregion Contructors

        #region Get

        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = customerBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }


        // GET admin/api/customer/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = customerBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/customer/Customers
        [HttpGet]
        public HttpResponseMessage Customers(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<CustomerViewDTO> result = customerBUS.CustomerPage(ref page, keyword);
                var pagingData = new DataPagination<CustomerViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/customer/getCustomer/id
        [HttpGet]
        public HttpResponseMessage GetCustomer(int id)
        {
            try
            {
                var customer = customerBUS.GetByID(id, false);
                if (ValidationHelpers.IsValid(customer))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    customer, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your customer is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Get

        #region Edit

        // GET admin/api/customer/getCustomer/id
        [HttpPost]
        public HttpResponseMessage Edit(CustomerDTO customer)
        {
            try
            {
                string name = customer.FirstName + " " + customer.LastName;
                customer = customerBUS.Edit(customer);
                if (ValidationHelpers.IsValid(customer))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        customer, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit customer " + name + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/customer/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = customerBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete customer have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Customer ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }		
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(CustomerDTO customer)
        {
            try
            {
                customer = customerBUS.Create(customer);
                if (customer != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, customer,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating customer have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Create
    }
}