﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper")]
    public class ReceiptController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ReceiptController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IReceiptBUS receiptBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly IReceiptDetailBUS receiptDetailBUS;

        /// <summary>
        /// 
        /// </summary>
        public ReceiptController()
        {
            receiptBUS = new ReceiptBUS();
            receiptDetailBUS = new ReceiptDetailBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiptBUS"></param>
        public ReceiptController(IReceiptBUS receiptBUS, IReceiptDetailBUS receiptDetailBUS)
        {
            this.receiptBUS = receiptBUS;
            this.receiptDetailBUS = receiptDetailBUS;
        }

        #endregion Contructors

        #region Get

        // GET admin/api/receipt/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = receiptBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/receipt/Receipts
        [HttpGet]
        public HttpResponseMessage Receipts(int CurrentPage = 1, int PageSize = 5, string employeeName = null, string publisherName = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                employeeName = employeeName == "undefined" ? null : employeeName;
                publisherName = publisherName == "undefined" ? null : publisherName;
                List<ReceiptViewDTO> result = receiptBUS.ReceiptPage(ref page, employeeName, publisherName);
                var pagingData = new DataPagination<ReceiptViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/receipt/getReceipt/id
        [HttpGet]
        public HttpResponseMessage GetReceipt(int id)
        {
            try
            {
                var receipt = receiptBUS.GetByID(id);
                if (ValidationHelpers.IsValid(receipt))
                {
                    ReceiptViewModel result = new ReceiptViewModel();
                    result.ReceiptID = receipt.ID;
                    result.TotalPrice = (double)receipt.TotalPrice;
                    result.PublisherID = receipt.PublisherID;
                    result.Description = receipt.Description;
                    result.EmployeeID = receipt.EmployeeID;
                    result.ReceiptDetailModel = receipt.ReceiptDetails.Select(x => new ReceiptDetailDTO()
                    {
                        BookName = x.Book.Name + " - " + x.Book.Author.FirstName + " " + x.Book.Author.LastName + " - " + x.Book.Publisher.Name,
                        BookID = x.BookID,
                        ImgUrl = x.Book.ImgUrl,
                        ReceiptID = x.ReceiptID,
                        Price = x.UnitPrice,
                        Total = x.Book.Total,
                        Quantity = x.Quantity,
                        TotalPrice = (double)x.TotalPrice
                    }).ToArray();
                    return Request.CreateResponse(HttpStatusCode.OK,
                        result, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your receipt is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Get

        #region Edit

        // GET admin/api/receipt/getReceipt/id
        [HttpPost]
        public HttpResponseMessage Edit(ReceiptViewModel receipt)
        {
            try
            {
                ReceiptDTO receiptDTO = new ReceiptDTO();
                receiptDTO.Description = receipt.Description;
                receiptDTO.PublisherID = receipt.PublisherID;
                receiptDTO.EmployeeID = receipt.EmployeeID;
                receiptDTO.TotalPrice = receipt.TotalPrice;
                receiptDTO.ID = receipt.ReceiptID;
                receiptDTO = receiptBUS.Edit(receiptDTO);
                if (!ValidationHelpers.IsValid(receiptDTO))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new { Message = "Edit receipt have been failed" },
                        Configuration.Formatters.JsonFormatter);
                }
                var result = receiptDetailBUS.Edit(receipt.ReceiptDetailModel, receipt.ReceiptID);
                return Request.CreateResponse(HttpStatusCode.OK,
                        receipt, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Edit

        #region Delete

        // admin/api/receipt/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = receiptBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            true,
                            Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete receipt have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Receipt ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // admin/api/receipt/DeleteDetail
        [HttpGet]
        public HttpResponseMessage DeleteDetail(int ReceiptID, int BookID)
        {
            try
            {
                if (ReceiptID > 0 && BookID > 0)
                {
                    bool result = receiptDetailBUS.Delete(ReceiptID, BookID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            true,
                            Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete receipt detail have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Receipt ID or Book ID is not valid." },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(ReceiptViewModel receipt)
        {
            try
            {
                ReceiptDTO receiptDTO = new ReceiptDTO();
                receiptDTO.Description = receipt.Description;
                receiptDTO.PublisherID = receipt.PublisherID;
                receiptDTO.EmployeeID = receipt.EmployeeID;
                receiptDTO.TotalPrice = receipt.TotalPrice;
                receiptDTO = receiptBUS.Create(receiptDTO);
                if (receiptDTO == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        new { Message = "Creating receipt have been failed" },
                        Configuration.Formatters.JsonFormatter);
                }
                receipt.ReceiptID = receiptDTO.ID;
                var result = receiptDetailBUS.Create(receipt.ReceiptDetailModel, receiptDTO.ID);
                return Request.CreateResponse(HttpStatusCode.OK, receipt,
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create
    }
}