﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    public class AccountController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AccountController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IAccountBUS accountBUS;

        /// <summary>
        /// 
        /// </summary>
        public AccountController()
        {
            accountBUS = new AccountBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adsBUS"></param>
        public AccountController(IAccountBUS accountBUS)
        {
            this.accountBUS = accountBUS;
        }

        #endregion Contructors

        #region Get

        // admin/api/account/GetRoles
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetRoles()
        {
            try
            {
                var roles = accountBUS.GetAllRoles();
                return Request.CreateResponse(HttpStatusCode.OK, roles, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // admin/api/account/GetRole
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetRole()
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                if (identityClaims != null)
                {
                    var claimEmail = identityClaims.FindFirst(ClaimTypes.Role);
                    if (claimEmail != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, claimEmail.Value, Configuration.Formatters.JsonFormatter);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Change Password failed" }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/account/getEmployee/id
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetEmployeeProfile()
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                EmployeeDTO employee = null;
                if (identityClaims != null)
                {
                    var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                    IEmployeeBUS employeeBUS = new EmployeeBUS();
                    employee = employeeBUS.GetByEmail(claimEmail.Value);
                    if (ValidationHelpers.IsValid(employee))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                        employee, Configuration.Formatters.JsonFormatter);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your employee is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Get

        #region Password

        // admin/api/account/AdminChangeEmployeePassword
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage AdminChangeEmployeePassword(AccountViewModel model)
        {
            try
            {
                var result = accountBUS.ChangeEmployeePassword(model.ID, model.Email, model.Password);
                return Request.CreateResponse(HttpStatusCode.OK, result, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }


        // admin/api/account/ChangePassword
        [HttpPost]
        [Authorize]
        public HttpResponseMessage ChangePassword(AccountViewModel model)
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                if (identityClaims != null)
                {
                    var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                    var result = accountBUS.ChangeEmployeePassword(-1, claimEmail.Value, model.Password);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, result, Configuration.Formatters.JsonFormatter);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Change Password failed"}, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // admin/api/account/ChangePassword
        [HttpPost]
        [Authorize]
        public HttpResponseMessage ChangeEmployeeImage(AccountViewModel ImageUrl)
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                if (identityClaims != null)
                {
                    var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                    IEmployeeBUS employeeBUS = new EmployeeBUS();
                    EmployeeDTO employee = null;
                    employee = employeeBUS.GetByEmail(claimEmail.Value);
                    if (ValidationHelpers.IsValid(employee))
                    {
                        employee.ImgUrl = ImageUrl.Password;
                        employee = employeeBUS.Edit(employee);
                        return Request.CreateResponse(HttpStatusCode.OK,
                        employee != null, Configuration.Formatters.JsonFormatter);
                    }                    
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Change Avatar failed" }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
        // admin/api/account/AdminChangeCustomerPassword
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage AdminChangeCustomerPassword(AccountViewModel model)
        {
            try
            {
                var result = accountBUS.ChangeCustomerPassword(model.ID, model.Email, model.Password);
                return Request.CreateResponse(HttpStatusCode.OK, result, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }


        #endregion Password

        #region login
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Authenticate([FromBody] AccountViewModel account)
        {
            try
            {
                var loginResponse = new LoginResponse();
                Employee emp = null;

                IHttpActionResult response;
                HttpResponseMessage responseMsg = new HttpResponseMessage();
                if (account != null)
                {
                    emp = accountBUS.Login(account.Email, account.Password);
                }

                // if credentials are valid
                if (emp != null)
                {
                    string token = createToken(emp);
                    account.Password = token;
                    //return the token
                    return Ok<AccountViewModel>(account);
                }
                else
                {
                    // if credentials are not valid send unauthorized status code in response
                    loginResponse.responseMsg.StatusCode = HttpStatusCode.Unauthorized;
                    loginResponse.responseMsg.Content = new StringContent("Your Email or password is invalid.");
                    response = ResponseMessage(loginResponse.responseMsg);
                    return response;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private string createToken(Employee user)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);
            var tokenHandler = new JwtSecurityTokenHandler();

            //create a identity and add claims to the user which we want to log in
            var allClaim = user.Roles.Select(x => new Claim(ClaimTypes.Role, x.Name)).ToList();
            allClaim.Add(new Claim(ClaimTypes.Name, user.Email));
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(allClaim);

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb3375912343e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "Bookstore", audience: "Bookstore",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
        #endregion login
    }
}
