﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper, HR, Salesman")]
    public class BlogReviewController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BlogReviewController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IBlogReviewBUS blogReviewBUS;

        /// <summary>
        /// 
        /// </summary>
        public BlogReviewController()
        {
            blogReviewBUS = new BlogReviewBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blogReviewBUS"></param>
        public BlogReviewController(IBlogReviewBUS blogReviewBUS)
        {
            this.blogReviewBUS = blogReviewBUS;
        }

		  #endregion Contructors

        #region Get

        // GET admin/api/blogReview/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = blogReviewBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/blogReview/BlogReviews
        [HttpGet]
        public HttpResponseMessage BlogReviews(int CurrentPage = 1, int PageSize = 5, string keyword = null, int blogID = 0)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<BlogReviewViewDTO> result = blogReviewBUS.BlogReviewPage(ref page, keyword, blogID);
                var pagingData = new DataPagination<BlogReviewViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/blogReview/getBlogReview/id
        [HttpGet]
        public HttpResponseMessage GetBlogReview(int id)
        {
            try
            {
                var blogReview = blogReviewBUS.GetByID(id);
                if (ValidationHelpers.IsValid(blogReview))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    blogReview, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your blogReview is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Get

        #region Edit

        // GET admin/api/blogReview/getBlogReview/id
        [HttpPost]
        public HttpResponseMessage Edit(BlogReviewDTO blogReview)
        {
            try
            {
                string title = blogReview.Title;
                blogReview = blogReviewBUS.Edit(blogReview);
                if (ValidationHelpers.IsValid(blogReview))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        blogReview, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit blogReview " + title + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/blogReview/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = blogReviewBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete blogReview have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your BlogReview ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(BlogReviewDTO blogReview)
        {
            try
            {
                blogReview = blogReviewBUS.Create(blogReview);
                if (blogReview != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, blogReview,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating blogReview have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		#endregion Create
    }
}