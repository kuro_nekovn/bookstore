﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin")]
    public class EmployeeRoleController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(EmployeeRoleController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IEmployeeRoleBUS employeeRoleBUS;

        /// <summary>
        /// 
        /// </summary>
        public EmployeeRoleController()
        {
            employeeRoleBUS = new EmployeeRoleBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeRoleBUS"></param>
        public EmployeeRoleController(IEmployeeRoleBUS employeeRoleBUS)
        {
            this.employeeRoleBUS = employeeRoleBUS;
        }
		
		  #endregion Contructors

        #region Get      
       
		
		// GET admin/api/publisher/Publishers
        [HttpGet]
        public HttpResponseMessage EmployeeRoles(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<EmployeeRoleDTO> result = employeeRoleBUS.EmployeePage(ref page, keyword);
                var pagingData = new DataPagination<EmployeeRoleDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
        #endregion Get

        #region Edit

        // GET admin/api/employeeRole/getRole/id
        [HttpPost]
        public HttpResponseMessage Edit(EmployeeRoleDTO employeeRole)
        {
            try
            {
                if (employeeRole != null)
                {
                    var employee = employeeRoleBUS.EditEmployeeRole(employeeRole);
                    if (employee != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, employeeRole,
                            Configuration.Formatters.JsonFormatter);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit employeeRole " + employeeRole.LastName + " " + employeeRole.FirstName + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

    }
}