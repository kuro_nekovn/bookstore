﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, HR")]
    public class EmployeeController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(EmployeeController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IEmployeeBUS employeeBUS;

        /// <summary>
        /// 
        /// </summary>
        public EmployeeController()
        {
            employeeBUS = new EmployeeBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeBUS"></param>
        public EmployeeController(IEmployeeBUS employeeBUS)
        {
            this.employeeBUS = employeeBUS;
        }

        #endregion Contructors

        #region Get

        // GET admin/api/employee/getall
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = employeeBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/employee/Employees
        [HttpGet]
        [Authorize(Roles = "Admin,HR")]
        public HttpResponseMessage Employees(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<EmployeeViewDTO> result = employeeBUS.EmployeePage(ref page, keyword);
                var pagingData = new DataPagination<EmployeeViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/employee/getEmployee/id
        [HttpGet]
        [Authorize(Roles = "Admin,HR")]
        public HttpResponseMessage GetEmployee(int id)
        {
            try
            {
                var employee = employeeBUS.GetByID(id, false);
                if (ValidationHelpers.IsValid(employee))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    employee, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your employee is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }


       

        // GET admin/api/employee/GetItems
        [HttpGet]
        [Authorize(Roles = "Admin,HR")]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = employeeBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Get

        #region Edit

        // GET admin/api/employee/getEmployee/id
        [HttpPost]
        [Authorize(Roles = "Admin,HR")]
        public HttpResponseMessage Edit(EmployeeDTO employee)
        {
            try
            {
                string name = employee.FirstName + " " + employee.LastName;
                employee = employeeBUS.Edit(employee);
                if (ValidationHelpers.IsValid(employee))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        employee, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit employee " + name + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Edit

        #region Delete

        // admin/api/employee/delete
        [HttpGet]
        [Authorize(Roles = "Admin,HR")]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    var identityClaims = (ClaimsIdentity)User.Identity;
                    EmployeeDTO employee = null;
                    if (identityClaims != null)
                    {
                        var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                        employee = employeeBUS.GetByEmail(claimEmail.Value);
                        if (ValidationHelpers.IsValid(employee) && employee.ID == ID)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest,
                                new { Message = "Can not deleted your account." },
                                Configuration.Formatters.JsonFormatter);
                        }
                    }
                    bool result = employeeBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete employee have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Employee ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Delete

        #region Create

        [HttpPost]
        [Authorize(Roles = "Admin,HR")]
        public HttpResponseMessage Create(EmployeeDTO employee)
        {
            try
            {
                employee = employeeBUS.Create(employee);
                if (employee != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, employee,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating employee have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create
    }
}