﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper, HR, Salesman")]
    public class BlogController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BlogController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IBlogBUS blogBUS;

        /// <summary>
        /// 
        /// </summary>
        public BlogController()
        {
            blogBUS = new BlogBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blogBUS"></param>
        public BlogController(IBlogBUS blogBUS)
        {
            this.blogBUS = blogBUS;
        }

		  #endregion Contructors

        #region Get

        // GET admin/api/blog/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = blogBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/blog/getall
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = blogBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/blog/Blogs
        [HttpGet]
        public HttpResponseMessage Blogs(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<BlogViewDTO> result = blogBUS.BlogPage(ref page, keyword);
                var pagingData = new DataPagination<BlogViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/blog/getBlog/id
        [HttpGet]
        public HttpResponseMessage GetBlog(int id)
        {
            try
            {
                var blog = blogBUS.GetByID(id);
                if (ValidationHelpers.IsValid(blog))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    blog, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your blog is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Get

        #region Edit

        // GET admin/api/blog/getBlog/id
        [HttpPost]
        public HttpResponseMessage Edit(BlogDTO blog)
        {
            try
            {
                string title = blog.Title;
                blog.UserCreateName = "1";
                blog = blogBUS.Edit(blog);
                if (ValidationHelpers.IsValid(blog))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        blog, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit blog " + title + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/blog/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = blogBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete blog have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Blog ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(BlogDTO blog)
        {
            try
            {
                blog.UserCreateName = "1";
                blog = blogBUS.Create(blog);
                if (blog != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, blog,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating blog have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Create
    }
}