﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, Saleman")]
    public class AdvertisementController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AdvertisementController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IAdvertisementBUS adsBUS;

        /// <summary>
        /// 
        /// </summary>
        public AdvertisementController()
        {
            adsBUS = new AdvertisementBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adsBUS"></param>
        public AdvertisementController(IAdvertisementBUS adsBUS)
        {
            this.adsBUS = adsBUS;
        }
		
		  #endregion Contructors

        #region Get

        // GET admin/api/ads/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = adsBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/ads/getAdvertisement/id
        [HttpGet]
        public HttpResponseMessage GetAdvertisement(int id)
        {
            try
            {
                var ads = adsBUS.GetByID(id);
                if (ValidationHelpers.IsValid(ads))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    ads, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your ads is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		// GET admin/api/publisher/Publishers
        [HttpGet]
        public HttpResponseMessage Advertisements(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<AdvertisementDTO> result = adsBUS.AdvertisementPage(ref page, keyword);
                var pagingData = new DataPagination<AdvertisementDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Get

        #region Edit

        // GET admin/api/ads/getAdvertisement/id
        [HttpPost]
        public HttpResponseMessage Edit(AdvertisementDTO ads)
        {
            try
            {
                ads = adsBUS.Edit(ads);
                if (ValidationHelpers.IsValid(ads))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        ads, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit ads have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/ads/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = adsBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete ads have been failed. ID must be greate than 0" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Advertisement ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(AdvertisementDTO ads)
        {
            try
            {
                ads = adsBUS.Create(ads);
                if (ads != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ads,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating ads have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Create

        
    }
}