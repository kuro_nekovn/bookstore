﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper")]
    public class PublisherController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(PublisherController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IPublisherBUS publisherBUS;

        /// <summary>
        /// 
        /// </summary>
        public PublisherController()
        {
            publisherBUS = new PublisherBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="publisherBUS"></param>
        public PublisherController(IPublisherBUS publisherBUS)
        {
            this.publisherBUS = publisherBUS;
        }

		#endregion Contructors

        #region Get

        // GET admin/api/publisher/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = publisherBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/publisher/getall
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = publisherBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/publisher/Publishers
        [HttpGet]
        public HttpResponseMessage Publishers(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<PublisherDTO> result = publisherBUS.PublisherPage(ref page, keyword);
                var pagingData = new DataPagination<PublisherDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/publisher/getPublisher/id
        [HttpGet]
        public HttpResponseMessage GetPublisher(int id)
        {
            try
            {
                var publisher = publisherBUS.GetByID(id);
                if (ValidationHelpers.IsValid(publisher))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    publisher, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your publisher is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		#endregion Get

        #region Edit

        // GET admin/api/publisher/getPublisher/id
        [HttpPost]
        public HttpResponseMessage Edit(PublisherDTO publisher)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string name = publisher.Name;
                    publisher = publisherBUS.Edit(publisher);
                    if (ValidationHelpers.IsValid(publisher))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            publisher, Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new { Message = "Edit publisher " + name + " have been failed" },
                    Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your publisher model is not valid" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/publisher/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = publisherBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete publisher have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Publisher ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(PublisherDTO publisher)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    publisher = publisherBUS.Create(publisher);
                    if (publisher != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, publisher,
                            Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Creating publisher have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Publisher is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Create
    }
}