﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper")]
    public class GenreController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(GenreController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IGenreBUS genreBUS;

        /// <summary>
        /// 
        /// </summary>
        public GenreController()
        {
            genreBUS = new GenreBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genreBUS"></param>
        public GenreController(IGenreBUS genreBUS)
        {
            this.genreBUS = genreBUS;
        }
		
		  #endregion Contructors

        #region Get

        // GET admin/api/genre/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = genreBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/genre/getGenre/id
        [HttpGet]
        public HttpResponseMessage GetGenre(int id)
        {
            try
            {
                var genre = genreBUS.GetByID(id);
                if (ValidationHelpers.IsValid(genre))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    genre, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your genre is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		// GET admin/api/publisher/Publishers
        [HttpGet]
        public HttpResponseMessage Genres(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<GenreDTO> result = genreBUS.GenrePage(ref page, keyword);
                var pagingData = new DataPagination<GenreDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/author/getall
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = genreBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Get

        #region Edit

        // GET admin/api/genre/getGenre/id
        [HttpPost]
        public HttpResponseMessage Edit(GenreDTO genre)
        {
            try
            {
                string name = genre.Name;
                genre = genreBUS.Edit(genre);
                if (ValidationHelpers.IsValid(genre))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        genre, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit genre " + name + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/genre/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = genreBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete genre have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Genre ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Delete

        #region Create
		
        [HttpPost]
        public HttpResponseMessage Create(GenreDTO genre)
        {
            try
            {
                genre = genreBUS.Create(genre);
                if (genre != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, genre,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating genre have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
        
        #endregion Create
    }
}