﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper")]
    public class BookController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BookController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IBookBUS bookBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBookImageBUS bookImageBUS;

        /// <summary>
        /// 
        /// </summary>
        public BookController()
        {
            bookBUS = new BookBUS();
            bookImageBUS = new BookImageBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookBUS"></param>
        public BookController(IBookBUS bookBUS, IBookImageBUS bookImageBUS)
        {
            this.bookBUS = bookBUS;
            this.bookImageBUS = bookImageBUS;
        }

        #endregion Contructors

        #region Get

        // GET admin/api/book/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = bookBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/book/getall
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = bookBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/book/getall
        [HttpGet]
        public HttpResponseMessage GetOrderBooks()
        {
            try
            {
                var data = bookBUS.GetOrderBook();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/book/Books
        [HttpGet]
        public HttpResponseMessage Books(int CurrentPage = 1, int PageSize = 5, string bookName = null, string authorName = null, string publisherName = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                bookName = bookName == "undefined" ? null : bookName;
                authorName = authorName == "undefined" ? null : authorName;
                publisherName = publisherName == "undefined" ? null : publisherName;
                List<BookViewDTO> result = bookBUS.BookPage(ref page, bookName, authorName, publisherName);
                var pagingData = new DataPagination<BookViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/book/getBook/id
        [HttpGet]
        public HttpResponseMessage GetBook(int id)
        {
            try
            {
                BookViewModel book = new BookViewModel();
                var bookDetail = bookBUS.GetByID(id);
                if (!ValidationHelpers.IsValid(bookDetail))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your book is not found" },
                    Configuration.Formatters.JsonFormatter);
                }
                var bookImage = bookImageBUS.GetAll(id).Select( x => new BookImageDTO()
                    {
                        ID = x.ID,
                        BookUrl = x.ImageUrl,
                        BookID = x.BookID
                    })?.ToArray();
                book.BookDetail = bookDetail;
                book.BookImages = bookImage;
                return Request.CreateResponse(HttpStatusCode.OK,
                    book, Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Get

        #region Edit

        // GET admin/api/book/getBook/id
        [HttpPost]
        public HttpResponseMessage Edit(BookViewModel book)
        {
            try
            {
                string name = book.BookDetail.Name;
                book.BookDetail = bookBUS.Edit(book.BookDetail);
                if (!ValidationHelpers.IsValid(book))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        new { Message = "Edit book " + name + " have been failed" },
                        Configuration.Formatters.JsonFormatter);
                }
                var bookImg = bookImageBUS.Edit(book.BookImages, book.BookDetail.ID);
                return Request.CreateResponse(HttpStatusCode.OK,
                        book, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Edit

        #region Delete

        // admin/api/book/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = bookBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete book have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Book ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // admin/api/book/deleteImage
        [HttpGet]
        public HttpResponseMessage deleteImage(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = bookImageBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            true,
                            Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete receipt detail have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Receipt ID or Book ID is not valid." },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(BookViewModel book)
        {
            try
            {
                book.BookDetail = bookBUS.Create(book.BookDetail);
                if (book.BookDetail == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        new { Message = "Creating book have been failed" },
                        Configuration.Formatters.JsonFormatter);
                }
                var bookImage = bookImageBUS.Create(book.BookImages, book.BookDetail.ID);
                return Request.CreateResponse(HttpStatusCode.OK, book,
                        Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create
    }
}