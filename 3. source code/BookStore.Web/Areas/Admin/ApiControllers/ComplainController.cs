﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, StockKeeper, Salesman")]
    public class ComplainController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ComplainController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IComplainBUS complainBUS;

        /// <summary>
        /// 
        /// </summary>
        public ComplainController()
        {
            complainBUS = new ComplainBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="complainBUS"></param>
        public ComplainController(IComplainBUS complainBUS)
        {
            this.complainBUS = complainBUS;
        }

		  #endregion Contructors

        #region Get

        // GET admin/api/complain/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = complainBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/complain/Complains
        [HttpGet]
        public HttpResponseMessage Complains(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<ComplainViewDTO> result = complainBUS.ComplainPage(ref page, keyword);
                var pagingData = new DataPagination<ComplainViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/complain/getComplain/id
        [HttpGet]
        public HttpResponseMessage GetComplain(int id)
        {
            try
            {
                var complain = complainBUS.GetByID(id);
                if (ValidationHelpers.IsValid(complain))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    complain, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your complain is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Get

        #region Edit

        // GET admin/api/complain/getComplain/id
        [HttpPost]
        public HttpResponseMessage Edit(ComplainDTO complain)
        {
            try
            {
                complain = complainBUS.Edit(complain);
                if (ValidationHelpers.IsValid(complain))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        complain, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit complain have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/complain/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = complainBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete complain have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Complain ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(ComplainDTO complain)
        {
            try
            {
                complain = complainBUS.Create(complain);
                if (complain != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, complain,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating complain have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Create
    }
}