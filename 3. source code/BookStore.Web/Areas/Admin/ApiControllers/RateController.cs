﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.Areas.Admin.ApiControllers
{
    [Authorize(Roles = "Admin, Salesman")]
    public class RateController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(RateController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IRateBUS rateBUS;

        /// <summary>
        /// 
        /// </summary>
        public RateController()
        {
            rateBUS = new RateBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rateBUS"></param>
        public RateController(IRateBUS rateBUS)
        {
            this.rateBUS = rateBUS;
        }
		
		#endregion Contructors

        #region Get

        // GET admin/api/rate/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = rateBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/rate/Rates
        [HttpGet]
        public HttpResponseMessage Rates(int CurrentPage = 1, int PageSize = 5, string keyword = null, string bookID = null, string customerID = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                bookID = bookID == "undefined" ? null : bookID;
                customerID = customerID == "undefined" ? null : customerID;
                List<RateViewDTO> result = rateBUS.RatePage(ref page, keyword, bookID, customerID);
                var pagingData = new DataPagination<RateViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/rate/getRate/id
        [HttpGet]
        public HttpResponseMessage GetRate(int id)
        {
            try
            {
                var rate = rateBUS.GetByID(id);
                if (ValidationHelpers.IsValid(rate))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                    rate, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your rate is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

		#endregion Get

        #region Edit
		
        // GET admin/api/rate/getRate/id
        [HttpPost]
        public HttpResponseMessage Edit(RateDTO rate)
        {
            try
            {
                string title = rate.Title;
                rate = rateBUS.Edit(rate);
                if (ValidationHelpers.IsValid(rate))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        rate, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit rate " + title + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Edit

        #region Delete

        // admin/api/rate/delete
        [HttpGet]
        public HttpResponseMessage Delete(int ID)
        {
            try
            {
                if (ID > 0)
                {
                    bool result = rateBUS.Delete(ID);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                       true,
                       Configuration.Formatters.JsonFormatter);
                    }
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Delete rate have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Your Rate ID is not valid" },
                       Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Delete

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(RateDTO rate)
        {
            try
            {
                rate = rateBUS.Create(rate);
                if (rate != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, rate,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating rate have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
        #endregion Create
    }
}