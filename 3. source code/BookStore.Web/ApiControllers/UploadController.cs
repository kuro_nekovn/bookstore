﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using log4net;
using BookStore.Commons.Extentions;
using BookStore.Web.Models;

namespace BookStore.Web.ApiControllers
{
    public class UploadController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(UploadController));
		
		#endregion Contructors

        #region Get

        // POST: admin/API/Upload/Image
        [HttpPost]
        public async Task<HttpResponseMessage> Image(HttpRequestMessage request)
        {
            try
            {
                if (!request.Content.IsMimeMultipartContent("form-data"))
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

                }

                var provider = new MultipartMemoryStreamProvider();

                await Request.Content.ReadAsMultipartAsync(provider);

                if (provider.Contents.Count > 0)
                {
                    var file = provider.Contents[0];
                    byte[] buffer = await file.ReadAsByteArrayAsync();
                    string filename = file.Headers.ContentDisposition.FileName.Trim('\"');

                    try
                    {
                        filename = Save(filename, buffer);
                    }
                    catch (Exception ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ex, Configuration.Formatters.JsonFormatter);
                    }

                    if (filename != null)
                    {

                        return Request.CreateResponse(HttpStatusCode.OK, filename, Configuration.Formatters.JsonFormatter);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "File not found" }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex, Configuration.Formatters.JsonFormatter);
            }

            
        }
		#endregion Get

        #region Edit

        private string Save(string fileName, byte[] buffer)
        {
            try
            {
                string uniqueFileName = Convert.ToString(Guid.NewGuid());
                string fileExtension = Path.GetExtension(fileName);
                string newFileName = "~/shared/uploads/" + uniqueFileName + fileExtension;
                string path = HostingEnvironment.MapPath(newFileName);

                File.WriteAllBytes(path, buffer);

                string result = string.Format("/shared/uploads/{0}{1}", uniqueFileName, fileExtension);

                return result;
            }
            catch
            {
                throw;
            }

        }
		
        #endregion Edit
    }
}