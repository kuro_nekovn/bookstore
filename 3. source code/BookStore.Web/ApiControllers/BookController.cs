﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class BookController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BookController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IBookBUS bookBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBookImageBUS bookImageBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly Random random = new Random();

        /// <summary>
        /// 
        /// </summary>
        public BookController()
        {
            bookBUS = new BookBUS();
            bookImageBUS = new BookImageBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookBUS"></param>
        public BookController(IBookBUS bookBUS, IBookImageBUS bookImageBUS)
        {
            this.bookBUS = bookBUS;
            this.bookImageBUS = bookImageBUS;
        }

        #endregion Contructors

        #region Gets
        [HttpGet]
        public HttpResponseMessage GetRandomBooks()
        {
            try
            {
                List<Book> data = bookBUS.GetAll();
                List<BookRandomModel> result = new List<BookRandomModel>();
                for (int i = 0; i < 20; i++)
                {
                    result.Add(new BookRandomModel(data[random.Next(data.Count)]));
                }
                return Request.CreateResponse(HttpStatusCode.OK,
                    result.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET api/book/Books
        [HttpGet]
        public HttpResponseMessage ShopBooks(int CurrentPage = 1, int PageSize = 5, string bookName = null, int authorName = 0, int publisherName = 0, int genre = 0)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                bookName = bookName == "undefined" ? null : bookName;
                List<BookViewDTO> result = bookBUS.BookShopPage(ref page, bookName, authorName, publisherName, genre);
                var pagingData = new DataPagination<BookViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/book/getBook/id
        [HttpGet]
        public HttpResponseMessage GetBook(int id)
        {
            try
            {
                BookDetailViewMode book = new BookDetailViewMode();
                var bookDetail = bookBUS.GetAll().SingleOrDefault(x => x.ID == id);
                if (!ValidationHelpers.IsValid(bookDetail))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your book is not found" },
                    Configuration.Formatters.JsonFormatter);
                }
                var bookImage = bookImageBUS.GetAll(id).Select(x => new BookImageDTO()
                {
                    ID = x.ID,
                    BookUrl = x.ImageUrl,
                    BookID = x.BookID
                })?.ToArray();
                book.BookDetail = new BookDetailDTO(bookDetail);
                book.BookImages = bookImage;
                return Request.CreateResponse(HttpStatusCode.OK,
                    book, Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Gets
    }
}