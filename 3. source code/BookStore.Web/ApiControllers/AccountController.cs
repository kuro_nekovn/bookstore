﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class AccountController : ApiController
    {
        // GET api/<controller>
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AccountController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IAccountBUS accountBUS;

        private readonly ICustomerBUS customerBUS;

        /// <summary>
        /// 
        /// </summary>
        public AccountController()
        {
            accountBUS = new AccountBUS();
            customerBUS = new CustomerBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adsBUS"></param>
        public AccountController(IAccountBUS accountBUS, ICustomerBUS customerBUS)
        {
            this.accountBUS = accountBUS;
            this.customerBUS = customerBUS;
        }

        #endregion Contructors

        #region login
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Authenticate([FromBody] AccountViewModel account)
        {
            try
            {
                var loginResponse = new LoginResponse();
                Customer emp = null;

                IHttpActionResult response;
                HttpResponseMessage responseMsg = new HttpResponseMessage();
                if (account != null)
                {
                    emp = accountBUS.ShopLogin(account.Email, account.Password);
                }

                // if credentials are valid
                if (emp != null)
                {
                    string token = createToken(emp);
                    account.Password = token;
                    //return the token
                    return Ok<AccountViewModel>(account);
                }
                else
                {
                    // if credentials are not valid send unauthorized status code in response
                    loginResponse.responseMsg.StatusCode = HttpStatusCode.Unauthorized;
                    loginResponse.responseMsg.Content = new StringContent("Your Email or password is invalid.");
                    response = ResponseMessage(loginResponse.responseMsg);
                    return response;
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private string createToken(Customer user)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);
            var tokenHandler = new JwtSecurityTokenHandler();           

            const string sec = "jdsh564ashdjashjsadwwwoaoisdajs8fd5318192b0a75f201d8b3727429090fb3375912343e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);

            //create a identity and add claims to the user which we want to log in
            List<Claim> allClaim = new List<Claim>();
            allClaim.Add(new Claim(ClaimTypes.Name, user.Email));
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(allClaim);
            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "Bookstore", subject: claimsIdentity,audience: "Bookstore", notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
        #endregion login

        #region change password

        // api/account/ChangePassword
        [HttpPost]
        [Authorize]
        public HttpResponseMessage ChangePassword(AccountViewModel model)
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                if (identityClaims != null)
                {
                    var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                    var result = accountBUS.ChangeCustomerPassword(-1, claimEmail.Value, model.Password);
                    if (result)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, result, Configuration.Formatters.JsonFormatter);
                    }
                }                
                return Request.CreateResponse(HttpStatusCode.BadRequest,  "Change Password failed" , Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion change password

        #region registration


        [HttpPost]
        public HttpResponseMessage Registration(CustomerDTO customer)
        {
            try
            {
                customer = customerBUS.Create(customer);
                if (customer != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, customer,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Đăng kí thất bại.",
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }


        #endregion registration
    }
}