﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class BlogController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BlogController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IBlogBUS blogBUS;

        /// <summary>
        /// 
        /// </summary>
        public BlogController()
        {
            blogBUS = new BlogBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blogBUS"></param>
        public BlogController(IBlogBUS blogBUS)
        {
            this.blogBUS = blogBUS;
        }

        #endregion Contructors

        #region get

        // GET admin/api/blog/getall
        [HttpGet]
        public HttpResponseMessage GetNewBlogs()
        {
            try
            {
                var data = blogBUS.GetAll().OrderByDescending(x => x.DateCreate).Take(5);
                List<BlogShopModel> result = data.Select(x => new BlogShopModel(x)).ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    result.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/blog/Blogs
        [HttpGet]
        public HttpResponseMessage Blogs(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<BlogShopModel> result = blogBUS.BlogShoppingPage(ref page, keyword);
                var pagingData = new DataPagination<BlogShopModel>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET api/blog/getBlog/id
        [HttpGet]
        public HttpResponseMessage GetBlog(int id)
        {
            try
            {
                Blog blog = blogBUS.GetAll().FirstOrDefault(x => x.ID == id);
                if (ValidationHelpers.IsValid(blog))
                {
                    var result = new BlogShopModel();
                    result.ID = blog.ID;
                    result.Title = blog.Title;
                    result.Sumary = blog.Content;
                    result.TotalComment = blog.BlogReviews.Count;
                    result.UserCreateName = blog.Employee.LastName + " " + blog.Employee.FirstName;
                    result.BlogImg = blog.BlogImg;
                    result.CreateDate = blog.DateCreate;
                    return Request.CreateResponse(HttpStatusCode.OK,
                    result, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    "Your blog is not found" ,
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion get
    }
}