﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class BlogReviewController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BlogReviewController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IBlogReviewBUS blogReviewBUS;

        /// <summary>
        /// 
        /// </summary>
        public BlogReviewController()
        {
            blogReviewBUS = new BlogReviewBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blogReviewBUS"></param>
        public BlogReviewController(IBlogReviewBUS blogReviewBUS)
        {
            this.blogReviewBUS = blogReviewBUS;
        }

        #endregion Contructors

        #region Gets

        // GET admin/api/blogReview/BlogReviews
        [HttpGet]
        public HttpResponseMessage BlogReviews(int CurrentPage = 1, int PageSize = 5, string keyword = null, int blogID = 0)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<BlogReviewDTO> result = blogReviewBUS.BlogReviewShoppingPage(ref page, keyword, blogID);
                var pagingData = new DataPagination<BlogReviewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }
        #endregion Gets

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(BlogReviewDTO blogReview)
        {
            try
            {
                blogReview = blogReviewBUS.Create(blogReview);
                if (blogReview != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, blogReview,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating blogReview have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create

    }
}