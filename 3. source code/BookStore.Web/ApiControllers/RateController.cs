﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class RateController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(RateController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IRateBUS rateBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBUS customerBUS;

        /// <summary>
        /// 
        /// </summary>
        public RateController()
        {
            rateBUS = new RateBUS();
            customerBUS = new CustomerBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rateBUS"></param>
        public RateController(IRateBUS rateBUS, ICustomerBUS customerBUS)
        {
            this.rateBUS = rateBUS;
            this.customerBUS = customerBUS;
        }

        #endregion Contructors

        #region get

        // GET admin/api/rate/Rates
        [HttpGet]
        public HttpResponseMessage ShopRates(int CurrentPage = 1, int PageSize = 5, string keyword = null, int bookID = 0, string customerName = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                customerName = customerName == "undefined" ? null : customerName;
                List<RateShopDTO> result = rateBUS.RateShoppingPage(ref page, keyword, bookID, customerName);
                var pagingData = new DataPagination<RateShopDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        [HttpGet]
        public HttpResponseMessage NewRate()
        {
            try
            {
                List<Rate> rates = rateBUS.GetAll().OrderByDescending(x => x.DateCreate).ToList();
                if (rates.Count > 0)
                {
                    List<NewRateViewModel> data = new List<NewRateViewModel>();
                    for (int i = 0; i < 2 && i < rates.Count; i++)
                    {
                        data.Add(new NewRateViewModel(rates[i]));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK,
                   data.ToJArray(), Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    "Không tìm thấy bình luận nào", Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }


        #endregion get

        #region Create
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Create(RateDTO rate)
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                var emp = customerBUS.GetByEmail(claimEmail.Value, true);
                rate.CustomerID = emp.ID;
                rate = rateBUS.Create(rate);
                if (rate != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, rate,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating rate have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
        #endregion Create
    }
}