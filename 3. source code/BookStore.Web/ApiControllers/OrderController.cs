﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Areas.Admin.Models;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Security;

namespace BookStore.Web.ApiControllers
{
    [Authorize]
    public class OrderController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(OrderController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IOrderBUS orderBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBUS customerBUS;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOrderDetailBUS orderDetailBUS;

        /// <summary>
        /// 
        /// </summary>
        public OrderController()
        {
            orderBUS = new OrderBUS();
            customerBUS = new CustomerBUS();
            orderDetailBUS = new OrderDetailBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderBUS"></param>
        public OrderController(IOrderBUS orderBUS, IOrderDetailBUS orderDetailBUS, ICustomerBUS customerBUS)
        {
            this.orderBUS = orderBUS;
            this.orderDetailBUS = orderDetailBUS;
            this.customerBUS = customerBUS;
        }

        #endregion Contructors

        #region Get

        // GET admin/api/order/getall
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var data = orderBUS.GetAll().ToList();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/order/Orders
        [HttpGet]
        public HttpResponseMessage Orders(int CurrentPage = 1, int PageSize = 5)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                var identityClaims = (ClaimsIdentity)User.Identity;
                var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                List<OrderViewDTO> result = orderBUS.OrderShoppingPage(ref page, claimEmail.Value);
                var pagingData = new DataPagination<OrderViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/order/getOrder/id
        [HttpGet]
        public HttpResponseMessage GetOrder(int id)
        {
            try
            {
                var order = orderBUS.GetOrderDetailByOrderID(id);
                var identityClaims = (ClaimsIdentity)User.Identity;
                var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                if (order[0].Order.Customer.Email == claimEmail.Value)
                {
                    var result = order.Select(x => new OrderShopModel(x)).ToList();
                    return Request.CreateResponse(HttpStatusCode.OK,
                        result.ToJArray(), Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden,
                   "You don't have any role for getting this entry",
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

       
        #endregion Get

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(OrderViewModel order)
        {
            try
            {
                OrderDTO orderDTO = new OrderDTO();
                orderDTO.Address = order.Address;
                var identityClaims = (ClaimsIdentity)User.Identity;
                var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                var emp = customerBUS.GetByEmail(claimEmail.Value, true);
                orderDTO.CustomerID = emp.ID;
                orderDTO.EmployeeID = new Random().Next(10) + 1;
                orderDTO.TotalPrice = order.TotalPrice;
                orderDTO = orderBUS.Create(orderDTO);
                if (orderDTO == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        new { Message = "Creating order have been failed" },
                        Configuration.Formatters.JsonFormatter);
                }
                order.OrderID = orderDTO.ID;
                var result = orderDetailBUS.Create(order.OrderDetailModel, orderDTO.ID);
                if (result == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                       new { Message = "Creating detail order have been failed" },
                       Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.OK, order,
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create
    }
}