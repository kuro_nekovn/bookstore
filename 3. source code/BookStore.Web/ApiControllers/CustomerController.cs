﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class CustomerController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(CustomerController));

        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBUS customerBUS;

        /// <summary>
        /// 
        /// </summary>
        public CustomerController()
        {
            customerBUS = new CustomerBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBUS"></param>
        public CustomerController(ICustomerBUS customerBUS)
        {
            this.customerBUS = customerBUS;
        }

        #endregion Contructors

        #region Create

        [HttpPost]
        public HttpResponseMessage Create(CustomerDTO customer)
        {
            try
            {
                customer = customerBUS.Create(customer);
                if (customer != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, customer,
                        Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                   new { Message = "Creating customer have been failed" },
                   Configuration.Formatters.JsonFormatter);

            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Create

        #region Edit

        // post api/customer/getCustomer/id
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Edit(CustomerDTO customer)
        {
            try
            {
                string name = customer.FirstName + " " + customer.LastName;
                customer.ID = 1;
                customer = customerBUS.Edit(customer);
                if (ValidationHelpers.IsValid(customer))
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        customer, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                new { Message = "Edit customer " + name + " have been failed" },
                Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Edit

        #region Get

        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetCustomer()
        {
            try
            {
                var identityClaims = (ClaimsIdentity)User.Identity;
                if (identityClaims != null)
                {
                    var claimEmail = identityClaims.FindFirst(ClaimTypes.Name);
                    var customer = customerBUS.GetByEmail(claimEmail.Value, false);
                    if (ValidationHelpers.IsValid(customer))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                        customer, Configuration.Formatters.JsonFormatter);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your customer is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Get
    }
}