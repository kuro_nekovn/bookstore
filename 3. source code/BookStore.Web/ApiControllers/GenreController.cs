﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class GenreController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(GenreController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IGenreBUS genreBUS;

        /// <summary>
        /// 
        /// </summary>
        public GenreController()
        {
            genreBUS = new GenreBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="genreBUS"></param>
        public GenreController(IGenreBUS genreBUS)
        {
            this.genreBUS = genreBUS;
        }

        #endregion Contructors

        #region Gets

        // GET admin/api/author/getall
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = genreBUS.GetItem();
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        #endregion Gets
    }
}