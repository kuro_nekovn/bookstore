﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class AuthorController : ApiController
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AuthorController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IAuthorBUS authorBUS;

        /// <summary>
        /// 
        /// </summary>
        public AuthorController()
        {
            authorBUS = new AuthorBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorBUS"></param>
        public AuthorController(IAuthorBUS authorBUS)
        {
            this.authorBUS = authorBUS;
        }

        #endregion Contructors

        #region get
        [HttpGet]
        public HttpResponseMessage GetRandomAuthor()
        {
            try
            {
                Author data = authorBUS.GetRandomAuthor();
                AuthorAndBookViewModel result = new AuthorAndBookViewModel();
                result.AuthorInfomation.ID = data.ID;
                result.AuthorInfomation.ImgUrl = data.ImgUrl;
                result.AuthorInfomation.LastName = data.LastName;
                result.AuthorInfomation.FirstName = data.FirstName;
                result.AuthorInfomation.Sumary = data.Sumary;
                result.AuthorInfomation.Sex = data.Sex;
                List<Book> books = data.Books?.ToList();
                if (books != null)
                {
                    BookViewDTO book;
                    for (int i = 0; i < 4 && data.Books.Count > i; i++)
                    {
                        book = new BookViewDTO();
                        book.ImgUrl = books[i].ImgUrl;
                        book.Name = books[i].Name;
                        book.Price = books[i].Price;
                        book.Sumary = books[i].Sumary.Length < 150 ? books[i].Sumary : books[i].Sumary.Substring(0, 150);
                        book.Total = books[i].Total;
                        result.BookInformation.Add(book);
                    }                   
                }
                
                return Request.CreateResponse(HttpStatusCode.OK,
                    result, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex.Message, Configuration.Formatters.JsonFormatter);
            }
        }
        // GET admin/api/author/getall
        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            try
            {
                var data = authorBUS.GetItem().GetRange(0, 5);
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJArray(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }

        // GET admin/api/author/Authors
        [HttpGet]
        public HttpResponseMessage Authors(int CurrentPage = 1, int PageSize = 5, string keyword = null)
        {
            try
            {
                Pagination page = new Pagination();
                page.CurrentPage = CurrentPage;
                page.PageSize = PageSize;
                keyword = keyword == "undefined" ? null : keyword;
                List<AuthorViewDTO> result = authorBUS.AuthorPage(ref page, keyword);
                var pagingData = new DataPagination<AuthorViewDTO>()
                {
                    Page = page,
                    Items = result
                };
                var data = new DataTransfeViewModel()
                {
                    Data = pagingData
                };
                return Request.CreateResponse(HttpStatusCode.OK,
                    data.ToJObject(), Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
        #endregion get
    }
}