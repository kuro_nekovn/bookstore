﻿using BookStore.Business;
using BookStore.Business.Interfaces;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;
using BookStore.DataTransfer;
using BookStore.Entities;
using BookStore.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookStore.Web.ApiControllers
{
    public class AdvertisementController : ApiController
    {
		#region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AdvertisementController));

        /// <summary>
        /// 
        /// </summary>
        private readonly IAdvertisementBUS adsBUS;

        /// <summary>
        /// 
        /// </summary>
        public AdvertisementController()
        {
            adsBUS = new AdvertisementBUS();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adsBUS"></param>
        public AdvertisementController(IAdvertisementBUS adsBUS)
        {
            this.adsBUS = adsBUS;
        }
		
		  #endregion Contructors

        #region Get

       

        // GET admin/api/ads/getAdvertisement/id
        [HttpGet]
        public HttpResponseMessage GetAdvertisements()
        {
            try
            {
                DateTime now = DateTime.Now;
                List<Advertisement> ads = adsBUS.GetAll().FindAll(x => x.DateRelease < now && x.DateExpire > now);
                if (ValidationHelpers.IsValid(ads))
                {
                    List<AdvertisementDTO> result = new List<AdvertisementDTO>();
                    for (int i = 0; i < ads.Count && i < 6; i++)
                    {
                        result.Add(new AdvertisementDTO(ads[i]));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK,
                    result.ToJArray(), Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new { Message = "Your ads is not found" },
                    Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    ex, Configuration.Formatters.JsonFormatter);
            }
        }
		
		#endregion Get
    }
}