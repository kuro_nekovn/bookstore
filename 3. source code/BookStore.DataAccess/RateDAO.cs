﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class RateDAO : IRateDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(RateDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of RateDAO
        /// </summary>
        public RateDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of RateDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public RateDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Rate Create(Rate rate)
        {
            try
            {
                if (ValidationHelpers.IsValid(rate))
                {
                    rate = _context.Rates.Add(rate);
                    var book = _context.Books.FirstOrDefault(x => x.ID == rate.BookID);
                    book.Rating = _context.Rates.Where(x => x.BookID == book.ID).Sum(s => s.Rating) / _context.Rates.Where(x => x.BookID == book.ID).Count();
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return rate;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int rateID)
        {
            try
            {
                var rate = _context.Rates.SingleOrDefault(x => x.ID == rateID);
                if (rate != null)
                {
                    var book = _context.Books.FirstOrDefault(x => x.ID == rate.BookID);
                    _context.Rates.Remove(rate);
                    int count = _context.Rates.Where(x => x.BookID == book.ID).Count() > 0 ? _context.Rates.Where(x => x.BookID == book.ID).Count() : 1;
                    book.Rating = _context.Rates.Where(x => x.BookID == book.ID).Sum(s => s.Rating) / count;
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Rate Edit(RateDTO rate)
        {
            try
            {
                if (ValidationHelpers.IsValid(rate))
                {
                    var editRate = _context.Rates.SingleOrDefault(x => x.ID == rate.ID);
                    if (editRate != null)
                    {
                        editRate.Title = rate.Title;
                        editRate.Content = rate.Content;
                        editRate.Rating = rate.Rating;
                        var book = _context.Books.FirstOrDefault(x => x.ID == editRate.BookID);
                        book.Rating = _context.Rates.Where(x => x.BookID == book.ID).Sum(s => s.Rating) / _context.Rates.Where(x => x.BookID == book.ID).Count();
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editRate;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Rate> GetAll(string keyword, string bookID, string customerID)
        {
            try
            {
                return _context.Rates.ToList()
                    .FindAll(x => VietnameseCharExtension.CompareVNChar(x.Title, keyword)
                    && VietnameseCharExtension.CompareVNChar(x.Book.Name, bookID)
                     && VietnameseCharExtension.CompareVNChar(x.Customer.FirstName + " " + x.Customer.LastName, customerID));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Rate GetByID(int id)
        {
            try
            {
                return _context.Rates.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
