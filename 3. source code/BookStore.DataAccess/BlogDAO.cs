﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class BlogDAO : IBlogDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BlogDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of BlogDAO
        /// </summary>
        public BlogDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of BlogDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public BlogDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Blog Create(Blog blog)
        {
            try
            {
                if (ValidationHelpers.IsValid(blog))
                {
                    blog = _context.Blogs.Add(blog);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return blog;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int blogID)
        {
            try
            {
                var blog = _context.Blogs.SingleOrDefault(x => x.ID == blogID);
                if (blog != null)
                {
                    _context.BlogReviews.RemoveRange(_context.BlogReviews.Where(x => x.BlogID == blogID).ToList());
                    _context.Blogs.Remove(blog);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Blog Edit(BlogDTO blog)
        {
            try
            {
                if (ValidationHelpers.IsValid(blog))
                {
                    var editBlog = _context.Blogs.SingleOrDefault(x => x.ID == blog.ID);
                    if (editBlog != null)
                    {
                        editBlog.BlogImg = blog.BlogImg;
                        editBlog.Sumary = blog.Sumary;
                        editBlog.Title   = blog.Title;
                        editBlog.Content = blog.Content;
                        editBlog.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editBlog;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Blog> GetAll(string keyword)
        {
            try
            {
                List<Blog> blogs = _context.Blogs.ToList();
                return blogs.FindAll(x => VietnameseCharExtension.CompareVNChar(x.Title, keyword)
                    || VietnameseCharExtension.CompareVNChar(x.Employee.LastName + " " + x.Employee.FirstName, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Blog GetByID(int id)
        {
            try
            {
                return _context.Blogs.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
