﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class GenreDAO : IGenreDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(GenreDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of GenreDAO
        /// </summary>
        public GenreDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of GenreDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public GenreDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Genre Create(Genre genre)
        {
            try
            {
                if (ValidationHelpers.IsValid(genre))
                {
                    genre = _context.Genres.Add(genre);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return genre;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int genreID)
        {
            try
            {
                var genre = _context.Genres.SingleOrDefault(x => x.ID == genreID);
                if (genre != null)
                {
                    _context.Genres.Remove(genre);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Genre Edit(GenreDTO genre)
        {
            try
            {
                if (ValidationHelpers.IsValid(genre))
                {
                    var editGenre = _context.Genres.SingleOrDefault(x => x.ID == genre.ID);
                    if (editGenre != null)
                    {
                        editGenre.Name = genre.Name;
                        editGenre.Description = genre.Description;
                        editGenre.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editGenre;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Genre> GetAll(string keyword)
        {
            try
            {
                return _context.Genres.ToList().FindAll(x => VietnameseCharExtension.CompareVNChar(x.Name, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Genre GetByID(int id)
        {
            try
            {
                return _context.Genres.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
