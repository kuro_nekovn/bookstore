﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class OrderDetailDAO : IOrderDetailDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(OrderDetailDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of OrderDetailDAO
        /// </summary>
        public OrderDetailDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of OrderDetailDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public OrderDetailDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public OrderDetail Create(OrderDetail orderDetail)
        {
            try
            {
                if (ValidationHelpers.IsValid(orderDetail))
                {
                    orderDetail = _context.OrderDetails.Add(orderDetail);
                    var updateBook = _context.Books.Single(x => x.ID == orderDetail.BookID);
                    updateBook.Total -= orderDetail.Quantity;
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return orderDetail;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int orderDetailID, int bookID)
        {
            try
            {
                var orderDetail = _context.OrderDetails.SingleOrDefault(x => 
                    x.OrderID == orderDetailID && (x.BookID == bookID || bookID < 1));
                if (orderDetail != null)
                {
                    var updateBook = _context.Books.Single(x => x.ID == bookID);
                    updateBook.Total += orderDetail.Quantity;
                    _context.OrderDetails.Remove(orderDetail);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public OrderDetail Edit(OrderDetailDTO orderDetail)
        {
            try
            {
                if (ValidationHelpers.IsValid(orderDetail))
                {
                    var editOrderDetail = GetByID(orderDetail.OrderID, orderDetail.BookID);
                    if (editOrderDetail != null)
                    {
                        editOrderDetail.Quantity = orderDetail.Quantity;
                        int balance = orderDetail.Discount - editOrderDetail.Discount;
                        var updateBook = _context.Books.Single(x => x.ID == orderDetail.BookID);
                        updateBook.Total += balance;
                        editOrderDetail.Discount = orderDetail.Discount;
                        editOrderDetail.TotalPrice = orderDetail.TotalPrice;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editOrderDetail;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<OrderDetail> GetAll(int orderID, string bookName)
        {
            try
            {
                return _context.OrderDetails.ToList().FindAll(x =>
                     VietnameseCharExtension.CompareVNChar(x.Book.Name, bookName)
                     && (orderID < 1 || x.OrderID == orderID));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public OrderDetail GetByID(int orderDetailID, int bookID)
        {
            try
            {
                return _context.OrderDetails.FirstOrDefault(x => x.OrderID == orderDetailID && x.BookID == bookID);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
