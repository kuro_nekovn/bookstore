﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class PublisherDAO : IPublisherDAO
    {
        #region Contructors

        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(PublisherDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        public PublisherDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public PublisherDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Publisher Create(Publisher publisher)
        {
            try
            {
                if (ValidationHelpers.IsValid(publisher))
                {
                    publisher = _context.Publishers.Add(publisher);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return publisher;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete

        public bool Delete(int publisherID)
        {
            try
            {
                var publisher = _context.Publishers.SingleOrDefault(x => x.ID == publisherID);
                if (publisher != null)
                {
                    publisher.IsActive = false;
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Publisher Edit(PublisherDTO publisher)
        {
            try
            {
                if (ValidationHelpers.IsValid(publisher))
                {
                    var editPublisher = _context.Publishers.SingleOrDefault(x => x.ID == publisher.ID);
                    if (editPublisher != null)
                    {
                        editPublisher.ImgUrl = publisher.ImgUrl;
                        editPublisher.Address = publisher.Address;
                        editPublisher.Name = publisher.Name;
                        editPublisher.Phone = publisher.Phone;
                        editPublisher.ImgUrl = publisher.ImgUrl;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editPublisher;
                        }
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Get
        

        public List<Publisher> GetAll(string keyword)
        {
            try
            {
                return _context.Publishers.ToList().FindAll(x => VietnameseCharExtension.CompareVNChar(x.Name, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Publisher GetByID(int id)
        {
            try
            {
                return _context.Publishers.FirstOrDefault(x => x.ID == id && x.IsActive == true);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
