﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class EmployeeDAO : IEmployeeDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(EmployeeDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        public EmployeeDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public EmployeeDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Employee Create(Employee employee)
        {
            try
            {
                if (ValidationHelpers.IsValid(employee))
                {
                    employee = _context.Employees.Add(employee);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return employee;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int employeeID)
        {
            try
            {
                var employee = _context.Employees.SingleOrDefault(x => x.ID == employeeID);
                if (employee != null)
                {
                    employee.IsActive = false;
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Employee Edit(EmployeeDTO employee)
        {
            try
            {
                if (ValidationHelpers.IsValid(employee))
                {
                    var editEmployee = _context.Employees.SingleOrDefault(x => x.ID == employee.ID);
                    if (editEmployee != null)
                    {
                        editEmployee.FirstName = employee.FirstName;
                        editEmployee.LastName = employee.LastName;
                        editEmployee.Phone = employee.Phone;
                        editEmployee.ImgUrl = employee.ImgUrl;
                        editEmployee.Address = employee.Address;
                        editEmployee.Birthday = employee.Birthday;
                        editEmployee.Sex = employee.Sex;
                        editEmployee.Email = employee.Email;
                        editEmployee.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editEmployee;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Get

        public List<Employee> GetAll(string keyword)
        {
            try
            {
                return _context.Employees.ToList().FindAll(x => VietnameseCharExtension.CompareVNChar(x.LastName + " " + x.FirstName, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Employee GetByID(int id, bool includeDelete)
        {
            try
            {
                return _context.Employees.FirstOrDefault(x => x.ID == id && (x.IsActive == true || includeDelete));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Employee GetByEmail(string email)
        {
            try
            {
                return _context.Employees.FirstOrDefault(x => x.Email == email && x.IsActive == true);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
