﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class AdvertisementDAO : IAdvertisementDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AdvertisementDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of AdvertisementDAO
        /// </summary>
        public AdvertisementDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of AdvertisementDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public AdvertisementDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Advertisement Create(Advertisement ads)
        {
            try
            {
                if (ValidationHelpers.IsValid(ads))
                {
                    ads = _context.Advertisements.Add(ads);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return ads;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int adsID)
        {
            try
            {
                var ads = _context.Advertisements.SingleOrDefault(x => x.ID == adsID);
                if (ads != null)
                {
                    _context.Advertisements.Remove(ads);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Advertisement Edit(AdvertisementDTO ads)
        {
            try
            {
                if (ValidationHelpers.IsValid(ads))
                {
                    var editAdvertisement = _context.Advertisements.SingleOrDefault(x => x.ID == ads.ID);
                    if (editAdvertisement != null)
                    {
                        editAdvertisement.Description = ads.Description;
                        editAdvertisement.ImgUrl= ads.ImgUrl;
                        editAdvertisement.DateExpire = ads.DateExpire;
                        editAdvertisement.DateRelease   = ads.DateRelease;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editAdvertisement;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Advertisement> GetAll(string keyword) 
        {
            try
            {
                List<Advertisement> advertisements = _context.Advertisements.ToList();
                return advertisements.FindAll(x => VietnameseCharExtension.CompareVNChar(x.Description,keyword));                   
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Advertisement GetByID(int id)
        {
            try
            {
                return _context.Advertisements.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
