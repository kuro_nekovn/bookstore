﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;

namespace BookStore.DataAccess
{
    public class EmployeeRoleDAO : IEmployeeRoleDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(EmployeeRoleDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of EmployeeRoleDAO
        /// </summary>
        public EmployeeRoleDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of EmployeeRoleDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public EmployeeRoleDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region edit
        public Employee EditEmployeeRole(EmployeeRoleDTO employeeRoleDTO)
        {
            Employee employee = _context.Employees.FirstOrDefault(x => x.ID == employeeRoleDTO.ID);
            if (employee == null)
            {
                return null;
            }
            List<Role> allRoles = _context.Roles.ToList();
            if (employeeRoleDTO.IsAdmin != (employee.Roles.SingleOrDefault(x => x.ID == 1) != null))
            {
                if (employeeRoleDTO.IsAdmin)
                {
                    employee.Roles.Add(allRoles.Single(x => x.ID == 1));
                }
                else
                {
                    employee.Roles.Remove(allRoles.Single(x => x.ID == 1));
                }
            }
            if (employeeRoleDTO.IsHr != (employee.Roles.SingleOrDefault(x => x.ID == 2) != null))
            {
                if (employeeRoleDTO.IsHr)
                {
                    employee.Roles.Add(allRoles.Single(x => x.ID == 2));
                }
                else
                {
                    employee.Roles.Remove(allRoles.Single(x => x.ID == 2));
                }
            }
            if (employeeRoleDTO.IsSaleman != (employee.Roles.SingleOrDefault(x => x.ID == 3) != null))
            {
                if (employeeRoleDTO.IsSaleman)
                {
                    employee.Roles.Add(allRoles.Single(x => x.ID == 3));
                }
                else
                {
                    employee.Roles.Remove(allRoles.Single(x => x.ID == 3));
                }
            }
            if (employeeRoleDTO.IsStockKeeper != (employee.Roles.SingleOrDefault(x => x.ID == 4) != null))
            {
                if (employeeRoleDTO.IsStockKeeper)
                {
                    employee.Roles.Add(allRoles.Single(x => x.ID == 4));
                }
                else
                {
                    employee.Roles.Remove(allRoles.Single(x => x.ID == 4));
                }
            }
            if (_context.SaveChanges() > 0)
            {
                return employee;
            }
            return null;
        }
        #endregion edit

    }

}
