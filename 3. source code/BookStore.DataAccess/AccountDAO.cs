﻿using BookStore.Commons.Helpers;
using BookStore.DataAccess.Interfaces;
using BookStore.DataTransfer;
using BookStore.Entities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccess
{
    public class AccountDAO : IAccountDAO
    {
        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(CustomerDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        public AccountDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public AccountDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Get Role

        public List<Role> GetAllRoles()
        {
            try
            {
                var result = _context.Roles;
                return result.ToList();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }
        public Role GetRoleByID(int ID, string roleName)
        {
            try
            {
                var result = _context.Roles.SingleOrDefault(x => x.ID == ID || x.Name == roleName);
                return result;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get Role

        #region Password
        public bool ChangeEmployeePassword(int ID, string Email, string Password)
        {
            try
            {
                var result = _context.Employees.SingleOrDefault(x => x.ID == ID || Email == x.Email);
                result.Password = Password;
                return _context.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public bool ChangeCustomerPassword(int ID, string Email, string Password)
        {
            try
            {
                var result = _context.Customers.SingleOrDefault(x => x.ID == ID || Email == x.Email);
                result.Password = Password;
                return _context.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }
        #endregion Password

        public Employee GetEmployee(string email)
        {
            try
            {
                var result = _context.Employees.FirstOrDefault(x => x.Email == email);
                return result;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Customer GetCustomer(string email)
        {
            try
            {
                var result = _context.Customers.FirstOrDefault(x => x.Email == email);
                return result;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }
    }
}
