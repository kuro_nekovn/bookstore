﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class OrderDAO : IOrderDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(OrderDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of OrderDAO
        /// </summary>
        public OrderDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of OrderDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public OrderDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Order Create(Order order)
        {
            try
            {
                if (ValidationHelpers.IsValid(order))
                {
                    order = _context.Orders.Add(order);                    
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return order;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int orderID)
        {
            try
            {
                var order = _context.Orders.SingleOrDefault(x => x.ID == orderID);
                if (order != null)
                {
                    _context.OrderDetails.RemoveRange(_context.OrderDetails.Where(x => x.OrderID == orderID).ToList());
                    _context.Complaints.RemoveRange(_context.Complaints.Where(x => x.OrderID == orderID).ToList());
                    _context.Orders.Remove(order);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Order Edit(OrderDTO order)
        {
            try
            {
                if (ValidationHelpers.IsValid(order))
                {
                    var editOrder = _context.Orders.SingleOrDefault(x => x.ID == order.ID);
                    if (editOrder != null)
                    {
                        editOrder.Address = order.Address;
                        editOrder.TotalPrice = order.TotalPrice;
                        editOrder.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editOrder;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Order> GetAll(string employeeID, string customerID)
        {
            try
            {
                return _context.Orders.ToList().FindAll(x =>
                     VietnameseCharExtension.CompareVNChar(x.Employee.LastName + " " + x.Employee.FirstName, employeeID)
                     && VietnameseCharExtension.CompareVNChar(x.Customer.LastName + " " + x.Customer.FirstName, customerID));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }
        public List<OrderDetail> GetOrderDetailsByOrderID(int ID)
        {
            return _context.OrderDetails.Where(x => x.OrderID == ID).ToList();
        }
        public Order GetByID(int id)
        {
            try
            {
                return _context.Orders.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
