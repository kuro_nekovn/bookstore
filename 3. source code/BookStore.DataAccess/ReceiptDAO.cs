﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class ReceiptDAO : IReceiptDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ReceiptDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of ReceiptDAO
        /// </summary>
        public ReceiptDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of ReceiptDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public ReceiptDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Receipt Create(Receipt receipt)
        {
            try
            {
                if (ValidationHelpers.IsValid(receipt))
                {
                    receipt = _context.Receipts.Add(receipt);                    
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return receipt;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int receiptID)
        {
            try
            {
                var receipt = _context.Receipts.SingleOrDefault(x => x.ID == receiptID);
                if (receipt != null)
                {
                    _context.ReceiptDetails.RemoveRange(_context.ReceiptDetails.Where(x => x.ReceiptID == receiptID).ToList());
                    _context.Receipts.Remove(receipt);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Receipt Edit(ReceiptDTO receipt)
        {
            try
            {
                if (ValidationHelpers.IsValid(receipt))
                {
                    var editReceipt = _context.Receipts.SingleOrDefault(x => x.ID == receipt.ID);
                    if (editReceipt != null)
                    {
                        editReceipt.Description = receipt.Description;
                        editReceipt.TotalPrice = receipt.TotalPrice;
                        editReceipt.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editReceipt;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Receipt> GetAll(string publisherID, string employeeID)
        {
            try
            {
                return _context.Receipts.ToList().FindAll(x =>
                     VietnameseCharExtension.CompareVNChar(x.Employee.FirstName + " " + x.Employee.LastName, employeeID)
                     && VietnameseCharExtension.CompareVNChar(x.Publisher.Name, publisherID));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Receipt GetByID(int id)
        {
            try
            {
                return _context.Receipts.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
