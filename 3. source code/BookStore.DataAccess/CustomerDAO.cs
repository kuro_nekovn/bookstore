﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class CustomerDAO : ICustomerDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(CustomerDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        public CustomerDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of PushlisherDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public CustomerDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Customer Create(Customer customer)
        {
            try
            {
                if (ValidationHelpers.IsValid(customer))
                {
                    customer = _context.Customers.Add(customer);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return customer;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int customerID)
        {
            try
            {
                var customer = _context.Customers.SingleOrDefault(x => x.ID == customerID);
                if (customer != null)
                {
                    customer.IsActive = false;
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Customer Edit(CustomerDTO customer)
        {
            try
            {
                if (ValidationHelpers.IsValid(customer))
                {
                    var editCustomer = _context.Customers.SingleOrDefault(x => x.ID == customer.ID);
                    if (editCustomer != null)
                    {
                        editCustomer.FirstName = customer.FirstName;
                        editCustomer.LastName = customer.LastName;
                        editCustomer.Phone = customer.Phone;
                        editCustomer.ImgUrl = customer.ImgUrl;
                        editCustomer.Address = customer.Address;
                        editCustomer.Email = customer.Email;
                        editCustomer.Birthday = customer.Birthday;
                        editCustomer.Point = customer.Point;
                        editCustomer.Sex = customer.Sex;
                        editCustomer.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editCustomer;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Get

        public List<Customer> GetAll(string keyword)
        {
            try
            {
                return _context.Customers.ToList().FindAll(x => VietnameseCharExtension.CompareVNChar(x.LastName + " " + x.FirstName, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Customer GetByID(int id, bool includeDelete)
        {
            try
            {
                return _context.Customers.FirstOrDefault(x => x.ID == id && (x.IsActive == true || includeDelete));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Customer GetByEmail(string id, bool includeDelete)
        {
            try
            {
                return _context.Customers.FirstOrDefault(x => x.Email == id && (x.IsActive == true || includeDelete));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
