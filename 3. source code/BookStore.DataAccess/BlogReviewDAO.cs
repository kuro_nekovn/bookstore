﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class BlogReviewDAO : IBlogReviewDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BlogReviewDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of BlogReviewDAO
        /// </summary>
        public BlogReviewDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of BlogReviewDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public BlogReviewDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public BlogReview Create(BlogReview blogReview)
        {
            try
            {
                if (ValidationHelpers.IsValid(blogReview))
                {
                    blogReview = _context.BlogReviews.Add(blogReview);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return blogReview;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int blogReviewID)
        {
            try
            {
                var blogReview = _context.BlogReviews.SingleOrDefault(x => x.ID == blogReviewID);
                if (blogReview != null)
                {
                    _context.BlogReviews.Remove(blogReview);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public BlogReview Edit(BlogReviewDTO blogReview)
        {
            try
            {
                if (ValidationHelpers.IsValid(blogReview))
                {
                    var editBlogReview = _context.BlogReviews.SingleOrDefault(x => x.ID == blogReview.ID);
                    if (editBlogReview != null)
                    {
                        editBlogReview.Title = blogReview.Title;
                        editBlogReview.Email = blogReview.Email;
                        editBlogReview.Content = blogReview.Content;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editBlogReview;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<BlogReview> GetAll(string keyword, int blogID)
        {
            try
            {
                List<BlogReview> blogs = _context.BlogReviews.ToList();                     
                return blogs.FindAll(x => VietnameseCharExtension.CompareVNChar(x.Title, keyword) &&
                     (x.BlogID == blogID || blogID < 1));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public BlogReview GetByID(int id)
        {
            try
            {
                return _context.BlogReviews.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
