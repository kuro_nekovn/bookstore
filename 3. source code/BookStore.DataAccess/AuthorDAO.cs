﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class AuthorDAO : IAuthorDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(AuthorDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of AuthorDAO
        /// </summary>
        public AuthorDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of AuthorDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public AuthorDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Author Create(Author author)
        {
            try
            {
                if (ValidationHelpers.IsValid(author))
                {
                    author = _context.Authors.Add(author);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return author;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int authorID)
        {
            try
            {
                var author = _context.Authors.SingleOrDefault(x => x.ID == authorID);
                if (author != null)
                {
                    author.IsActive = false;
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Author Edit(AuthorDTO author)
        {
            try
            {
                if (ValidationHelpers.IsValid(author))
                {
                    var editAuthor = _context.Authors.SingleOrDefault(x => x.ID == author.ID);
                    if (editAuthor != null)
                    {
                        editAuthor.FirstName = author.FirstName;
                        editAuthor.LastName = author.LastName;
                        editAuthor.Phone = author.Phone;
                        editAuthor.ImgUrl = author.ImgUrl;
                        editAuthor.Address = author.Address;
                        editAuthor.Birthday = author.Birthday;
                        editAuthor.Content = author.Content;
                        editAuthor.Sex = author.Sex;
                        editAuthor.DateEdit = DateTime.Now;
                        editAuthor.Sumary = author.Sumary;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editAuthor;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Get

        public List<Author> GetAll(string keyword)
        {
            try
            {
                List<Author> authors = _context.Authors.ToList();
                return authors.FindAll(x => VietnameseCharExtension.CompareVNChar(x.LastName + " " + x.FirstName, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Author GetByID(int id)
        {
            try
            {
                return _context.Authors.FirstOrDefault(x => x.ID == id && x.IsActive == true);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
