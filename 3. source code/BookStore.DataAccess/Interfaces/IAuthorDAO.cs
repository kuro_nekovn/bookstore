﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IAuthorDAO
    {
        List<Author> GetAll(string keyword);
        Author Create(Author author);
        Author Edit(AuthorDTO author);
        bool Delete(int authorID);
        Author GetByID(int id);
    }
}
