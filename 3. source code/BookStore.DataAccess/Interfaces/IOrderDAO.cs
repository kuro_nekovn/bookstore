﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IOrderDAO
    {
        List<Order> GetAll(string employeeID, string customerID);
        List<OrderDetail> GetOrderDetailsByOrderID(int ID);
        Order Create(Order order);
        Order Edit(OrderDTO order);
        bool Delete(int orderID);
        Order GetByID(int id);
    }
}
