﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IAdvertisementDAO
    {
        List<Advertisement> GetAll(string keyword);
        Advertisement Create(Advertisement ads);
        Advertisement Edit(AdvertisementDTO ads);
        bool Delete(int id);
        Advertisement GetByID(int id);
    }
}
