﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IReceiptDAO
    {
        List<Receipt> GetAll(string employeeID, string publisherID);
        Receipt Create(Receipt receipt);
        Receipt Edit(ReceiptDTO receipt);
        bool Delete(int receiptID);
        Receipt GetByID(int id);
    }
}
