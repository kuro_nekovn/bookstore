﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IEmployeeDAO
    {
        List<Employee> GetAll(string keyword);
        Employee Create(Employee employee);
        Employee Edit(EmployeeDTO employee);
        bool Delete(int employeeID);
        Employee GetByID(int id, bool includeDelete);
        Employee GetByEmail(string email);
    }
}
