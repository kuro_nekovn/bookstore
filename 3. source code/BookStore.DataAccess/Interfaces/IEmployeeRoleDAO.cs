﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IEmployeeRoleDAO
    {
        Employee EditEmployeeRole(EmployeeRoleDTO employeeRoleDTO);
    }
}
