﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IBlogReviewDAO
    {
        List<BlogReview> GetAll(string keyword, int blogID);
        BlogReview Create(BlogReview blogReview);
        BlogReview Edit(BlogReviewDTO blogReview);
        bool Delete(int blogReviewID);
        BlogReview GetByID(int id);
    }
}
