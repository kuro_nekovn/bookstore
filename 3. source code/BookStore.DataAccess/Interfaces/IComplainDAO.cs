﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IComplainDAO
    {
        List<Complaint> GetAll(string keyword);
        Complaint Create(Complaint blog);
        Complaint Edit(ComplainDTO blog);
        bool Delete(int id);
        Complaint GetByID(int id);
    }
}
