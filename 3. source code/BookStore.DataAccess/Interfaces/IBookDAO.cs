﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IBookDAO
    {
        List<Book> GetAll(string bookName, string authorName, string publisherName);
        List<Book> GetShopAll(string bookName, int authorName, int publisherName, int genreID);
        Book Create(Book book);
        Book Edit(BookDTO book);
        Book UpdateTotal(int bookID, int balance);
        bool Delete(int id);
        Book GetByID(int id);
    }
}
