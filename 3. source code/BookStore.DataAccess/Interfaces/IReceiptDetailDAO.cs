﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IReceiptDetailDAO
    {
        List<ReceiptDetail> GetAll(int receiptDetailID, string bookID);
        ReceiptDetail Create(ReceiptDetail receiptDetail);
        ReceiptDetail Edit(ReceiptDetailDTO receiptDetail);
        bool Delete(int receiptDetailID, int bookID);
        ReceiptDetail GetByID(int receiptDetailID, int bookID);
    }
}
