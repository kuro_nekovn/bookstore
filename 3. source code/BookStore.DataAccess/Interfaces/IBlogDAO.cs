﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IBlogDAO
    {
        List<Blog> GetAll(string keyword);
        Blog Create(Blog blog);
        Blog Edit(BlogDTO blog);
        bool Delete(int id);
        Blog GetByID(int id);
    }
}
