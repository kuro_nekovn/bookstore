﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IBookImageDAO
    {
        List<BookImage> GetAll(int bookID);
        BookImage Create(BookImage bookImage);
        BookImage Edit(BookImageDTO bookImage);
        bool Delete(int ID);
        BookImage GetByID(int ID);
    }
}
