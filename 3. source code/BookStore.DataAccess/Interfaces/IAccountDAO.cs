﻿using BookStore.DataTransfer;
using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Interfaces
{
    public interface IAccountDAO
    {
        #region Role
        List<Role> GetAllRoles();
        Role GetRoleByID(int ID, string roleName);
        #endregion Role

        #region Password
        bool ChangeEmployeePassword(int ID, string Email, string Password);
        bool ChangeCustomerPassword(int ID, string Email, string Password);
        #endregion Password
        Employee GetEmployee(string email);
        Customer GetCustomer(string email);
    }
}
