﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IGenreDAO
    {
        List<Genre> GetAll(string keyword);
        Genre Create(Genre genre);
        Genre Edit(GenreDTO genre);
        bool Delete(int genreID);
        Genre GetByID(int id);
    }
}
