﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IPublisherDAO
    {
        List<Publisher> GetAll(string keyword);
        Publisher Create(Publisher publisher);
        Publisher Edit(PublisherDTO publisher);
        bool Delete(int publisherID);
        Publisher GetByID(int id);
    }
}
