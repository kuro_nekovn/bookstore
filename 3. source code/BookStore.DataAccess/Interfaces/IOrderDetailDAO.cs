﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IOrderDetailDAO
    {
        List<OrderDetail> GetAll(int orderDetailID, string bookID);
        OrderDetail Create(OrderDetail orderDetail);
        OrderDetail Edit(OrderDetailDTO orderDetail);
        bool Delete(int orderDetailID, int bookID);
        OrderDetail GetByID(int orderDetailID, int bookID);
    }
}
