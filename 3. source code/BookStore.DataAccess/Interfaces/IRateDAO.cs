﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface IRateDAO
    {
        List<Rate> GetAll(string keyword, string bookID, string customerID);
        Rate Create(Rate rate);
        Rate Edit(RateDTO rate);
        bool Delete(int rateID);
        Rate GetByID(int id);
    }
}
