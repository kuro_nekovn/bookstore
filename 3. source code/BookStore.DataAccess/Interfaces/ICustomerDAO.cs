﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.DataAccess.Interfaces
{
    public interface ICustomerDAO
    {
        List<Customer> GetAll(string keyword);
        Customer Create(Customer customer);
        Customer Edit(CustomerDTO customer);
        bool Delete(int customerID);
        Customer GetByID(int id, bool includeDelete);
        Customer GetByEmail(string id, bool includeDelete);
    }
}
