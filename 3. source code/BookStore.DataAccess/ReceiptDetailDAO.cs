﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class ReceiptDetailDAO : IReceiptDetailDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ReceiptDetailDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of ReceiptDetailDAO
        /// </summary>
        public ReceiptDetailDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of ReceiptDetailDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public ReceiptDetailDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public ReceiptDetail Create(ReceiptDetail receiptDetail)
        {
            try
            {
                if (ValidationHelpers.IsValid(receiptDetail))
                {
                    receiptDetail = _context.ReceiptDetails.Add(receiptDetail);
                    var updateBook = _context.Books.Single(x => x.ID == receiptDetail.BookID);
                    updateBook.Total += receiptDetail.Quantity;
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return receiptDetail;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int receiptDetailID, int bookID)
        {
            try
            {
                var receiptDetail = _context.ReceiptDetails.SingleOrDefault(x => 
                    x.ReceiptID == receiptDetailID && (x.BookID == bookID || bookID < 1));
                if (receiptDetail != null)
                {
                    var updateBook = _context.Books.Single(x => x.ID == bookID);
                    updateBook.Total -= receiptDetail.Quantity;
                    _context.ReceiptDetails.Remove(receiptDetail);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public ReceiptDetail Edit(ReceiptDetailDTO receiptDetail)
        {
            try
            {
                if (ValidationHelpers.IsValid(receiptDetail))
                {
                    var editReceiptDetail = GetByID(receiptDetail.ReceiptID, receiptDetail.BookID);
                    if (editReceiptDetail != null)
                    {
                        editReceiptDetail.Quantity = receiptDetail.Quantity;
                        int balance = receiptDetail.Total - editReceiptDetail.Quantity;
                        var updateBook = _context.Books.Single(x => x.ID == receiptDetail.BookID);
                        updateBook.Total += balance;
                        editReceiptDetail.TotalPrice = receiptDetail.TotalPrice;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editReceiptDetail;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<ReceiptDetail> GetAll(int receiptID, string bookName)
        {
            try
            {
                return _context.ReceiptDetails.ToList().FindAll(x =>
                    VietnameseCharExtension.CompareVNChar(x.Book.Name, bookName)
                     && (receiptID < 1 || x.ReceiptID == receiptID));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public ReceiptDetail GetByID(int receiptDetailID, int bookID)
        {
            try
            {
                return _context.ReceiptDetails.FirstOrDefault(x => x.ReceiptID == receiptDetailID && x.BookID == bookID);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
