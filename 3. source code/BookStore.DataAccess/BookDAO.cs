﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class BookDAO : IBookDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BookDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of BookDAO
        /// </summary>
        public BookDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of BookDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public BookDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Book Create(Book book)
        {
            try
            {
                if (ValidationHelpers.IsValid(book))
                {
                    book = _context.Books.Add(book);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return book;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int bookID)
        {
            try
            {
                var book = _context.Books.SingleOrDefault(x => x.ID == bookID);
                if (book != null)
                {
                    book.IsActive = false;
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Book Edit(BookDTO book)
        {
            try
            {
                if (ValidationHelpers.IsValid(book))
                {
                    var editBook = _context.Books.SingleOrDefault(x => x.ID == book.ID);
                    if (editBook != null)
                    {
                        editBook.Name = book.Name;
                        editBook.Sumary = book.Sumary;
                        editBook.Content = book.Content;
                        editBook.ImgUrl = book.ImgUrl;
                        editBook.AuthorID = book.Author;
                        editBook.PublisherID = book.Publisher;
                        editBook.Status = book.Status;
                        editBook.GenreID = book.GenreID;
                        editBook.DateEdit = DateTime.Now;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editBook;
                        }
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Book Edit(Book book)
        {
            try
            {
                if (ValidationHelpers.IsValid(book))
                {
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return book;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Book UpdateTotal(int bookID, int balance)
        {
            var book = GetByID(bookID);
            if (book != null)
            {
                book.Total += balance;
                var result = _context.SaveChanges();
                if (result > 0)
                {
                    return book;
                }
            }
            return null;
        }

        #endregion Edit

        #region Get

        public List<Book> GetAll(string bookName, string authorName, string publisherName)
        {
            try
            {
                var result = _context.Books.ToList().FindAll(x =>
                    VietnameseCharExtension.CompareVNChar(x.Name,bookName)
                    && VietnameseCharExtension.CompareVNChar(x.Author.LastName + " " + x.Author.FirstName, authorName)
                    && VietnameseCharExtension.CompareVNChar(x.Publisher.Name, publisherName));
                return result;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public List<Book> GetShopAll(string bookName, int authorName, int publisherName, int genreID)
        {
            try
            {
                var result = _context.Books.ToList().FindAll(x =>
                    VietnameseCharExtension.CompareVNChar(x.Name, bookName)
                    && (x.AuthorID == authorName || authorName == 0)
                    && (x.PublisherID == publisherName || publisherName == 0)
                    && (x.GenreID == genreID || genreID == 0));
                return result;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Book GetByID(int id)
        {
            try
            {
                return _context.Books.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
