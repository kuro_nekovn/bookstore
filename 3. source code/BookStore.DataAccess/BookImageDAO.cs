﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;

namespace BookStore.DataAccess
{
    public class BookImageDAO : IBookImageDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(BookImageDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of BookImageDAO
        /// </summary>
        public BookImageDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of BookImageDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public BookImageDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public BookImage Create(BookImage bookImage)
        {
            try
            {
                if (ValidationHelpers.IsValid(bookImage))
                {
                    bookImage = _context.BookImages.Add(bookImage);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return bookImage;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int ID)
        {
            try
            {
                var bookImage = _context.BookImages.SingleOrDefault(x => 
                   x.ID == ID);
                if (bookImage != null)
                {
                    var updateBook = _context.Books.Single(x => x.ID == ID);
                    _context.BookImages.Remove(bookImage);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public BookImage Edit(BookImageDTO bookImage)
        {
            try
            {
                if (ValidationHelpers.IsValid(bookImage))
                {
                    var editBookImage = GetByID(bookImage.ID);
                    if (editBookImage != null)
                    {
                        editBookImage.ImageUrl = bookImage.BookUrl;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editBookImage;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<BookImage> GetAll(int bookID)
        {
            try
            {
                return _context.BookImages.Where(x => x.BookID == bookID || x.BookID < 1).ToList();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public BookImage GetByID(int ID)
        {
            try
            {
                return _context.BookImages.FirstOrDefault(x => x.ID == ID);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
