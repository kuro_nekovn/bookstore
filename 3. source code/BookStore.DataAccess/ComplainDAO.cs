﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.DataAccess.Interfaces;
using BookStore.Entities;
using BookStore.Commons.Helpers;

using log4net;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;

namespace BookStore.DataAccess
{
    public class ComplainDAO : IComplainDAO
    {

        #region Contructors
        /// <summary>
        /// write log of any errors or information
        /// </summary>
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ComplainDAO));

        /// <summary>
        /// Entities of database
        /// </summary>
        private readonly BookStoreOnlineEntities _context;

        /// <summary>
        /// contructor of ComplaintDAO
        /// </summary>
        public ComplainDAO()
        {
            _context = new BookStoreOnlineEntities();
        }

        /// <summary>
        /// contructor of ComplaintDAO
        /// </summary>
        /// <param name="_context">Entities of database</param>
        public ComplainDAO(BookStoreOnlineEntities _context)
        {
            this._context = _context;
        }

        #endregion Contructors

        #region Create

        public Complaint Create(Complaint complain)
        {
            try
            {
                if (ValidationHelpers.IsValid(complain))
                {
                    complain = _context.Complaints.Add(complain);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        return complain;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Create

        #region Delete
        public bool Delete(int complainID)
        {
            try
            {
                var complain = _context.Complaints.SingleOrDefault(x => x.ID == complainID);
                if (complain != null)
                {
                    _context.Complaints.Remove(complain);
                    var result = _context.SaveChanges();
                    return result > 0;
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Delete

        #region Edit

        public Complaint Edit(ComplainDTO complain)
        {
            try
            {
                if (ValidationHelpers.IsValid(complain))
                {
                    var editComplaint = _context.Complaints.SingleOrDefault(x => x.ID == complain.ID);
                    if (editComplaint != null)
                    {
                        editComplaint.EmployeeID = complain.EmployeeID;
                        editComplaint.CustomerID = complain.CustomerID;
                        editComplaint.Status = complain.Status;
                        editComplaint.Content = complain.Content;
                        var result = _context.SaveChanges();
                        if (result > 0)
                        {
                            return editComplaint;
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Edit

        #region Get

        public List<Complaint> GetAll(string keyword)
        {
            try
            {
                return _context.Complaints.ToList().FindAll(x => VietnameseCharExtension.CompareVNChar(x.Customer.FirstName + " " + x.Customer.LastName, keyword) 
                    || VietnameseCharExtension.CompareVNChar(x.Employee.FirstName + " " + x.Employee.LastName, keyword)
                    || VietnameseCharExtension.CompareVNChar(x.Content, keyword));
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        public Complaint GetByID(int id)
        {
            try
            {
                return _context.Complaints.FirstOrDefault(x => x.ID == id);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                throw;
            }
        }

        #endregion Get
    }
}
