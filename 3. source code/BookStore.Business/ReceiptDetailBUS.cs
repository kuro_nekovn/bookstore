﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class ReceiptDetailBUS : IReceiptDetailBUS
    {
        #region Contructors

        private readonly IReceiptDetailDAO receiptDetailDAO;
        public ReceiptDetailBUS()
        {
            receiptDetailDAO = new ReceiptDetailDAO();
        }

        public ReceiptDetailBUS(IReceiptDetailDAO receiptDetailDAO)
        {
            this.receiptDetailDAO = receiptDetailDAO;
        }

        #endregion Contructors

        #region Create

        public List<string> Create(ReceiptDetailDTO[] receiptDetails, int receiptID)
        {
            if (receiptDetails.Count() > 0)
            {
                List<string> result = new List<string>();
                ReceiptDetail createReceiptDetail;
                foreach (var receiptDetail in receiptDetails)
                {
                    createReceiptDetail = new ReceiptDetail();
                    createReceiptDetail.BookID = receiptDetail.BookID;
                    createReceiptDetail.ReceiptID = receiptID;
                    createReceiptDetail.Quantity = receiptDetail.Quantity;
                    createReceiptDetail.TotalPrice = receiptDetail.TotalPrice;
                    createReceiptDetail.UnitPrice = receiptDetail.Price;
                    var resultCreate = receiptDetailDAO.Create(createReceiptDetail);
                    if (resultCreate != null)
                    {
                        result.Add($"added { resultCreate.BookID } in {receiptID} successfully ");
                    }
                    else
                    {
                        result.Add($"added { resultCreate.BookID } in {receiptID} failed ");
                    }
                }
                return result;
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int receiptID, int bookID)
        {
            return receiptDetailDAO.Delete(receiptID, bookID);
        }

        #endregion Delete

        #region Edit

        public List<string> Edit(ReceiptDetailDTO[] receiptDetails, int receiptID)
        {
            List<string> result = new List<string>();
            foreach (var receiptDetail in receiptDetails)
            {                
                if (receiptDetail.ReceiptID < 1)
                {
                    var createReceiptDetail = new ReceiptDetail();
                    createReceiptDetail.BookID = receiptDetail.BookID;
                    createReceiptDetail.ReceiptID = receiptDetail.ReceiptID;
                    createReceiptDetail.Quantity = receiptDetail.Quantity;
                    createReceiptDetail.TotalPrice = receiptDetail.TotalPrice;
                    createReceiptDetail.UnitPrice = receiptDetail.Price;
                    createReceiptDetail.ReceiptID = receiptID;
                    var resultCreate = receiptDetailDAO.Create(createReceiptDetail);
                    if (resultCreate != null)
                    {                        
                        result.Add($"Create { receiptDetail.BookName } in {receiptID} successfully ");
                    }
                    else
                    {
                        result.Add($"Create { receiptDetail.BookName } in {receiptID} failed ");
                    }
                }
                else
                {
                    var editResult = receiptDetailDAO.Edit(receiptDetail);
                    if (editResult != null)
                    {
                        result.Add($"Edit { receiptDetail.BookName } in {receiptID} successfully ");
                    }
                    else
                    {
                        result.Add($"Edit { receiptDetail.BookName } in {receiptID} failed ");
                    }
                }

            }
            return result;
        }

        #endregion Edit

        #region Get


        public List<ReceiptDetail> GetAll()
        {
            return receiptDetailDAO.GetAll(0, null);
        }

        public List<ReceiptDetailDTO> ReceiptDetailPage(ref Pagination page, int receiptID, string bookname)
        {
            List<ReceiptDetail> query = receiptDetailDAO.GetAll(receiptID, bookname); //sort? (x => x.BookID)
            query = query.Paging<ReceiptDetail>(ref page);
            var result = query.Select(x => new ReceiptDetailDTO()
            {
                BookID = x.BookID,
                BookName = x.Book.Name,
                ImgUrl = x.Book.ImgUrl,
                ReceiptID = x.ReceiptID,
                Price = x.UnitPrice,
                Quantity = x.Quantity,
                TotalPrice = x.TotalPrice
            });
            return result.ToList();
        }

        public ReceiptDetailDTO GetByID(int receiptID, int bookID)
        {
            var result = receiptDetailDAO.GetByID(receiptID, bookID);
            if (ValidationHelpers.IsValid(result))
            {
                var receipt = new ReceiptDetailDTO(result);
                return receipt;
            }
            return null;
        }

        #endregion Get
    }
}
