﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class GenreBUS : IGenreBUS
    {
        #region Contructors

        private readonly IGenreDAO genreDAO;
        public GenreBUS()
        {
            genreDAO = new GenreDAO();
        }

        public GenreBUS(IGenreDAO genreDAO)
        {
            this.genreDAO = genreDAO;
        }

        #endregion Contructors

        #region Create

        public GenreDTO Create(GenreDTO genre)
        {
            if (ValidationHelpers.IsValid(genre))
            {
                Genre createGenre = new Genre();
                createGenre.Name = genre.Name;
                createGenre.Description = genre.Description;                
                createGenre.DateCreate = DateTime.Now;
                var result = genreDAO.Create(createGenre);
                if (result != null)
                {
                    genre.ID = result.ID;
                    return genre;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int genreID)
        {
            return genreDAO.Delete(genreID);
        }

        #endregion Delete

        #region Edit

        public GenreDTO Edit(GenreDTO genre)
        {           
            var result = genreDAO.Edit(genre);
            if (result != null)
            {
                return new GenreDTO(result);
            }
            return null;
        }

       

        #endregion Edit

        #region Get


        public List<Genre> GetAll()
        {
            return genreDAO.GetAll(null);
        }

        public GenreDTO GetByID(int id)
        {
            var result = genreDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var genre = new GenreDTO(result);
                return genre;
            }
            return null;
        }
        public List<ItemDTO> GetItem()
        {
            return GetAll().Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.Name
            })?.ToList();
        }

        public List<GenreDTO> GenrePage(ref Pagination page, string keyword)
        {
            List<Genre> query = genreDAO.GetAll(keyword);//.OrderBy(x => x.ID);
            query = query.Paging<Genre>(ref page);
            var result = query.Select(x => new GenreDTO()
            {
                ID = x.ID,
                Name = x.Name,
                Description = x.Description
            });
            return result.ToList();
        }

        #endregion Get
    }
}
