﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class AdvertisementBUS : IAdvertisementBUS
    {
        #region Contructors

        private readonly IAdvertisementDAO adsDAO;
        public AdvertisementBUS()
        {
            adsDAO = new AdvertisementDAO();
        }

        public AdvertisementBUS(IAdvertisementDAO adsDAO)
        {
            this.adsDAO = adsDAO;
        }

        #endregion Contructors

        #region Create

        public AdvertisementDTO Create(AdvertisementDTO ads)
        {
            if (ValidationHelpers.IsValid(ads))
            {
                Advertisement createAdvertisement = new Advertisement();
                createAdvertisement.ImgUrl = ads.ImgUrl;
                createAdvertisement.Description = ads.Description;
                createAdvertisement.DateExpire = ads.DateExpire;
                createAdvertisement.DateRelease = ads.DateRelease;
                createAdvertisement.DateCreate = DateTime.Now;
                var result = adsDAO.Create(createAdvertisement);
                if (result != null)
                {
                    ads.ID = result.ID;
                    return ads;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int adsID)
        {
            return adsDAO.Delete(adsID);
        }

        #endregion Delete

        #region Edit

        public AdvertisementDTO Edit(AdvertisementDTO ads)
        {           
            var result = adsDAO.Edit(ads);
            if (result != null)
            {
                return new AdvertisementDTO(result);
            }
            return null;
        }

        public List<AdvertisementDTO> AdvertisementPage(ref Pagination page, string keyword)
        {
            List<Advertisement> query = adsDAO.GetAll(keyword);//.OrderBy(x => x.ID);
            query = query.Paging<Advertisement>(ref page);
            var result = query.Select(x => new AdvertisementDTO()
            {
                ID = x.ID,
                DateCreate = x.DateCreate,
                DateExpire = x.DateExpire,
                DateRelease = x.DateRelease,
                Description = x.Description,
                ImgUrl = x.ImgUrl
            });
            return result.ToList();
        }

        #endregion Edit

        #region Get


        public List<Advertisement> GetAll()
        {
            return adsDAO.GetAll(null);
        }

        public AdvertisementDTO GetByID(int id)
        {
            var result = adsDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var ads = new AdvertisementDTO(result);
                return ads;
            }
            return null;
        }

        #endregion Get
    }
}
