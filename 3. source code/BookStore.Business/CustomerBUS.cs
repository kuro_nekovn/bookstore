﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class CustomerBUS : ICustomerBUS
    {
        #region Contructors

        private readonly ICustomerDAO customerDAO;
        public CustomerBUS()
        {
            customerDAO = new CustomerDAO();
        }

        public CustomerBUS(ICustomerDAO customerDAO)
        {
            this.customerDAO = customerDAO;
        }

        #endregion Contructors

        #region Create

        public CustomerDTO Create(CustomerDTO customer)
        {
            if (ValidationHelpers.IsValid(customer))
            {
                Customer createCustomer = new Customer();
                createCustomer.FirstName = customer.FirstName;
                createCustomer.LastName = customer.LastName;
                createCustomer.Phone = customer.Phone;
                createCustomer.ImgUrl = customer.ImgUrl;
                createCustomer.Address = customer.Address;
                createCustomer.Birthday = customer.Birthday;
                createCustomer.Sex = customer.Sex;
                createCustomer.Password = EncryptionHelpers.Encryption(customer.Password);
                createCustomer.Point = 0;
                createCustomer.IsActive = true;
                createCustomer.Email = customer.Email;
                createCustomer.DateCreate = DateTime.Now;
                var result = customerDAO.Create(createCustomer);
                if (result != null)
                {
                    customer.DateCreate = result.DateCreate;
                    customer.ID = result.ID;
                    return customer;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int customerID)
        {
            return customerDAO.Delete(customerID);
        }

        #endregion Delete

        #region Edit

        public CustomerDTO Edit(CustomerDTO customer)
        {           
            var result = customerDAO.Edit(customer);
            if (result != null)
            {
                return new CustomerDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get `
             

        public List<Customer> GetAll()
        {
            return customerDAO.GetAll(null);
        }

        public List<CustomerViewDTO> CustomerPage(ref Pagination page, string keyword)
        {
            List<Customer> query = customerDAO.GetAll(keyword).FindAll(x => x.IsActive == true);//.OrderBy(x => x.ID);
            query = query.Paging<Customer>(ref page);
            var result = query.Select(x => new CustomerViewDTO()
            {
                ID = x.ID,
                FirstName = x.FirstName,
                LastName = x.LastName,
                ImgUrl = x.ImgUrl,
                Sex = x.Sex,
                Point = (int)x.Point,
                Email = x.Email
            });
            return result.ToList();
        }

        public CustomerDTO GetByID(int id, bool includeDelete)
        {
            var result = customerDAO.GetByID(id, includeDelete);
            if (ValidationHelpers.IsValid(result))
            {
                return new CustomerDTO(result);
            }
            return null;
        }

        public CustomerDTO GetByEmail(string id, bool includeDelete)
        {
            var result = customerDAO.GetByEmail(id, includeDelete);
            if (ValidationHelpers.IsValid(result))
            {
                return new CustomerDTO(result);
            }
            return null;
        }

        public List<ItemDTO> GetItem()
        {
            return GetAll().Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.FirstName + " " + x.LastName
            })?.ToList();
        }
        #endregion Get
    }
}
