﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class BookImageBUS : IBookImageBUS
    {
        #region Contructors

        private readonly IBookImageDAO bookImageDAO;
        public BookImageBUS()
        {
            bookImageDAO = new BookImageDAO();
        }

        public BookImageBUS(IBookImageDAO bookImageDAO)
        {
            this.bookImageDAO = bookImageDAO;
        }

        #endregion Contructors

        #region Create

        public List<string> Create(BookImageDTO[] bookImages, int bookID)
        {
            if (bookImages.Count() > 0)
            {
                List<string> result = new List<string>();
                BookImage createBookImage;
                foreach (var bookImage in bookImages)
                {
                    createBookImage = new BookImage();
                    createBookImage.BookID = bookID;
                    createBookImage.ImageUrl = bookImage.BookUrl;
                    var resultCreate = bookImageDAO.Create(createBookImage);
                    if (resultCreate != null)
                    {
                        result.Add($"added {bookID} successfully ");
                    }
                    else
                    {
                        result.Add($"added {bookID} failed ");
                    }
                }
                return result;
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int ID)
        {
            return bookImageDAO.Delete(ID);
        }

        #endregion Delete

        #region Edit

        public List<string> Edit(BookImageDTO[] bookImages, int bookID)
        {
            List<string> result = new List<string>();
            foreach (var bookImage in bookImages)
            {                
                if (bookImage.ID < 1)
                {
                    var createBookImage = new BookImage();
                    createBookImage.BookID = bookID;
                    createBookImage.ImageUrl = bookImage.BookUrl;
                    var resultCreate = bookImageDAO.Create(createBookImage);
                    if (resultCreate != null)
                    {                        
                        result.Add($"Create {bookID} successfully ");
                    }
                    else
                    {
                        result.Add($"Create in {bookID} failed ");
                    }
                }
                else
                {
                    var editResult = bookImageDAO.Edit(bookImage);
                    if (editResult != null)
                    {
                        result.Add($"Edit {bookID} successfully ");
                    }
                    else
                    {
                        result.Add($"Edit in {bookID} failed ");
                    }
                }

            }
            return result;
        }

        #endregion Edit

        #region Get


        public List<BookImage> GetAll(int bookID)
        {
            return bookImageDAO.GetAll(bookID);
        }

        public List<BookImageDTO> BookImagePage(ref Pagination page, int bookID, string bookname)
        {
            List<BookImage> query = bookImageDAO.GetAll(bookID);//.OrderBy(x => x.BookID);
            query = query.Paging<BookImage>(ref page);
            var result = query.Select(x => new BookImageDTO()
            {
                BookID = x.BookID,
                BookUrl = x.ImageUrl,
                ID = x.ID
            });
            return result.ToList();
        }

        public BookImageDTO GetByID(int ID)
        {
            var result = bookImageDAO.GetByID(ID);
            if (ValidationHelpers.IsValid(result))
            {
                var book = new BookImageDTO(result);
                return book;
            }
            return null;
        }

        #endregion Get
    }
}
