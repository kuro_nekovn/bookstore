﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class PublisherBUS : IPublisherBUS
    {
        #region Contructors

        private readonly IPublisherDAO publisherDAO;
        public PublisherBUS()
        {
            publisherDAO = new PublisherDAO();
        }

        public PublisherBUS(IPublisherDAO publisherDAO)
        {
            this.publisherDAO = publisherDAO;
        }

        #endregion Contructors

        #region Create

        public PublisherDTO Create(PublisherDTO publisher)
        {
            if (ValidationHelpers.IsValid(publisher))
            {
                Publisher createPublisher = new Publisher();
                createPublisher.Name = publisher.Name;
                createPublisher.Phone = publisher.Phone;
                createPublisher.Address = publisher.Address;
                createPublisher.ImgUrl = publisher.ImgUrl;
                createPublisher.IsActive = true;
                createPublisher.DateCreate = DateTime.Now;
                var result = publisherDAO.Create(createPublisher);
                if (result != null)
                {
                    publisher.DateCreate = result.DateCreate;
                    publisher.ID = result.ID;
                    return publisher;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int publisherID)
        {
            return publisherDAO.Delete(publisherID);
        }

        #endregion Delete

        #region Edit

        public PublisherDTO Edit(PublisherDTO publisher)
        {
            if (ValidationHelpers.IsValid(publisher))
            {
                var result = publisherDAO.Edit(publisher);
                if (result != null)
                {
                    return new PublisherDTO(result);
                }
            }            
            return null;
        }

        #endregion Delete

        #region Get

        public List<Publisher> GetAll()
        {
            return publisherDAO.GetAll(null);
        }

        public List<PublisherDTO> PublisherPage(ref Pagination page, string keyword)
        {
            List<Publisher> query = publisherDAO.GetAll(keyword).FindAll(x => x.IsActive == true);//.OrderBy(x => x.ID);
            query = query.Paging<Publisher>(ref page);
            var result = query.Select(x => new PublisherDTO()
            {
                ID = x.ID,
                Name = x.Name,
                Address = x.Address,
                DateCreate = x.DateCreate,
                ImgUrl = x.ImgUrl,
                Phone = x.Phone
            });
            return result.ToList();
        }

        public PublisherDTO GetByID(int id)
        {
            var result = publisherDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                return new PublisherDTO(result);
            }
            return null;
        }

        public List<ItemDTO> GetItem()
        {
            return GetAll().Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.Name
            }).ToList();
        }

        #endregion Get
    }
}
