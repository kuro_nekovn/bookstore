﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IReceiptBUS
    {
        List<Receipt> GetAll();
        ReceiptDTO Create(ReceiptDTO receipt);
        ReceiptDTO Edit(ReceiptDTO receipt);
        bool Delete(int receiptID);
        List<ReceiptViewDTO> ReceiptPage(ref Pagination page, string employeeID, string customerID);
        Receipt GetByID(int ID);
    }
}
