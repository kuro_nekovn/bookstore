﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IEmployeeBUS
    {
        List<Employee> GetAll();
        EmployeeDTO Create(EmployeeDTO employee);
        EmployeeDTO Edit(EmployeeDTO employee);
        bool Delete(int employeeID);
        List<EmployeeViewDTO> EmployeePage(ref Pagination page, string keyword);
        EmployeeDTO GetByID(int ID, bool includeDelete);
        EmployeeDTO GetByEmail(string email);
        List<ItemDTO> GetItem();

    }
}
