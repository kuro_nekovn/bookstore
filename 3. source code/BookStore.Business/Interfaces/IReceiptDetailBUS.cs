﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IReceiptDetailBUS
    {
        List<ReceiptDetail> GetAll();
        List<string> Create(ReceiptDetailDTO[] ReceiptDetails, int receiptID);
        List<string> Edit(ReceiptDetailDTO[] ReceiptDetails, int receiptID);
        bool Delete(int receiptID, int bookID);
        List<ReceiptDetailDTO> ReceiptDetailPage(ref Pagination page, int receiptID, string bookName);
        ReceiptDetailDTO GetByID(int receiptID, int bookID);
    }
}
