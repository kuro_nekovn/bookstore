﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IBlogReviewBUS
    {
        List<BlogReview> GetAll();
        BlogReviewDTO Create(BlogReviewDTO blogReview);
        BlogReviewDTO Edit(BlogReviewDTO blogReview);
        bool Delete(int blogReviewID);
        List<BlogReviewViewDTO> BlogReviewPage(ref Pagination page, string keyword, int blogID);
        List<BlogReviewDTO> BlogReviewShoppingPage(ref Pagination page, string keyword, int blogID);
        BlogReviewDTO GetByID(int ID);
    }
}
