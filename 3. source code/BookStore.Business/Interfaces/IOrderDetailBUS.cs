﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IOrderDetailBUS
    {
        List<OrderDetail> GetAll();
        List<string> Create(OrderDetailDTO[] OrderDetails, int orderID);
        List<string> Edit(OrderDetailDTO[] OrderDetails, int orderID);
        bool Delete(int orderID, int bookID);
        List<OrderDetailDTO> OrderDetailPage(ref Pagination page, int orderID, string bookName);
        OrderDetailDTO GetByID(int orderID, int bookID);
    }
}
