﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IBlogBUS
    {
        List<Blog> GetAll();
        BlogDTO Create(BlogDTO blog);
        BlogDTO Edit(BlogDTO blog);
        bool Delete(int blogID);
        List<BlogViewDTO> BlogPage(ref Pagination page, string keyword);
        List<BlogShopModel> BlogShoppingPage(ref Pagination page, string keyword);
        BlogDTO GetByID(int ID);
        List<ItemDTO> GetItem();
    }
}
