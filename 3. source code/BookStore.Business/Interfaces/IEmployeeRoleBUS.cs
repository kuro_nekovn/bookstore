﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IEmployeeRoleBUS
    {
        List<Employee> GetAll();
        List<EmployeeRoleDTO> EmployeePage(ref Pagination page, string keyword);
        Employee EditEmployeeRole(EmployeeRoleDTO employeeRole);

    }
}
