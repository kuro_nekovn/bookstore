﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IGenreBUS
    {
        List<Genre> GetAll();
        GenreDTO Create(GenreDTO genre);
        GenreDTO Edit(GenreDTO genre);
        bool Delete(int genreID);
        List<GenreDTO> GenrePage(ref Pagination page, string keyword);
        GenreDTO GetByID(int ID);
        List<ItemDTO> GetItem();
    }
}
