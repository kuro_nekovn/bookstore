﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IBookImageBUS
    {
        List<BookImage> GetAll(int bookID);
        List<string> Create(BookImageDTO[] BookImages, int bookID);
        List<string> Edit(BookImageDTO[] BookImages, int bookID);
        bool Delete(int ID);
        List<BookImageDTO> BookImagePage(ref Pagination page, int bookID, string bookName);
        BookImageDTO GetByID(int ID);
    }
}
