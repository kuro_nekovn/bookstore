﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IPublisherBUS
    {
        List<Publisher> GetAll();
        PublisherDTO Create(PublisherDTO publisher);
        PublisherDTO Edit(PublisherDTO publisher);
        bool Delete(int publisherID);
        List<PublisherDTO> PublisherPage(ref Pagination page, string keyword);
        PublisherDTO GetByID(int ID);
        List<ItemDTO> GetItem();
    }
}
