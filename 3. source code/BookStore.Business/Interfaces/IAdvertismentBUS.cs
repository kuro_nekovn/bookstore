﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IAdvertisementBUS
    {
        List<Advertisement> GetAll();
        AdvertisementDTO Create(AdvertisementDTO ads);
        AdvertisementDTO Edit(AdvertisementDTO ads);
        bool Delete(int adsID);
        List<AdvertisementDTO> AdvertisementPage(ref Pagination page, string keyword);
        AdvertisementDTO GetByID(int ID);
    }
}
