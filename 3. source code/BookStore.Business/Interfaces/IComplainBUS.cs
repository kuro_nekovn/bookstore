﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IComplainBUS
    {
        List<Complaint> GetAll();
        ComplainDTO Create(ComplainDTO complain);
        ComplainDTO Edit(ComplainDTO complain);
        bool Delete(int complainID);
        List<ComplainViewDTO> ComplainPage(ref Pagination page, string keyword);
        ComplainDTO GetByID(int ID);
    }
}
