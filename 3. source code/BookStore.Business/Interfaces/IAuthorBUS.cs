﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IAuthorBUS
    {
        List<Author> GetAll();
        AuthorDTO Create(AuthorDTO author);
        AuthorDTO Edit(AuthorDTO author);
        bool Delete(int authorID);
        List<AuthorViewDTO> AuthorPage(ref Pagination page, string keyword);
        AuthorDTO GetByID(int ID);
        List<ItemDTO> GetItem();
        Author GetRandomAuthor();
    }
}
