﻿using BookStore.DataTransfer;
using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Business.Interfaces
{
    public interface IAccountBUS
    {
        List<RoleDTO> GetAllRoles();
        Role GetRoleByID(int ID, string roleName);
        bool ChangeEmployeePassword(int ID, string Email, string Password);
        bool ChangeCustomerPassword(int ID, string Email, string Password);
        Employee Login(string email, string password);
        Customer ShopLogin(string email, string password);
    }
}
