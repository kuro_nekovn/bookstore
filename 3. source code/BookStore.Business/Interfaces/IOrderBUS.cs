﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IOrderBUS
    {
        List<Order> GetAll();
        OrderDTO Create(OrderDTO order);
        OrderDTO Edit(OrderDTO order);
        bool Delete(int orderID);
        List<OrderViewDTO> OrderPage(ref Pagination page, string employeeID, string customerID);
        List<OrderViewDTO> OrderShoppingPage(ref Pagination page, string customerID);
        Order GetByID(int ID);
        List<OrderDetail> GetOrderDetailByOrderID(int ID);
        List<ItemDTO> GetItem();
    }
}
