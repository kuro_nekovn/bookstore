﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IRateBUS
    {
        List<Rate> GetAll();
        RateDTO Create(RateDTO rate);
        RateDTO Edit(RateDTO rate);
        bool Delete(int rateID);
        List<RateViewDTO> RatePage(ref Pagination page, string keyword, string bookID, string customerID);
        List<RateShopDTO> RateShoppingPage(ref Pagination page, string keyword, int bookID, string customerID);
        RateDTO GetByID(int ID);
    }
}
