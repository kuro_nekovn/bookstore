﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface ICustomerBUS
    {
        List<Customer> GetAll();
        CustomerDTO Create(CustomerDTO customer);
        CustomerDTO Edit(CustomerDTO customer);
        bool Delete(int customerID);
        List<CustomerViewDTO> CustomerPage(ref Pagination page, string keyword);
        CustomerDTO GetByID(int ID, bool includeDelete);
        CustomerDTO GetByEmail(string ID, bool includeDelete);
        List<ItemDTO> GetItem();
    }
}
