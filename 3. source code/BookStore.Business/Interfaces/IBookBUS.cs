﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BookStore.Entities;
using BookStore.DataTransfer;

namespace BookStore.Business.Interfaces
{
    public interface IBookBUS
    {
        List<Book> GetAll();
        BookDTO Create(BookDTO book);
        BookDTO Edit(BookDTO book);
        bool Delete(int bookID);
        List<BookViewDTO> BookPage(ref Pagination page, string bookName, string authorName, string publisherName);
        List<BookViewDTO> BookShopPage(ref Pagination page, string bookName, int authorName, int publisherName, int genreID);
        BookDTO GetByID(int ID);
        List<ItemDTO> GetItem();
        List<BookOrderDTO> GetOrderBook();

    }
}
