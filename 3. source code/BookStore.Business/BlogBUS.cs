﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class BlogBUS : IBlogBUS
    {
        #region Contructors

        private readonly IBlogDAO blogDAO;
        public BlogBUS()
        {
            blogDAO = new BlogDAO();
        }

        public BlogBUS(IBlogDAO blogDAO)
        {
            this.blogDAO = blogDAO;
        }

        #endregion Contructors

        #region Create

        public BlogDTO Create(BlogDTO blog)
        {
            if (ValidationHelpers.IsValid(blog))
            {
                Blog createBlog = new Blog();
                createBlog.BlogImg = blog.BlogImg;
                createBlog.Sumary = blog.Sumary;
                createBlog.Title = blog.Title;
                createBlog.Content = blog.Content;
                createBlog.UserCreate = int.Parse(blog.UserCreateName);
                createBlog.DateCreate = DateTime.Now;
                var result = blogDAO.Create(createBlog);
                if (result != null)
                {
                    blog.ID = result.ID;
                    return blog;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int blogID)
        {
            return blogDAO.Delete(blogID);
        }

        #endregion Delete

        #region Edit

        public BlogDTO Edit(BlogDTO blog)
        {           
            var result = blogDAO.Edit(blog);
            if (result != null)
            {
                return new BlogDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<Blog> GetAll()
        {
            return blogDAO.GetAll(null);
        }

        public List<ItemDTO> GetItem()
        {
            return blogDAO.GetAll(null).Select(x => new ItemDTO() {
                ID = x.ID,
                Name = x.Title
            })?.ToList();
        }

        public List<BlogViewDTO> BlogPage(ref Pagination page, string keyword)
        {
            List<Blog> query = blogDAO.GetAll(keyword);//.OrderBy(x => x.ID);
            query = query.Paging<Blog>(ref page);
            var result = query.Select(x => new BlogViewDTO()
            {
                ID = x.ID,
                BlogImg = x.BlogImg,
                UserCreateName = x.Employee.FirstName + " " + x.Employee.LastName,
                Sumary = x.Sumary,
                Title = x.Title

            });
            return result.ToList();
        }

        public List<BlogShopModel> BlogShoppingPage(ref Pagination page, string keyword)
        {
            List<Blog> query = blogDAO.GetAll(keyword);//.OrderBy(x => x.ID);
            query = query.Paging<Blog>(ref page);
            var result = query.Select(x => new BlogShopModel(x));
            return result.ToList();
        }

        public BlogDTO GetByID(int id)
        {
            var result = blogDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var blog = new BlogDTO(result);
                return blog;
            }
            return null;
        }

        #endregion Get
    }
}
