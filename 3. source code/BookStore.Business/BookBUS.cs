﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class BookBUS : IBookBUS
    {
        #region Contructors

        private readonly IBookDAO bookDAO;
        public BookBUS()
        {
            bookDAO = new BookDAO();
        }

        public BookBUS(IBookDAO bookDAO)
        {
            this.bookDAO = bookDAO;
        }

        #endregion Contructors

        #region Create

        public BookDTO Create(BookDTO book)
        {
            if (ValidationHelpers.IsValid(book))
            {
                Book createBook = new Book();
                createBook.ImgUrl = book.ImgUrl;
                createBook.Sumary = book.Sumary;
                createBook.Rating = 0;
                createBook.Status = 0;
                createBook.Total = 0;
                createBook.Name = book.Name;
                createBook.Content = book.Content;
                createBook.PublisherID = book.Publisher;
                createBook.AuthorID = book.Author;
                createBook.GenreID = book.GenreID;
                createBook.DateCreate = DateTime.Now;
                var result = bookDAO.Create(createBook);
                if (result != null)
                {
                    book.ID = result.ID;
                    return book;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int bookID)
        {
            return bookDAO.Delete(bookID);
        }

        #endregion Delete

        #region Edit

        public BookDTO Edit(BookDTO book)
        {
            var result = bookDAO.Edit(book);
            if (result != null)
            {
                return new BookDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get


        public List<Book> GetAll()
        {
            return bookDAO.GetAll(null, null, null);
        }

        public List<ItemDTO> GetItem()
        {
            return bookDAO.GetAll(null, null, null).Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.Name 
            })?.ToList();
        }

        public List<BookOrderDTO> GetOrderBook()
        {
            return GetAll().Select(x => new BookOrderDTO()
            {
                ID = x.ID,
                Price = x.Price,
                Total = x.Total,
                ImgUrl = x.ImgUrl,
                Discount = x.Discount,
                Name = x.Name + " - " + x.Author.FirstName + " " + x.Author.LastName + " - " + x.Publisher.Name
            })?.ToList();
        }


        public BookDTO GetByID(int id)
        {
            var result = bookDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var book = new BookDTO(result);
                return book;
            }
            return null;
        }

        public List<BookViewDTO> BookPage(ref Pagination page, string bookName, string authorName, string publisherName)
        {
            List<Book> query = bookDAO.GetAll(bookName, authorName, publisherName).FindAll(x => x.IsActive);//.OrderBy(x => x.ID);
            query = query.Paging<Book>(ref page);
            var result = query.Select(x => new BookViewDTO()
            {
                ID = x.ID,
                Name = x.Name,
                Total = x.Total,
                Sumary = x.Sumary,
                ImgUrl = x.ImgUrl,
                AuthorName = x.Author.FirstName + " " + x.Author.LastName,
                PublisherName = x.Publisher.Name
            });
            return result.ToList();
        }

        public List<BookViewDTO> BookShopPage(ref Pagination page, string bookName, int authorName, int publisherName, int genreID)
        {
            List<Book> query = bookDAO.GetShopAll(bookName, authorName, publisherName, genreID).FindAll(x => x.IsActive);//.OrderBy(x => x.ID);
            query = query.Paging<Book>(ref page);
            var result = query.Select(x => new BookViewDTO()
            {
                ID = x.ID,
                Name = x.Name,
                Total = x.Total,
                Sumary = x.Sumary,
                ImgUrl = x.ImgUrl,
                AuthorName = x.Author.FirstName + " " + x.Author.LastName,
                PublisherName = x.Publisher.Name,
                Price = x.Price
            });
            return result.ToList();
        }

        #endregion Get
    }
}
