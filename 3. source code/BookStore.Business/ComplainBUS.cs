﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class ComplainBUS : IComplainBUS
    {
        #region Contructors

        private readonly IComplainDAO complainDAO;
        public ComplainBUS()
        {
            complainDAO = new ComplainDAO();
        }

        public ComplainBUS(IComplainDAO complainDAO)
        {
            this.complainDAO = complainDAO;
        }

        #endregion Contructors

        #region Create

        public ComplainDTO Create(ComplainDTO complain)
        {
            if (ValidationHelpers.IsValid(complain))
            {
                Complaint createComplain = new Complaint();
                createComplain.EmployeeID = complain.EmployeeID;
                createComplain.CustomerID = complain.CustomerID;
                createComplain.Status = complain.Status;
                createComplain.Content = complain.Content;
                createComplain.OrderID = complain.OrderID;
                createComplain.DateCreate = DateTime.Now;
                var result = complainDAO.Create(createComplain);
                if (result != null)
                {
                    complain.ID = result.ID;
                    return complain;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int complainID)
        {
            return complainDAO.Delete(complainID);
        }

        #endregion Delete

        #region Edit

        public ComplainDTO Edit(ComplainDTO complain)
        {           
            var result = complainDAO.Edit(complain);
            if (result != null)
            {
                return new ComplainDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<Complaint> GetAll()
        {
            return complainDAO.GetAll(null);
        }

        

        public List<ComplainViewDTO> ComplainPage(ref Pagination page, string keyword)
        {
            List<Complaint> query = complainDAO.GetAll(keyword);//.OrderBy(x => x.ID);
            query = query.Paging<Complaint>(ref page);
            var result = query.Select(x => new ComplainViewDTO()
            {
                ID = x.ID,
                EmployeeID = x.Employee.FirstName + " " + x.Employee.LastName,
                CustomerID = x.Customer.FirstName + " " + x.Customer.LastName,
                OrderID = x.OrderID == null ? 0 : (int)x.OrderID,
                Status = x.Status
            });
            return result.ToList();
        }

        public ComplainDTO GetByID(int id)
        {
            var result = complainDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var complain = new ComplainDTO(result);
                return complain;
            }
            return null;
        }

        #endregion Get
    }
}
