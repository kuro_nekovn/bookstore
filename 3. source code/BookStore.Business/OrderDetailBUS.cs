﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class OrderDetailBUS : IOrderDetailBUS
    {
        #region Contructors

        private readonly IOrderDetailDAO orderDetailDAO;
        public OrderDetailBUS()
        {
            orderDetailDAO = new OrderDetailDAO();
        }

        public OrderDetailBUS(IOrderDetailDAO orderDetailDAO)
        {
            this.orderDetailDAO = orderDetailDAO;
        }

        #endregion Contructors

        #region Create

        public List<string> Create(OrderDetailDTO[] orderDetails, int orderID)
        {
            if (orderDetails.Count() > 0)
            {
                List<string> result = new List<string>();
                OrderDetail createOrderDetail;
                foreach (var orderDetail in orderDetails)
                {
                    createOrderDetail = new OrderDetail();
                    createOrderDetail.BookID = orderDetail.BookID;
                    createOrderDetail.OrderID = orderID;
                    createOrderDetail.Quantity = orderDetail.Quantity;
                    createOrderDetail.TotalPrice = orderDetail.TotalPrice;
                    createOrderDetail.UnitPrice = orderDetail.Price;
                    createOrderDetail.Discount = orderDetail.Discount;
                    var resultCreate = orderDetailDAO.Create(createOrderDetail);
                    if (resultCreate != null)
                    {
                        result.Add($"added { resultCreate.BookID } in {orderID} successfully ");
                    }
                    else
                    {
                        result.Add($"added { resultCreate.BookID } in {orderID} failed ");
                    }
                }
                return result;
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int orderID, int bookID)
        {
            return orderDetailDAO.Delete(orderID, bookID);
        }

        #endregion Delete

        #region Edit

        public List<string> Edit(OrderDetailDTO[] orderDetails, int orderID)
        {
            List<string> result = new List<string>();
            foreach (var orderDetail in orderDetails)
            {                
                if (orderDetail.OrderID < 1)
                {
                    var createOrderDetail = new OrderDetail();
                    createOrderDetail.BookID = orderDetail.BookID;
                    createOrderDetail.OrderID = orderDetail.OrderID;
                    createOrderDetail.Quantity = orderDetail.Quantity;
                    createOrderDetail.TotalPrice = orderDetail.TotalPrice;
                    createOrderDetail.UnitPrice = orderDetail.Price;
                    createOrderDetail.Discount = orderDetail.Discount;
                    createOrderDetail.OrderID = orderID;
                    var resultCreate = orderDetailDAO.Create(createOrderDetail);
                    if (resultCreate != null)
                    {                        
                        result.Add($"Create { orderDetail.BookName } in {orderID} successfully ");
                    }
                    else
                    {
                        result.Add($"Create { orderDetail.BookName } in {orderID} failed ");
                    }
                }
                else
                {
                    var editResult = orderDetailDAO.Edit(orderDetail);
                    if (editResult != null)
                    {
                        result.Add($"Edit { orderDetail.BookName } in {orderID} successfully ");
                    }
                    else
                    {
                        result.Add($"Edit { orderDetail.BookName } in {orderID} failed ");
                    }
                }

            }
            return result;
        }

        #endregion Edit

        #region Get


        public List<OrderDetail> GetAll()
        {
            return orderDetailDAO.GetAll(0, null);
        }

        public List<OrderDetailDTO> OrderDetailPage(ref Pagination page, int orderID, string bookname)
        {
            List<OrderDetail> query = orderDetailDAO.GetAll(orderID, bookname);//.OrderBy(x => x.BookID);
            query = query.Paging<OrderDetail>(ref page);
            var result = query.Select(x => new OrderDetailDTO()
            {
                BookID = x.BookID,
                BookName = x.Book.Name,
                ImgUrl = x.Book.ImgUrl,
                OrderID = x.OrderID,
                Price = x.UnitPrice,
                Quantity = x.Quantity,
                TotalPrice = x.TotalPrice == null ? 0 : (double)x.TotalPrice
            });
            return result.ToList();
        }

        public OrderDetailDTO GetByID(int orderID, int bookID)
        {
            var result = orderDetailDAO.GetByID(orderID, bookID);
            if (ValidationHelpers.IsValid(result))
            {
                var order = new OrderDetailDTO(result);
                return order;
            }
            return null;
        }

        #endregion Get
    }
}
