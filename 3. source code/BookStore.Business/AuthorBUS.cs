﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class AuthorBUS : IAuthorBUS
    {
        #region Contructors

        private readonly IAuthorDAO authorDAO;
        public AuthorBUS()
        {
            authorDAO = new AuthorDAO();
        }

        public AuthorBUS(IAuthorDAO authorDAO)
        {
            this.authorDAO = authorDAO;
        }

        #endregion Contructors

        #region Create

        public AuthorDTO Create(AuthorDTO author)
        {
            if (ValidationHelpers.IsValid(author))
            {
                Author createAuthor = new Author();
                createAuthor.FirstName = author.FirstName;
                createAuthor.LastName = author.LastName;
                createAuthor.Phone = author.Phone;
                createAuthor.ImgUrl = author.ImgUrl;
                createAuthor.Address = author.Address;
                createAuthor.Birthday = author.Birthday;
                createAuthor.Content = author.Content;
                createAuthor.Sex = author.Sex;
                createAuthor.Sumary = author.Sumary;
                createAuthor.IsActive = true;
                createAuthor.DateCreate = DateTime.Now;
                var result = authorDAO.Create(createAuthor);
                if (result != null)
                {
                    author.ID = result.ID;
                    return author;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int authorID)
        {
            return authorDAO.Delete(authorID);
        }

        #endregion Delete

        #region Edit

        public AuthorDTO Edit(AuthorDTO author)
        {           
            var result = authorDAO.Edit(author);
            if (result != null)
            {
                return new AuthorDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get

        public Author GetRandomAuthor()
        {
            var result = authorDAO.GetAll(null);
            return result[new Random().Next(result.Count - 1)];
        }
        public List<Author> GetAll()
        {
            return authorDAO.GetAll(null);
        }

        public List<AuthorViewDTO> AuthorPage(ref Pagination page, string keyword)
        {
            List<Author> query = authorDAO.GetAll(keyword).FindAll(x => x.IsActive == true);//.OrderBy(x => x.ID);
            query = query.Paging<Author>(ref page);
            var result = query.Select(x => new AuthorViewDTO()
            {
                ID = x.ID,
                FirstName = x.FirstName,
                LastName = x.LastName,
                ImgUrl = x.ImgUrl,
                Sex = x.Sex,
                Sumary = x.Sumary
            });
            return result.ToList();
        }

        public AuthorDTO GetByID(int id)
        {
            var result = authorDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var author = new AuthorDTO(result);
                return author;
            }
            return null;
        }

        public List<ItemDTO> GetItem()
        {
            return GetAll().Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.FirstName + " " + x.LastName
            }).ToList();
        }

        #endregion Get
    }
}
