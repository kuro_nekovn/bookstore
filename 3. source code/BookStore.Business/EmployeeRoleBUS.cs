﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class EmployeeRoleBUS : IEmployeeRoleBUS
    {
        #region Contructors

        private readonly IEmployeeDAO employeeDAO;
        private readonly IEmployeeRoleDAO employeeRoleDAO;
        public EmployeeRoleBUS()
        {
            employeeDAO = new EmployeeDAO();
            employeeRoleDAO = new EmployeeRoleDAO();
        }

        public EmployeeRoleBUS(IEmployeeDAO employeeDAO, IEmployeeRoleDAO employeeRoleDAO)
        {
            this.employeeDAO = employeeDAO;
            this.employeeRoleDAO = employeeRoleDAO;
        }

        #endregion Contructors

       

        #region Edit

        public EmployeeDTO Edit(EmployeeDTO employee)
        {           
            var result = employeeDAO.Edit(employee);
            if (result != null)
            {
                return new EmployeeDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<Employee> GetAll()
        {
            return employeeDAO.GetAll(null);
        }

        public List<EmployeeRoleDTO> EmployeePage(ref Pagination page, string keyword)
        {
            List<Employee> query = employeeDAO.GetAll(keyword).FindAll(x => x.IsActive == true);//.OrderBy(x => x.ID);
            query = query.Paging<Employee>(ref page);
            var result = query.Select(x => new EmployeeRoleDTO()
            {
                ID = x.ID,
                FirstName = x.FirstName,
                LastName = x.LastName,
                ImgUrl = x.ImgUrl,
                IsAdmin = x.Roles.FirstOrDefault(r => r.ID == 1) != null,
                IsHr = x.Roles.FirstOrDefault(r => r.ID == 2) != null,
                IsSaleman = x.Roles.FirstOrDefault(r => r.ID == 3) != null,
                IsStockKeeper = x.Roles.FirstOrDefault(r => r.ID == 4) != null
            });
            return result.ToList();
        }
        public Employee EditEmployeeRole(EmployeeRoleDTO employeeRole)
        {
            return employeeRoleDAO.EditEmployeeRole(employeeRole);
        }

        #endregion Get
    }
}
