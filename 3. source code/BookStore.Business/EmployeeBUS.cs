﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class EmployeeBUS : IEmployeeBUS
    {
        #region Contructors

        private readonly IEmployeeDAO employeeDAO;
        public EmployeeBUS()
        {
            employeeDAO = new EmployeeDAO();
        }

        public EmployeeBUS(IEmployeeDAO employeeDAO)
        {
            this.employeeDAO = employeeDAO;
        }

        #endregion Contructors

        #region Create

        public EmployeeDTO Create(EmployeeDTO employee)
        {
            if (ValidationHelpers.IsValid(employee))
            {
                Employee createEmployee = new Employee();
                createEmployee.FirstName = employee.FirstName;
                createEmployee.LastName = employee.LastName;
                createEmployee.Phone = employee.Phone;
                createEmployee.ImgUrl = employee.ImgUrl;
                createEmployee.Address = employee.Address;
                createEmployee.Birthday = employee.Birthday;
                createEmployee.Sex = employee.Sex;
                createEmployee.Email = employee.Email;
                createEmployee.IsActive = true;
                createEmployee.Password = employee.Password;
                createEmployee.DateCreate = DateTime.Now;
                var result = employeeDAO.Create(createEmployee);
                if (result != null)
                {
                    employee.ID = result.ID;
                    return employee;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int employeeID)
        {
            return employeeDAO.Delete(employeeID);
        }

        #endregion Delete

        #region Edit

        public EmployeeDTO Edit(EmployeeDTO employee)
        {           
            var result = employeeDAO.Edit(employee);
            if (result != null)
            {
                return new EmployeeDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<Employee> GetAll()
        {
            return employeeDAO.GetAll(null);
        }

        public List<EmployeeViewDTO> EmployeePage(ref Pagination page, string keyword)
        {
            List<Employee> query = employeeDAO.GetAll(keyword).FindAll(x => x.IsActive == true);//.OrderBy(x => x.ID);
            query = query.Paging<Employee>(ref page);
            var result = query.Select(x => new EmployeeViewDTO()
            {
                ID = x.ID,
                FirstName = x.FirstName,
                LastName = x.LastName,
                ImgUrl = x.ImgUrl,
                Sex = x.Sex,
                Email = x.Email
            });
            return result.ToList();
        }

        public EmployeeDTO GetByID(int id, bool includeDelete)
        {
            var result = employeeDAO.GetByID(id, includeDelete);
            if (ValidationHelpers.IsValid(result))
            {
                return new EmployeeDTO(result);
            }
            return null;
        }
        public EmployeeDTO GetByEmail(string email)
        {
            var result = employeeDAO.GetByEmail(email);
            if (ValidationHelpers.IsValid(result))
            {
                return new EmployeeDTO(result);
            }
            return null;
        }

        public List<ItemDTO> GetItem()
        {
            return GetAll().Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.FirstName + " " + x.LastName
            })?.ToList();
        }

        #endregion Get
    }
}
