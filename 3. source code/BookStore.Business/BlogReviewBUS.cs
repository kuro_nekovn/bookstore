﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class BlogReviewBUS : IBlogReviewBUS
    {
        #region Contructors

        private readonly IBlogReviewDAO blogReviewDAO;
        public BlogReviewBUS()
        {
            blogReviewDAO = new BlogReviewDAO();
        }

        public BlogReviewBUS(IBlogReviewDAO blogReviewDAO)
        {
            this.blogReviewDAO = blogReviewDAO;
        }

        #endregion Contructors

        #region Create

        public BlogReviewDTO Create(BlogReviewDTO blogReview)
        {
            if (ValidationHelpers.IsValid(blogReview))
            {
                BlogReview createBlogReview = new BlogReview();
                createBlogReview.BlogID = int.Parse(blogReview.BlogName);
                createBlogReview.Email = blogReview.Email;
                createBlogReview.Title = blogReview.Title;
                createBlogReview.Content = blogReview.Content;
                createBlogReview.DateCreate = DateTime.Now;
                var result = blogReviewDAO.Create(createBlogReview);
                if (result != null)
                {
                    blogReview.ID = result.ID;
                    return blogReview;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int blogReviewID)
        {
            return blogReviewDAO.Delete(blogReviewID);
        }

        #endregion Delete

        #region Edit

        public BlogReviewDTO Edit(BlogReviewDTO blogReview)
        {           
            var result = blogReviewDAO.Edit(blogReview);
            if (result != null)
            {
                return new BlogReviewDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<BlogReview> GetAll()
        {
            return blogReviewDAO.GetAll(null, 0);
        }

        public List<BlogReviewViewDTO> BlogReviewPage(ref Pagination page, string keyword, int blogID)
        {
            List<BlogReview> query = blogReviewDAO.GetAll(keyword, blogID);//.OrderBy(x => x.ID);
            query = query.Paging<BlogReview>(ref page);
            var result = query.Select(x => new BlogReviewViewDTO()
            {
                ID = x.ID,
                BlogName = x.Blog.Title,
                Title = x.Title,
                Email = x.Email,
                DateCreate = (DateTime) x.DateCreate

            });
            return result.ToList();
        }

        public List<BlogReviewDTO> BlogReviewShoppingPage(ref Pagination page, string keyword, int blogID)
        {
            List<BlogReview> query = blogReviewDAO.GetAll(keyword, blogID).OrderByDescending(x => x.DateCreate).ToList();//.OrderBy(x => x.ID);
            query = query.Paging<BlogReview>(ref page);
            var result = query.Select(x => new BlogReviewDTO(x));           
            return result.ToList();
        }
        public BlogReviewDTO GetByID(int id)
        {
            var result = blogReviewDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var blogReview = new BlogReviewDTO(result);
                return blogReview;
            }
            return null;
        }

        #endregion Get
    }
}
