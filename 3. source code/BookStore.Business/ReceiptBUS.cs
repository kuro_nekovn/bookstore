﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class ReceiptBUS : IReceiptBUS
    {
        #region Contructors

        private readonly IReceiptDAO receiptDAO;
        public ReceiptBUS()
        {
            receiptDAO = new ReceiptDAO();
        }

        public ReceiptBUS(IReceiptDAO receiptDAO)
        {
            this.receiptDAO = receiptDAO;
        }

        #endregion Contructors

        #region Create

        public ReceiptDTO Create(ReceiptDTO receipt)
        {
            if (ValidationHelpers.IsValid(receipt))
            {
                Receipt createReceipt = new Receipt();
                createReceipt.PublisherID = receipt.PublisherID;
                createReceipt.EmployeeID = receipt.EmployeeID;
                createReceipt.Description = receipt.Description;
                createReceipt.TotalPrice = receipt.TotalPrice;
                createReceipt.DateCreate = DateTime.Now;
                var result = receiptDAO.Create(createReceipt);
                if (result != null)
                {
                    receipt.ID = result.ID;
                    return receipt;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int receiptID)
        {
            return receiptDAO.Delete(receiptID);
        }

        #endregion Delete

        #region Edit

        public ReceiptDTO Edit(ReceiptDTO receipt)
        {           
            var result = receiptDAO.Edit(receipt);
            if (result != null)
            {
                return new ReceiptDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<Receipt> GetAll()
        {
            return receiptDAO.GetAll(null, null);
        }

        public List<ReceiptViewDTO> ReceiptPage(ref Pagination page,  string employeeID, string customerID)
        {
            List<Receipt> query = receiptDAO.GetAll(employeeID, customerID);//.OrderBy(x => x.ID);
            query = query.Paging<Receipt>(ref page);
            var result = query.Select(x => new ReceiptViewDTO()
            {
                ID = x.ID,
                PublisherID = x.Publisher.Name,
                EmployeeID = x.Employee.FirstName + " " + x.Employee.LastName,
                TotalPrice = x.TotalPrice,
                ReceiptDate = (DateTime)x.DateCreate
            });
            return result.ToList();
        }

        public Receipt GetByID(int id)
        {
            return receiptDAO.GetByID(id);
        }

        #endregion Get
    }
}
