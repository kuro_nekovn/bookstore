﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class RateBUS : IRateBUS
    {
        #region Contructors

        private readonly IRateDAO rateDAO;
        public RateBUS()
        {
            rateDAO = new RateDAO();
        }

        public RateBUS(IRateDAO rateDAO)
        {
            this.rateDAO = rateDAO;
        }

        #endregion Contructors

        #region Create

        public RateDTO Create(RateDTO rate)
        {
            if (ValidationHelpers.IsValid(rate))
            {
                Rate createRate = new Rate();
                createRate.CustomerID = rate.CustomerID;
                createRate.BookID = rate.BookID;
                createRate.Title = rate.Title;
                createRate.Content = rate.Content;
                createRate.Rating = rate.Rating;
                createRate.DateCreate = DateTime.Now;
                var result = rateDAO.Create(createRate);
                if (result != null)
                {
                    rate.ID = result.ID;
                    return rate;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int rateID)
        {
            return rateDAO.Delete(rateID);
        }

        #endregion Delete

        #region Edit

        public RateDTO Edit(RateDTO rate)
        {           
            var result = rateDAO.Edit(rate);
            if (result != null)
            {
                return new RateDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get
             

        public List<Rate> GetAll()
        {
            return rateDAO.GetAll(null, null, null);
        }        
        public List<RateShopDTO> RateShoppingPage(ref Pagination page, string keyword, int bookID, string customerID)
        {
            List<Rate> query = rateDAO.GetAll(string.Empty, string.Empty, string.Empty).FindAll(x => x.BookID == bookID).OrderByDescending(x => x.DateCreate).ToList();//.OrderBy(x => x.ID);
            query = query.Paging<Rate>(ref page);
            var result = query.Select(x => new RateShopDTO(x));         
            return result.ToList();
        }

        public List<RateViewDTO> RatePage(ref Pagination page, string keyword, string bookID, string customerID)
        {
            List<Rate> query = rateDAO.GetAll(keyword, bookID, customerID);//.OrderBy(x => x.ID);
            query = query.Paging<Rate>(ref page);
            var result = query.Select(x => new RateViewDTO()
            {
                ID = x.ID,
                CustomerID = x.Customer.FirstName + " " + x.Customer.LastName,
                Title = x.Title,
                BookID = x.Book.Name,
                Rating = x.Rating

            });
            return result.ToList();
        }

        public RateDTO GetByID(int id)
        {
            var result = rateDAO.GetByID(id);
            if (ValidationHelpers.IsValid(result))
            {
                var rate = new RateDTO(result);
                return rate;
            }
            return null;
        }

        #endregion Get
    }
}
