﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class AccountBUS : IAccountBUS
    {
        #region Contructors

        private readonly IAccountDAO accountDAO;
        public AccountBUS()
        {
            accountDAO = new AccountDAO();
        }

        public AccountBUS(IAccountDAO customerDAO)
        {
            this.accountDAO = customerDAO;
        }

        #endregion Contructors

        #region Role
        public List<RoleDTO> GetAllRoles()
        {
            var result = accountDAO.GetAllRoles().Select( x => new RoleDTO() {
                ID = x.ID,
                Name = x.Name,
                Description = x.Description
            });
            return result.ToList();
        }
        public Role GetRoleByID(int ID, string roleName)
        {
            return accountDAO.GetRoleByID(ID, roleName);
        }

        #endregion Role

        #region Edit Password

        public bool ChangeCustomerPassword(int ID, string Email, string Password)
        {
            Password = EncryptionHelpers.Encryption(Password);
            return accountDAO.ChangeCustomerPassword(ID, Email, Password);
        }

        public bool ChangeEmployeePassword(int ID, string Email, string Password)
        {
            Password = EncryptionHelpers.Encryption(Password);
            return accountDAO.ChangeEmployeePassword(ID, Email, Password);
        }

        #endregion Edit Password

        #region Login

        public Employee Login(string email, string password)
        {
            var result = accountDAO.GetEmployee(email);
            if (result != null && result.IsActive)
            {
                result.Password = EncryptionHelpers.Decryption(result.Password);
                if (result.Password == password)
                {                    
                    return result;
                }
                
            }
            return null;
        }

        public Customer ShopLogin(string email, string password)
        {
            var result = accountDAO.GetCustomer(email);
            if (result != null && result.IsActive)
            {
                result.Password = EncryptionHelpers.Decryption(result.Password);
                if (result.Password == password)
                {
                    return result;
                }

            }
            return null;
        }

        #endregion Login


    }
}
