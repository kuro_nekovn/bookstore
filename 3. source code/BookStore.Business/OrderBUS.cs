﻿using BookStore.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.DataAccess.Interfaces;
using BookStore.DataAccess;
using BookStore.DataTransfer;
using BookStore.Commons.Extentions;
using BookStore.Commons.Helpers;

namespace BookStore.Business
{
    public class OrderBUS : IOrderBUS
    {
        #region Contructors

        private readonly IOrderDAO orderDAO;
        public OrderBUS()
        {
            orderDAO = new OrderDAO();
        }

        public OrderBUS(IOrderDAO orderDAO)
        {
            this.orderDAO = orderDAO;
        }

        #endregion Contructors

        #region Create

        public OrderDTO Create(OrderDTO order)
        {
            if (ValidationHelpers.IsValid(order))
            {
                Order createOrder = new Order();
                createOrder.CustomerID = order.CustomerID;
                createOrder.EmployeeID = order.EmployeeID;
                createOrder.Address = order.Address;
                createOrder.TotalPrice = order.TotalPrice;
                createOrder.DateCreate = DateTime.Now;
                var result = orderDAO.Create(createOrder);
                if (result != null)
                {
                    order.ID = result.ID;
                    return order;
                }
            }
            return null;
        }

        #endregion Create

        #region Delete

        public bool Delete(int orderID)
        {
            return orderDAO.Delete(orderID);
        }

        #endregion Delete

        #region Edit

        public OrderDTO Edit(OrderDTO order)
        {           
            var result = orderDAO.Edit(order);
            if (result != null)
            {
                return new OrderDTO(result);
            }
            return null;
        }

        #endregion Edit

        #region Get

        public List<ItemDTO> GetItem()
        {
            return GetAll().Select(x => new ItemDTO()
            {
                ID = x.ID,
                Name = x.ID + " - Date: " + x.DateCreate.ToString()
            }).ToList();
        }
        public List<Order> GetAll()
        {
            return orderDAO.GetAll(null, null);
        }

        public List<OrderViewDTO> OrderPage(ref Pagination page,  string employeeID, string customerID)
        {
            List<Order> query = orderDAO.GetAll(employeeID, customerID);//.OrderBy(x => x.ID);
            query = query.Paging<Order>(ref page);
            var result = query.Select(x => new OrderViewDTO()
            {
                ID = x.ID,
                CustomerID = x.Customer.FirstName + " " + x.Customer.LastName,
                EmployeeID = x.Employee.FirstName + " " + x.Employee.LastName,
                TotalPrice = x.TotalPrice == null ? 0 : (double)x.TotalPrice,
                OrderDate = (DateTime)x.DateCreate
            });
            return result.ToList();
        }

        public List<OrderViewDTO> OrderShoppingPage(ref Pagination page, string customerID)
        {
            List<Order> query = orderDAO.GetAll(null, null).FindAll(x => x.Customer.Email == customerID);//.OrderBy(x => x.ID);
            query = query.Paging<Order>(ref page);
            var result = query.Select(x => new OrderViewDTO()
            {
                ID = x.ID,
                CustomerID = x.Customer.FirstName + " " + x.Customer.LastName,
                EmployeeID = x.Employee.FirstName + " " + x.Employee.LastName,
                TotalPrice = x.TotalPrice == null ? 0 : (double)x.TotalPrice,
                OrderDate = (DateTime)x.DateCreate
            });
            return result.ToList();
        }

        public List<OrderDetail> GetOrderDetailByOrderID(int ID)
        {
            return orderDAO.GetOrderDetailsByOrderID(ID);
        }

        public Order GetByID(int id)
        {
            return orderDAO.GetByID(id);
        }

        #endregion Get
    }
}
