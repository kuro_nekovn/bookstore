/* Start BX slider*/
jQuery(document).ready(function($){	   
	/* BX slider 1*/
	   if ($('.slider1').length){
			$('.slider1').bxSlider({ slideWidth: 142, minSlides: 1, maxSlides: 8, slideMargin: 18,  speed: 1500  });
		}
	   if ($('.slider2').length){
			$('.slider2').bxSlider({ slideWidth: 270,   mode: 'horizontal',  useCSS: false, easing: 'easeOutElastic',  speed: 2000 });
		}
		if ($('.slider3').length){
			$('.slider3').bxSlider({ slideWidth: 425, minSlides: 1, maxSlides: 2, slideMargin: 0, slideMargin: 18});
		}
		if ($('.slider4').length){
			$('.slider4').bxSlider({ mode: 'fade', slideWidth: 270, minSlides: 1, maxSlides: 1, slideMargin: 1, slideMargin: 0 });
		}
		 if ($('.slider5').length){
			$('.slider5').bxSlider({ slideWidth: 870,   mode: 'horizontal',  useCSS: false, easing: 'easeOutElastic',  speed: 2000 });
		}
		if ($('.slider6').length){
			$('.slider6').bxSlider({ slideWidth: 155, minSlides: 1, maxSlides: 4, slideMargin: 18,  speed: 1500  });
		}
		if ($('.slider7').length){
			$('.slider7').bxSlider({ slideWidth: 1170,   mode: 'horizontal',  useCSS: false, easing: 'easeOutElastic',  speed: 2000 });
		}
	/* BX slider 1*/
	});
	/* End BX slider*/
	