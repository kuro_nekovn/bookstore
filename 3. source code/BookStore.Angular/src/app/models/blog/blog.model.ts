export interface INewBlog {
    ID: number;
    Title: string;
    BlogImg: string;
    UserCreateName: string;
    CreateDate: Date;
    TotalComment: number;
}

export class NewBlog implements INewBlog {
    ID: number;
    Title: string;
    BlogImg: string;
    UserCreateName: string;
    Sumary: string;
    CreateDate: Date;
    TotalComment: number;
}

export class BlogDetail implements INewBlog {
    ID: number;
    Title: string;
    BlogImg: string;
    UserCreateName: string;
    Content: string;
    CreateDate: Date;
    TotalComment: number;
}
