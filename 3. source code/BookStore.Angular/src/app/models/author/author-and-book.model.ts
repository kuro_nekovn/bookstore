import { Author } from './author.model';
import { Book } from '../book/book-main.model';

export interface IAuthorBook {
    AuthorInfomation: Author;
    BookInformation: Book[];
}

export class AuthorBook implements IAuthorBook {
    AuthorInfomation: Author;
    BookInformation: Book[];
}

