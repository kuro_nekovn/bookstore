export interface IAuthor {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Sumary: string;
}

export class Author implements IAuthor {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Sumary: string;
}

export class AuthorInput implements IAuthor {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Sumary: string;
    Address: string;
    Birthday: Date;
    Phone: string;
    Content: string;
}
