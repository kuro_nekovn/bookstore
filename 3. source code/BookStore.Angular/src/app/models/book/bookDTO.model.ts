import { BookInput } from './book-main.model';
import { BookImage } from './book-image.model';

export interface IBookDTO {
    BookDetail: BookInput;
    BookImages: BookImage[];
  }

export class BookDTO implements IBookDTO {
  BookDetail: BookInput;
  BookImages: BookImage[];
}
