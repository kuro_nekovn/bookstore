export interface IRandomBooks {
    BookID: number;
    BookName: string;
    AuthorName: string;
    Summary: string;
    BookImg: string;
    Rating: number;
    Price: number;
}

export class RandomBook implements IRandomBooks {
    BookID: number;
    BookName: string;
    AuthorName: string;
    Summary: string;
    BookImg: string;
    Rating: number;
    Price: number;
}

