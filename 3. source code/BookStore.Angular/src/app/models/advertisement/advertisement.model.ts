
export interface IAdvertisement {
  ID: number;
  Description: string;
  ImgUrl: string;
  DateCreate: Date;
  DateRelease: Date;
  DateExpire: Date;
}

export class Advertisement implements IAdvertisement {
  ID: number;
  Description: string;
  ImgUrl: string;
  DateCreate: Date;
  DateRelease: Date;
  DateExpire: Date;
}

