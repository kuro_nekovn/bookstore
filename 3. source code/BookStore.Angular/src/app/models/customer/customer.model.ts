export interface ICustomer {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Point: number;
    Email: string;
}

export class Customer implements ICustomer {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Point: number;
    Email: string;
}

export class CustomerInput implements ICustomer {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Point: number;
    Address: string;
    Birthday: Date;
    Phone: string;
    Email: string;
    Password: string;
}
