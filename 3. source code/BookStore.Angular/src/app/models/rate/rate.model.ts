export interface IRate {
    ID: number;
    Rating: number;
    Content: string;
    Title: string;
}

export class Rate implements IRate {
    ID: number;
    Rating: number;
    Content: string;
    Title: string;
    BookName: string;
    CustomerName: string;
    BookID: number;
}

export class NewRate implements IRate {
    ID: number;
    Rating: number;
    Content: string;
    Title: string;
    AuthorName: string;
    BookName: string;
    CustomerName: string;
    BookImg: string;
}
