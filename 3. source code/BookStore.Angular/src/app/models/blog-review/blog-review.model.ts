export interface IBlogReview {
    ID: number;
    Title: string;
    Email: string;
    Content: string;
}

export class BlogReview implements IBlogReview {
    ID: number;
    Title: string;
    Email: string;
    Content: string;
    CreateDate: Date;
}

export class BlogReviewInput implements IBlogReview {
    ID: number;
    Title: string;
    Email: string;
    BlogName: string;
    Content: string;
  }

