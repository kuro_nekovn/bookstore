export interface IGenre {
  ID: number;
  Name: string;
  Description: string;
}

export class Genre implements IGenre {
  ID: number;
  Name: string;
  Description: string;
}
