export interface IUploadModel {
    Data: FormData;
    Option: string;
}

export class UploadModel implements IUploadModel {
    Data: FormData;
    Option: string;
}
