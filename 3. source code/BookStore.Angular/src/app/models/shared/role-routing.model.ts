export interface IRoleRouting {
    publisher: string[];
    author: string[];
    customer: string[];
    employee: string[];
    blog: string[];
    advertisement: string[];
    blogReview: string[];
    genre: string[];
    book: string[];
    rate: string[];
    order: string[];
    receipt: string[];
    complain: string[];
    roles: string[];
}

export class RoleRouting implements IRoleRouting {
    publisher: string[] = ['Admin', 'StockKeeper'];
    author: string[] = ['Admin', 'StockKeeper'];
    customer: string[] = ['Admin', 'Salesman'];
    employee: string[] = ['Admin', 'HR'];
    blog: string[] = ['Admin', 'StockKeeper', 'HR', 'Salesman'];
    advertisement: string[] = ['Admin', 'Salesman'];
    blogReview: string[] = ['Admin', 'StockKeeper', 'HR', 'Salesman'];
    genre: string[] = ['Admin', 'StockKeeper'];
    book: string[] = ['Admin', 'StockKeeper'];
    rate: string[] = ['Admin', 'Salesman'];
    order: string[] = ['Admin', 'Salesman'];
    receipt: string[] = ['Admin', 'StockKeeper'];
    complain: string[] = ['Admin', 'StockKeeper', 'Salesman'];
    roles: string[] = ['Admin'];
}

