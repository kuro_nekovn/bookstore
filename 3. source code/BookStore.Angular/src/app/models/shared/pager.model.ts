export interface IPage {
    TotalItems: number;
    PageSize: number;
    Currentpage: number;
}

export class Pagination implements IPage {
    // tslint:disable-next-line:no-inferrable-types
    TotalItems: number = 0;
    // tslint:disable-next-line:no-inferrable-types
    PageSize: number = 1;
    // tslint:disable-next-line:no-inferrable-types
    Currentpage: number = 1;
}

export class Pager {
    totalItems: number;

    pageSize: number;
    totalPages: number;
    startPage: number;
    endPage: number;

    index: number;
    startIndex: number;
    endIndex: number;

    pages: number[];
}
