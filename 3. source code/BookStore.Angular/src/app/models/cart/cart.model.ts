export interface ICart {
    ID: number;
    BookID: number;
    BookName: string;
    BookImg: string;
    AuthorName: string;
    Quantity: number;
    Price: number;
}

export class Cart implements ICart {
    ID: number;
    BookID: number;
    BookName: string;
    BookImg: string;
    AuthorName: string;
    Quantity: number;
    Price: number;
}

export interface ICartNav {
    TotalItem: number;
    TotalPrice: number;
}
export class CartNav implements ICartNav {
    TotalItem: number;
    TotalPrice: number;
}
