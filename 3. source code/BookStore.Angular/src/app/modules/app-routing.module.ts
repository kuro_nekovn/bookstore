import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

// library
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

// import { NotificationComponent } from '../components/shared/notification/notification.component';
// import { MainLayoutComponent } from '../components/shared/main-layout/main-layout.component';
// import { NoneLayoutComponent } from '../components/shared/none-layout/none-layout.component';
import { PageNotFoundComponent } from '../components/shared';
import { MainLayoutComponent } from '../components/shared/main-layout/main-layout.component';

// sub component
import { HomeHotBookComponent } from '../components/home/home-hot-book/home-hot-book.component';
import { HomeFutureBookComponent } from '../components/home/home-future-book/home-future-book.component';
import { HomeAuthorComponent } from '../components/home/home-author/home-author.component';
import { HomeBlogComponent } from '../components/home/home-blog/home-blog.component';

// main components
import { HomeComponent } from '../components/home/home/home.component';
import { ContactComponent } from '../components/contact/contact.component';
import { BookComponent } from '../components/book/book.component';
import { BookDetailComponent } from '../components/book-detail/book-detail.component';
import { BlogComponent } from '../components/blog/blog.component';
import { BlogDetailComponent } from '../components/blog-detail/blog-detail.component';
import { CartComponent } from '../components/cart/cart.component';
import { AboutUsComponent } from '../components/about-us/about-us.component';
import { LoginComponent } from '../components/login/login.component';
import { HomeSaleComponent } from '../components/home/home-sale/home-sale.component';
import { SanitizeHtmlPipe } from '../components/shared/extension/html-convert.component';
import { CartService } from '../services/cart/cart-service';
import { ChangePasswordComponent } from '../components/change-password/change-password.component';
import { RegistrationComponent } from '../components/registration/registration.component';
import { ProfileComponent } from '../components/profile/profile.component';
import { AuthGuard } from '../components/ultities/auth.guard';
import { NoAuthGuard } from '../components/ultities/no-auth.guard';
import { HistoryComponent } from '../components/history/history.component';
import { OrderComponent } from '../components/order/order.component';

const appRoutes: Routes = [
    {   path: '', redirectTo: '/home', pathMatch: 'full' },
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            { path: 'contact', component: ContactComponent },

            { path: 'book', component: BookComponent },
            { path: 'searchbyname/:id', component: BookComponent },
            { path: 'searchbyauthor/:id', component: BookComponent },
            { path: 'searchbypublisher/:id', component: BookComponent },
            { path: 'searchbygenre/:id', component: BookComponent },

            { path: 'book-detail/:id', component: BookDetailComponent },
            { path: 'blog', component: BlogComponent },
            { path: 'blog-detail/:id', component: BlogDetailComponent },
            { path: 'about-us', component: AboutUsComponent },
        ]
    },
    { path: 'cart', component: CartComponent },
    { path: 'login', component: LoginComponent, canActivate: [NoAuthGuard] },
    { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard] },
    { path: 'registry', component: RegistrationComponent, canActivate: [NoAuthGuard] },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    { path: 'history', component: HistoryComponent, canActivate: [AuthGuard]},
    { path: 'order-detail/:id', component: OrderComponent, canActivate: [AuthGuard]},
    { path: 'page-not-found', component: PageNotFoundComponent },
    { path: 'home', component: HomeComponent },
    // { path: 'unauthorized', component: UnauthorizedComponent, canActivate: [AuthGuard]},
    { path: '**', component: PageNotFoundComponent },
];

@NgModule({
    providers: [
        CartService,
    ],
    declarations: [
        // NotificationComponent,
        PageNotFoundComponent,
        MainLayoutComponent,

        HomeComponent,
        HomeHotBookComponent,
        HomeFutureBookComponent,
        HomeAuthorComponent,
        HomeBlogComponent,
        HomeSaleComponent,

        ContactComponent,
        BookComponent,
        BookDetailComponent,
        BlogComponent,
        BlogDetailComponent,
        CartComponent,
        AboutUsComponent,

        ChangePasswordComponent,
        LoginComponent,
        RegistrationComponent,
        ProfileComponent,

        HistoryComponent,
        OrderComponent,

        SanitizeHtmlPipe,
    ],
    imports: [
        RouterModule.forRoot(appRoutes,
            { enableTracing: true
                 }
          ),
        BrowserModule,
        CommonModule,
        FormsModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        AngularDateTimePickerModule,
    ],
    exports: [
        // NotificationComponent,
        PageNotFoundComponent,
        MainLayoutComponent,

        HomeComponent,
        HomeHotBookComponent,
        HomeFutureBookComponent,
        HomeAuthorComponent,
        HomeBlogComponent,
        HomeSaleComponent,

        ContactComponent,
        BookComponent,
        BookDetailComponent,
        BlogComponent,
        BlogDetailComponent,
        CartComponent,
        AboutUsComponent,

        HistoryComponent,
        OrderComponent,

        ChangePasswordComponent,
        LoginComponent,
        RegistrationComponent,
        ProfileComponent
    ],
})

export class AppRoutingModule {
}
