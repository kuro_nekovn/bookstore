import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerInput } from 'src/app/models/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { UploadService } from 'src/app/services/shared/upload.service';
import { formatDate } from '@angular/common';
import { AccountService } from 'src/app/services/account/account.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  form: FormGroup;
  dateTime: Date = new Date();
  customer: CustomerInput;
  thumbnailFile: File;
  isReady: Boolean = false;
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private customerService: CustomerService,
    private uploadService: UploadService,
    private accountService: AccountService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.getCustomer();
  }


  getCustomer() {
    this.customerService.getProfile().subscribe(
      (data: CustomerInput) => {
        this.customer = data;
        this.form = this.formBuilder.group({
          'LastName': [data.LastName , Validators.required  ],
          'FirstName': [data.FirstName , Validators.required  ],
          'Email': [data.Email ,  Validators.compose([Validators.required, Validators.email])  ],
          'Phone': data.Phone ,
          'Sex': data.Sex ? 'true' : 'false' ,
          'ImgUrl': data.ImgUrl,
          'Address': data.Address,
        });
        this.isReady = true;
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      });
  }

  get FirstName() { return this.form.get('FirstName'); }
  get LastName() { return this.form.get('LastName'); }
  get Email() { return this.form.get('Email'); }
  get Sex() { return this.form.get('Sex'); }
  get Phone() { return this.form.get('Phone'); }
  get Address() { return this.form.get('Address'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }

  onThumbnailFileChanged(event) {
    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'customer').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
      });
  }

  onSubmit(formValue) {
    if (this.form.valid) {
      const customerInput = new CustomerInput();
      customerInput.FirstName = formValue.FirstName;
      customerInput.LastName = formValue.LastName;
      customerInput.Email = formValue.Email;
      customerInput.Phone = formValue.Phone;
      customerInput.Sex = formValue.Sex;
      customerInput.ImgUrl = formValue.ImgUrl;
      customerInput.Point = 0;
      customerInput.Address = formValue.Address;
      this.customerService.editCustomer(customerInput).subscribe(
        (data: CustomerInput) => {
          this._notifier.notify('success', 'Cập nhật thay đổi thành công!');
        },
        error => {
          this._notifier.notify('error', 'Cập nhật thất bại');
      });
    }
  }


}
