import { Component, OnInit, Input, HostListener } from '@angular/core';
import { RandomBook } from 'src/app/models';
import { CartService } from 'src/app/services/cart/cart-service';
import { Cart } from 'src/app/models/cart/cart.model';

@Component({
  selector: 'app-home-sale, .app-home-sale',
  templateUrl: './home-sale.component.html',
  styleUrls: ['./home-sale.component.css']
})
export class HomeSaleComponent implements OnInit {
  @Input() bookRandom: RandomBook[];
  constructor(
    private cartService: CartService,
  ) { }

  ngOnInit() {
  }
  @HostListener('addCart')
  addCart(tempCart: RandomBook) {
    const result = new Cart();
    result.ID = tempCart.BookID;
    result.BookName = tempCart.BookName;
    result.AuthorName = tempCart.AuthorName;
    result.Price = tempCart.Price;
    result.Quantity = 1;
    result.BookImg = tempCart.BookImg;
    this.cartService.update(result);
  }
}
