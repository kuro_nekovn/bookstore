import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Author } from 'src/app/models/author/author.model';
import { Book } from 'src/app/models/book/book-main.model';
import { Cart } from 'src/app/models/cart/cart.model';
import { CartService } from 'src/app/services/cart/cart-service';
declare var $: any;

@Component({
  selector: 'app-home-author, .app-home-author',
  templateUrl: './home-author.component.html',
  styleUrls: ['./home-author.component.css']
})
export class HomeAuthorComponent implements OnInit {
  @Input() author: Author;
  @Input() books: Book[];
  randomBook: Book;
  constructor(
    private cartService: CartService,
  ) { }

  ngOnInit() {
    this.randomBook = this.books[Math.floor(Math.random() * this.books.length)];
    $.getScript('assets/js/social.js');
  }

  @HostListener('addCart')
  addCart(tempCart: Book) {
    const result = new Cart();
    result.ID = tempCart.ID;
    result.BookName = tempCart.Name;
    result.AuthorName = tempCart.AuthorName;
    result.Price = tempCart.Price;
    result.Quantity = 1;
    result.BookImg = tempCart.ImgUrl;
    this.cartService.update(result);
  }
}
