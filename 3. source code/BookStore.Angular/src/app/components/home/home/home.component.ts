import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book/book.service';
import { RandomBook } from 'src/app/models';
import { AuthorBook } from 'src/app/models/author/author-and-book.model';
import { AuthorService } from 'src/app/services/author/author.service';
import { NotifierService } from 'angular-notifier';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  books: RandomBook[];
  authorBook: AuthorBook;
  hotBook: RandomBook[];
  saleBook: RandomBook[];
  constructor(
    private bookService: BookService,
    private authorService: AuthorService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.getBooks();
    this.getRandomAuthor();
  }
  reload() {
    $.getScript('assets/js/hot-book.js');
    $.getScript('assets/js/future-book.js');
    $.getScript('assets/js/social.js');

  }
  getBooks() {
    this.bookService.getRandomBook().subscribe(
      (data: RandomBook[]) => {
        this.books = data;
        this.hotBook = this.getRandomBooks(6);
        this.saleBook = this.getRandomBooks(3);
        this.reload();
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      }
    );
  }
  getRandomNumber(): number {
    // tslint:disable-next-line:radix
    return Math.floor((this.books.length - 1) * Math.random());
  }
  getRandomBooks(count: number): RandomBook[] {
    const result: RandomBook[] = [];
    for (let index = 0; index < count; index++) {
      result.push(this.books[this.getRandomNumber()]);
    }
    return result;
  }
  getRandomAuthor() {
    this.authorService.getRandomAuthor().subscribe(
      (data: AuthorBook) => {
        this.authorBook = data;
        this.reload();
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      }
    );
  }
}
