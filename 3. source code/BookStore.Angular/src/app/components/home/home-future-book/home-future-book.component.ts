import { Component, OnInit, Input } from '@angular/core';
import { RandomBook } from 'src/app/models';

@Component({
  selector: 'app-home-future-book, .app-home-future-book',
  templateUrl: './home-future-book.component.html',
  styleUrls: ['./home-future-book.component.css']
})
export class HomeFutureBookComponent implements OnInit {
  @Input() futureBook: RandomBook[];
  constructor() { }

  ngOnInit() {
  }

}
