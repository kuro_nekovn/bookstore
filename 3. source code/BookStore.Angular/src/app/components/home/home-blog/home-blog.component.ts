import { Component, OnInit } from '@angular/core';
import { NewBlog } from 'src/app/models/blog/blog.model';
import { BlogService } from 'src/app/services/blog/blog.service';
import { NotifierService } from 'angular-notifier';
declare var $: any;

@Component({
  selector: 'app-home-blog, .app-home-blog',
  templateUrl: './home-blog.component.html',
  styleUrls: ['./home-blog.component.css']
})
export class HomeBlogComponent implements OnInit {
  newBlogs: NewBlog[];
  constructor(
    private blogService: BlogService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.getNewBlog();
  }
  getNewBlog() {
    this.blogService.getNewBlogs().subscribe(
      (data: NewBlog[]) => {
        this.newBlogs = data;
        $.getScript('assets/js/home-blog.js');
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      }
    );
  }
}
