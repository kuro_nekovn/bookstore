import { Component, OnInit, Input, HostListener } from '@angular/core';
import { RandomBook } from 'src/app/models';
import { BookService } from 'src/app/services/book/book.service';
import { Cart } from 'src/app/models/cart/cart.model';
import { CartService } from 'src/app/services/cart/cart-service';
declare var $: any;

@Component({
  selector: 'app-home-hot-book, .app-home-hot-book',
  templateUrl: './home-hot-book.component.html',
  styleUrls: ['./home-hot-book.component.css']
})
export class HomeHotBookComponent implements OnInit {

  @Input() randomBooks: RandomBook[];
  constructor(
    private bookService: BookService,
    private cartService: CartService,
  ) { }

  ngOnInit() {
  }
  @HostListener('addCart')
  addCart(tempCart: RandomBook) {
    const result = new Cart();
    result.ID = tempCart.BookID;
    result.BookName = tempCart.BookName;
    result.AuthorName = tempCart.AuthorName;
    result.Price = tempCart.Price;
    result.Quantity = 1;
    result.BookImg = tempCart.BookImg;
    this.cartService.update(result);
  }
}
