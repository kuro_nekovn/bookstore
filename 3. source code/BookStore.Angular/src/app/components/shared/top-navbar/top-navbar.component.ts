import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart-service';
import { CartNav } from 'src/app/models/cart/cart.model';
import { AccountService } from 'src/app/services/account/account.service';
import { FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { AccountInput } from 'src/app/models/account/account.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-navbar, .app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {
  totalPrice = 0;
  totalItem = 0;
  userName = '';
  isLogon: Boolean = false;
  form: FormGroup;
  constructor(
    private cartService: CartService,
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }
  index = 0;
  ngOnInit() {
    this.isLogon = this.cartService.hasLogin();
    if (this.isLogon) {
      this.userName = this.accountService.getUserName();
    }
    this.cartService.change.subscribe((data: CartNav) => {
      this.totalItem = data.TotalItem;
      this.totalPrice = data.TotalPrice;
    });
    this.cartService.isLogin.subscribe((data: Boolean) => {
      this.isLogon = data;
      if (this.isLogon) {
        this.userName = this.accountService.getUserName();
      }
    });
    this.updateCart();
  }

  logOut() {
    localStorage.removeItem('UserToken');
    this.router.navigate(['/home']);
    this.cartService.checkLogin();
  }

  updateCart() {
    const temp = this.cartService.updateCartForNav();
    this.totalItem = temp.TotalItem;
    this.totalPrice = temp.TotalPrice;
  }
}



