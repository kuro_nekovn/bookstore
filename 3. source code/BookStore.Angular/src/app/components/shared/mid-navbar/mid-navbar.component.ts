import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart/cart-service';

@Component({
  selector: 'app-mid-navbar, .app-mid-navbar',
  templateUrl: './mid-navbar.component.html',
  styleUrls: ['./mid-navbar.component.css']
})
export class MidNavbarComponent implements OnInit {
  searchInput = '';
  isLogon: Boolean = false;
  constructor(
    private router: Router,
    private cartService: CartService,
  ) { }

  ngOnInit() {
    this.isLogon = this.cartService.hasLogin();
    this.cartService.isLogin.subscribe((data: Boolean) => {
      this.isLogon = data;
    });
  }

  search() {
    this.router.navigateByUrl(`/searchbyname/${this.searchInput}`);
  }
}
