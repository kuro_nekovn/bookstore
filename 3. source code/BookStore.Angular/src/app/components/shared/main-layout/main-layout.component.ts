import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart-service';
import { Advertisement } from 'src/app/models/advertisement/advertisement.model';
import { AdvertisementService } from 'src/app/services/advertisement/advertisement.service';
import { ItemDTO } from 'src/app/models';
import { AuthorService } from 'src/app/services/author/author.service';
import { Rate, NewRate } from 'src/app/models/rate/rate.model';
import { RateService } from 'src/app/services/rate/rate.service';
import { NotifierService } from 'angular-notifier';
declare var $: any;

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit, AfterViewInit {
  title = 'Tiêu đề';
  advertisements: Advertisement[];
  authors: ItemDTO[];
  rates: NewRate[];
  constructor(
    private cartService: CartService,
    private advertismentService: AdvertisementService,
    private authorService: AuthorService,
    private rateService: RateService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.cartService.updateTitle.subscribe((data: string) => {
      this.title = data;
    });
    this.getAdvertisements();
    this.getAuthor();
    this.getNewRate();
  }
  ngAfterViewInit() {
    $.getScript('assets/js/custom.js');
  }

  getAdvertisements() {
    this.advertismentService.getAdvertisements().subscribe(
      (data: Advertisement[]) => {
        this.advertisements = data;
        $.getScript('assets/js/custom.js');
        this._notifier.notify('sucess', 'load quảng cáo thành công');
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      }
    );
  }

  getAuthor() {
    this.authorService.getAuthorItem().subscribe(
      (data: ItemDTO[]) => {
        this.authors = data;
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      }
    );
  }

  getNewRate() {
    this.rateService.getNewRate().subscribe(
      (data: NewRate[]) => {
        this.rates = data;
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      }
    );
  }
}
