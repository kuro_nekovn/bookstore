import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerInput } from 'src/app/models/customer/customer.model';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { UploadService } from 'src/app/services/shared/upload.service';
import { formatDate } from '@angular/common';
import { AccountService } from 'src/app/services/account/account.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  form: FormGroup;
  dateTime: Date = new Date();
  customer: CustomerInput;
  thumbnailFile: File;
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private customerService: CustomerService,
    private uploadService: UploadService,
    private accountService: AccountService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'FirstName': ['', Validators.required  ],
      'LastName': ['', Validators.required  ],
      'Email': ['', Validators.compose([Validators.required, Validators.email])],
      'Sex': 'true',
      'Birthday': this.dateTime,
      'Phone': '',
      'Address': '',
      'ImgUrl' : '',
      'Password': ['', Validators.required  ],
    });
  }
  get FirstName() { return this.form.get('FirstName'); }
  get LastName() { return this.form.get('LastName'); }
  get Email() { return this.form.get('Email'); }
  get Sex() { return this.form.get('Sex'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Phone() { return this.form.get('Phone'); }
  get Address() { return this.form.get('Address'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Password() { return this.form.get('Password'); }

  onThumbnailFileChanged(event) {
    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'customer').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);
        this._notifier.notify('sucess', 'Up hình thành công');
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this._notifier.notify('error', message);
      });
  }

  onSubmit(formValue) {
    if (this.form.valid) {
      const customerInput = new CustomerInput();
      customerInput.FirstName = formValue.FirstName;
      customerInput.LastName = formValue.LastName;
      customerInput.Email = formValue.Email;
      customerInput.Phone = formValue.Phone;
      customerInput.Sex = formValue.Sex;
      customerInput.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      customerInput.ImgUrl = formValue.ImgUrl;
      customerInput.Point = 0;
      customerInput.Address = formValue.Address;
      customerInput.Password = this.accountService.encriptPassword(formValue.Password);
      this.customerService.createCustomer(customerInput).subscribe(
        (data: CustomerInput) => {
          this._notifier.notify('sucess', 'Đăng kí thành công');
          this.router.navigateByUrl('/login');
        },
        error => {
          this._notifier.notify('error', 'Đăng kí thất bại');
      });
    }
  }


}
