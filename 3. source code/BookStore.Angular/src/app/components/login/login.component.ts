import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/services/account/account.service';
import { AccountInput } from 'src/app/models/account/account.model';
import { CartService } from 'src/app/services/cart/cart-service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private cartService: CartService,
    private router: Router,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Password': ['', Validators.required  ],
      'Email': ['',  Validators.compose([Validators.required, Validators.email])]
    });
  }
  get Password() { return this.form.get('Password'); }
  get Email() { return this.form.get('Email'); }

  loginUser() {
    if (!this.form.invalid) {
      const user = new AccountInput();
      user.Email = this.Email.value;
      user.Password = this.accountService.encriptPassword(this.Password.value);
      this.accountService.login(user).subscribe(
        (data: AccountInput) => {
          localStorage.setItem('UserToken', data.Password);
          this.cartService.checkLogin();
          this.router.navigateByUrl(`/home`);
        },
        error => {
           let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        });
    }
  }

}
