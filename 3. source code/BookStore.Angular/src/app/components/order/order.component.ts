import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart-service';
import { Cart, CartNav } from 'src/app/models/cart/cart.model';
import { OrderInput } from 'src/app/models/order/order.model';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  totalPrice = 0;
  isLogon: Boolean = false;
  carts: Cart[];
  constructor(
    private cartService: CartService,
    private route: ActivatedRoute,
    private orderService: OrderService,
  ) { }

  ngOnInit() {
    this.getOrderDetail();
      this.cartService.changeTitle('Giỏ hàng');
    }

    getOrderDetail() {
      const id = this.route.snapshot.params['id'];
      this.orderService.getOrder(id).subscribe(
        (data: Cart[]) => {
          this.carts = data;
          this.totalPrice = this.getTotalPrice();
        },
        error => {
          let message = error.message;
          if (error.error.Message) {
            message = error.error.Message;
          }
          alert(message);
      });
    }
    getTotalPrice(): number {
      let sum = 0;
      this.carts.forEach(element => {
          sum += element.Price * element.Quantity;
      });
      return sum;
  }
  }
