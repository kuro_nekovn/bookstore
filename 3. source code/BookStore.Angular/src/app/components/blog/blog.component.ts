import { Component, OnInit } from '@angular/core';
import { Pagination, PagingData, DataResult } from 'src/app/models';
import { NewBlog } from 'src/app/models/blog/blog.model';
import { BlogService } from 'src/app/services/blog/blog.service';
import { CartService } from 'src/app/services/cart/cart-service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  page: Pagination = new Pagination();
  blogs: NewBlog[];
  searchInput = '';
  constructor(
    private blogService: BlogService,
    private cartService: CartService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.cartService.changeTitle('Tin tức');
    this.page.PageSize = 9;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
    this.blogService.getBlogs(index, this.page.PageSize, this.searchInput)
      .subscribe(
        (data: DataResult < PagingData < NewBlog[] >> ) => {
          this.blogs = data.Data.Items;
          this.page = data.Data.Page;
          this.page.Currentpage = index;
        },
        // tslint:disable-next-line:no-shadowed-variable
        error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        });
  }
}
