import { Component, OnInit } from '@angular/core';
import { BlogDetail, NewBlog } from 'src/app/models/blog/blog.model';
import { BlogService } from 'src/app/services/blog/blog.service';
import { ActivatedRoute } from '@angular/router';
import { Pagination, DataResult, PagingData } from 'src/app/models';
import { BlogReview, BlogReviewInput } from 'src/app/models/blog-review/blog-review.model';
import { BlogReviewService } from 'src/app/services/blog-review/blog-review..service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CartService } from 'src/app/services/cart/cart-service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  blog: NewBlog;
  page: Pagination = new Pagination();
  blogReviews: BlogReview[];
  form: FormGroup;
  constructor(
    private blogService: BlogService,
    private blogReviewService: BlogReviewService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private cartService: CartService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.cartService.changeTitle('Thông tin bài viết');
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'Content': ['',  Validators.compose([Validators.required, Validators.maxLength(300)])],
      'Email': ['',  Validators.compose([Validators.required, Validators.email])]
    });
    this.getBlog();
    this.page.PageSize = 4;
    this.refreshData();
  }

  get Title() { return this.form.get('Title'); }
  get Content() { return this.form.get('Content'); }
  get Email() { return this.form.get('Email'); }

  getBlog() {
    const id = this.route.snapshot.params['id'];
    this.blogService.getBlog(id).subscribe(
      (data: NewBlog) => {
        this.blog = data;
        this._notifier.notify('success', 'Tải tin tức mới thành công');
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      });
  }

  refreshData(index: number = 1, message: string = null) {
    const id = this.route.snapshot.params['id'];
    this.blogReviewService.getBlogReviews(index, this.page.PageSize, id)
      .subscribe(
        (data: DataResult < PagingData < BlogReview[] >> ) => {
          this.blogReviews = data.Data.Items;
          this.page = data.Data.Page;
          this.page.Currentpage = index;
        },
        error => {
          let message = error.message;
          if (error.error.Message) {
            message = error.error.Message;
          }
        alert(message);
        });
  }

  onSubmit(formValue) {
    const result: BlogReviewInput = new BlogReviewInput();
    const id = this.route.snapshot.params['id'];
    result.Title = formValue.Title;
    result.BlogName = id;
    result.Email = formValue.Email;
    result.Content = formValue.Content;
    this.blogReviewService.createBlogReview(result).subscribe(
      (data: BlogReviewInput) => {
        this.form.setValue({
          Title: '',
          Email: '',
          Content: '',
        });
        this.refreshData();
        this.page.Currentpage = 1;
        this._notifier.notify('success', 'Đăng bình luận thành công');
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
         this._notifier.notify('error', message);
        alert(message);
      });
  }
}
