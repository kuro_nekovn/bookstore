import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { AccountService } from 'src/app/services/account/account.service';
import { AccountInput } from 'src/app/models/account/account.model';
import { NotifierService } from 'angular-notifier';
declare var $: any;


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private router: Router,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'ConfirmPassword': ['', Validators.required  ],
      'Password': ['', Validators.required  ],
    },
    {
      validator: passwordMatchValidator
    });
  }
  get ConfirmPassword() { return this.form.get('ConfirmPassword'); }
  get Password() { return this.form.get('Password'); }

  changePassword(fromVaule) {
    this._notifier.notify('success', 'Mật khẩu thay đổi thành công!');
    if (!this.form.invalid) {
      const accountInput = new AccountInput();
      accountInput.Password = this.accountService.encriptPassword(this.Password.value);
      $('#change-password-nav').modal('hide');
      this.resetInput();
      this.accountService.changePassword(accountInput).subscribe(
        (data: boolean) => {
            if (data) {
              this._notifier.notify('success', 'Mật khẩu thay đổi thành công!');
            } else { this._notifier.notify('error', 'Mật khẩu không chính xác'); }
        },
        error => {
           let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        }
      );
    }
  }

  resetInput() {
    this.form.patchValue({
      'Password': '',
      'ConfirmPassword': ''
    });
  }

  onPasswordInput() {
    if (this.form.hasError('passwordMismatch')) {
      this.ConfirmPassword.setErrors([{'passwordMismatch': true}]);
    } else {
      this.ConfirmPassword.setErrors(null);
    }
  }
}

export const passwordMatchValidator: ValidatorFn = (form: FormGroup): ValidationErrors | null => {
  if (form.get('Password').value === form.get('ConfirmPassword').value) {
    return null;
  } else {
    return {passwordMismatch: true};
  }
};
