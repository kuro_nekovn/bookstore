import { Component, OnInit } from '@angular/core';
import { Pagination, PagingData, DataResult } from 'src/app/models';
import { CartService } from 'src/app/services/cart/cart-service';
import { NotifierService } from 'angular-notifier';
import { OrderService } from 'src/app/services/order/order.service';
import { Order } from 'src/app/models/order/order.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  page: Pagination = new Pagination();
  historys: Order[];
  searchInput = '';
  constructor(
    private orderService: OrderService,
    private cartService: CartService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.cartService.changeTitle('Tin tức');
    this.page.PageSize = 9;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
    this.orderService.getOrders(index, this.page.PageSize)
      .subscribe(
        (data: DataResult < PagingData < Order[] >> ) => {
          this.historys = data.Data.Items;
          this.page = data.Data.Page;
          this.page.Currentpage = index;
        },
        // tslint:disable-next-line:no-shadowed-variable
        error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        });
  }

}
