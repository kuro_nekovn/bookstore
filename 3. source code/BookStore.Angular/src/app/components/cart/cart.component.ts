import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart-service';
import { Cart, CartNav } from 'src/app/models/cart/cart.model';
import { OrderInput } from 'src/app/models/order/order.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  totalPrice = 0;
  address = '';
  isLogon: Boolean = false;
  carts: Cart[];
  constructor(
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.isLogon = this.cartService.hasLogin();
    this.carts = this.cartService.getCarts();
      this.totalPrice = this.cartService.updateCartForNav().TotalPrice;
      this.cartService.changeTitle('Giỏ hàng');
      this.cartService.change.subscribe((data: CartNav) => {
        this.totalPrice = data.TotalPrice;
      });
    }

    update(temp: Cart) {
      this.cartService.increaseQuantity(temp.ID, temp.Quantity);
      alert(`Cập nhật số lượng sách ${temp.BookName} thành công`);
    }

    removeCart(temp: Cart) {
      this.carts = this.carts.filter(item => item.ID !== temp.ID);
      this.cartService.deleteCart(temp);
      alert(`Xoá sách ${temp.BookName} thành công`);
    }
    getSubPrice(): number {
      let sum = 0;
      this.carts.forEach(x => {
        sum += x.Price * x.Quantity;
      });
      return sum;
    }
    checkout() {
      if (this.address === '' ) {
        alert('yêu cầu nhập địa chỉ');
        return;
      }
      if (this.carts.length === 0) {
        alert('Giỏ hàng trống');
        return;
      }
      this.cartService.checkout(this.address).subscribe(
        (data: OrderInput) => {
          alert('Thanh toán thành công');
          this.cartService.remove();
          this.carts = [];
          this.totalPrice = 0;
        },
        error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        });
    }
  }
