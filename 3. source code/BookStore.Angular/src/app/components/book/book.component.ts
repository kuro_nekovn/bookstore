import { Component, OnInit, HostListener } from '@angular/core';
import { Book } from 'src/app/models/book/book-main.model';
import { Pagination, DataResult, PagingData, ItemDTO } from 'src/app/models';
import { BookService } from 'src/app/services/book/book.service';
import { GenreService } from 'src/app/services/genre/genre.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CartService } from 'src/app/services/cart/cart-service';
import { Cart } from 'src/app/models/cart/cart.model';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  page: Pagination = new Pagination();
  hasGenreChoosen = true;
  bookName: string;
  books: Book[];
  genres: ItemDTO[];
  genre = 0;
  authorID = 0;
  publisherID = 0;
  showGridView: Boolean = true;
  constructor(
    private bookService: BookService,
    private genreService: GenreService,
    private router: Router,
    private route: ActivatedRoute,
    private cartService: CartService,
    private _notifier: NotifierService,
  ) { }

  ngOnInit() {
    this.cartService.changeTitle('Sách');
    this.getCurrentSearch();
    this.page.PageSize = 9;
    this.refreshData();
    this.getGenres();
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.getCurrentSearch();
      this.refreshData();
  });
  }

  refreshData(index: number = 1, message: string = null) {
    this.bookService.getBooks(index, this.page.PageSize,
        this.bookName, this.authorID, this.publisherID, this.genre)
      .subscribe(
        (data: DataResult < PagingData < Book[] >> ) => {
          this.books = data.Data.Items;
          this.page = data.Data.Page;
          this.page.Currentpage = index;
        },
        // tslint:disable-next-line:no-shadowed-variable
        error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        });
  }

  getGenres() {
    this.genreService.getGenreItem()
    .subscribe(
      (data: ItemDTO[] ) => {
        this.genres = data;
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
        console.error(error);
      });
  }

  getCurrentSearch() {
    const url = this.router.url;
    if (url.includes('searchbyname')) {
      this.bookName = this.route.snapshot.params['id'];
    }
    if (url.includes('searchbyauthor')) {
      this.authorID = this.route.snapshot.params['id'];
    }
    if (url.includes('searchbypublisher')) {
      this.publisherID = this.route.snapshot.params['id'];
    }
    if (url.includes('searchbygenre')) {
      this.genre = this.route.snapshot.params['id'];
    }
  }

  @HostListener('addCart')
  addCart(tempCart: Book) {
    const result = new Cart();
    result.ID = tempCart.ID;
    result.BookName = tempCart.Name;
    result.AuthorName = tempCart.AuthorName;
    result.Price = tempCart.Price;
    result.Quantity = 1;
    result.BookImg = tempCart.ImgUrl;
    this.cartService.update(result);
  }

}
