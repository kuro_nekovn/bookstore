import { Component, OnInit, HostListener } from '@angular/core';
import { BookDTO } from 'src/app/models/book/bookDTO.model';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/services/book/book.service';
import { BookInput } from 'src/app/models/book/book-main.model';
import { BookImage } from 'src/app/models/book/book-image.model';
import { Pagination, PagingData, DataResult } from 'src/app/models';
import { Rate } from 'src/app/models/rate/rate.model';
import { RateService } from 'src/app/services/rate/rate.service';
import { CartService } from 'src/app/services/cart/cart-service';
import { NotifierService } from 'angular-notifier';
import { Cart } from 'src/app/models/cart/cart.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  page: Pagination = new Pagination();
  quantity = 1;
  rates: Rate[];
  book: BookInput;
  form: FormGroup;
  bookImages: BookImage[];
  constructor(
    private route: ActivatedRoute,
    private bookService: BookService,
    private rateService: RateService,
    private cartService: CartService,
    private _notifier: NotifierService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.cartService.changeTitle('Thông tin sách');
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'Content': ['',  Validators.compose([Validators.required, Validators.maxLength(300)])],
      'Rating': [5,  Validators.required]
    });
    this.page.PageSize = 2;
    this.getBook();
    this.refreshData();
  }

  get Title() { return this.form.get('Title'); }
  get Content() { return this.form.get('Content'); }
  get Rating() { return this.form.get('Rating'); }

  getBook() {
    const id = this.route.snapshot.params['id'];
    this.bookService.getBook(id).subscribe(
      (data: BookDTO) => {
        this.book = data.BookDetail;
        this.bookImages = data.BookImages;
        $.getScript('assets/js/future-book.js');
      },
      error => {
        let message = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      });
  }

  overviewContent(content: string): string {
    if (content.length > 500) {
      content = content.substring(0, 500);
    }
    return content;
  }

  refreshData(index: number = 1, message: string = null) {
    const id = this.route.snapshot.params['id'];
    this.rateService.getRatesByBook(index, this.page.PageSize, '', id, '').subscribe(
      (data: DataResult < PagingData < Rate[] >> ) => {
        this.rates = data.Data.Items;
        this.page = data.Data.Page;
        this.page.Currentpage = index;
      },
      error => {
        let message: string = error.message;
        if (error.error.Message) {
          message = error.error.Message;
        }
        alert(message);
      });
  }

  onSubmit(formValue) {
    const temp: Rate = new Rate();
    const id = this.route.snapshot.params['id'];
    temp.Title = formValue.Title;
    temp.Content = formValue.Content;
    temp.Rating = formValue.Rating;
    temp.BookID = id;
    this.rateService.createRate(temp).subscribe(
      (data: Rate) => {
        this.form.setValue({
          Title: '',
          Rating: 5,
          Content: '',
        });
        this.refreshData();
        this.page.Currentpage = 1;
        alert('Đăng bình luận thành công');
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
         this._notifier.notify('error', message);
        alert(message);
      });
  }

  @HostListener('addCart')
  addCart(tempCart: BookInput) {
    const result = new Cart();
    result.ID = tempCart.ID;
    result.BookName = tempCart.Name;
    result.AuthorName = tempCart.AuthorName;
    result.Price = tempCart.Price;
    result.Quantity = this.quantity;
    result.BookImg = tempCart.ImgUrl;
    this.cartService.update(result);
  }
}
