import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart-service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(
    private cartService: CartService,
  ) { }

  ngOnInit() {
    this.cartService.changeTitle('Liên hệ');
  }

}
