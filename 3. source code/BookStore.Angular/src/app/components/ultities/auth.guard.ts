import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AccountService } from 'src/app/services/account/account.service';
import { CartService } from 'src/app/services/cart/cart-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  _helper = new JwtHelperService();
  constructor(
    private _http: Router,
    private _accountService: AccountService,
    private _cartService: CartService,
  ) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('UserToken')) {
      if (!this._helper.isTokenExpired(localStorage.getItem('UserToken'))) {
       return true;
      }
      this._cartService.checkLogin();
      localStorage.removeItem('UserToken');
    }
    return false;
  }
}
