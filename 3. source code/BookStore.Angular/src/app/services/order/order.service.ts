
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { DataResult, PagingData } from 'src/app/models';
import { Order, OrderInput } from 'src/app/models/order/order.model';
import { Cart } from 'src/app/models/cart/cart.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  readonly baseUrl: string = 'api/order';

  items: Order[];

  constructor(
    private http: HttpClient
  ) {  }

  getOrders(index: number, pagesize: number): Observable<DataResult<PagingData<Order[]>>> {
    const url = `${this.baseUrl}/Orders?CurrentPage=${index}
      &pageSize=${pagesize}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Order[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getOrder(id: number): Observable<Cart[]> {
    const url = `${this.baseUrl}/getOrder/${id}`;

    const result = this.http.get<Cart[]>(url).pipe(
      retry(2)
    );

    return result;
  }
}
