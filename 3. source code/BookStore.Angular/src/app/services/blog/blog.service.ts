
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { RandomBook,  DataResult, PagingData, ItemDTO, } from 'src/app/models';
import { AuthorBook } from 'src/app/models/author/author-and-book.model';
import { NewBlog, BlogDetail } from 'src/app/models/blog/blog.model';
import { removeSummaryDuplicates } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  readonly baseUrl: string = 'api/blog';


  constructor(
    private http: HttpClient
  ) {  }

  getBlogs(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<NewBlog[]>>> {
    const url = `${this.baseUrl}/Blogs?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    const result = this.http.get<DataResult<PagingData<NewBlog[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getBlog(id: number): Observable<NewBlog> {
    const url = `${this.baseUrl}/getBlog/${id}`;

    const result = this.http.get<NewBlog>(url).pipe(
      retry(2)
    );
    return result;
  }

  //   return result;
  // }

  getNewBlogs(): Observable<NewBlog[]> {
    const url = `${this.baseUrl}/GetNewBlogs`;
    const result = this.http.get<NewBlog[]>(url).pipe(
         retry(2)
        );
    return result;
  }

  // deleteBook(id: number): Observable<boolean> {
  //   const url = `${this.baseUrl}/delete/${id}`;
  //   const result = this.http.get<boolean>(url);
  //   return result;
  // }

  //  deleteImage(ID: number): Observable<boolean> {
  //   const url = `${this.baseUrl}/deleteImage?ID=${ID}`;
  //   const result = this.http.get<boolean>(url);
  //   return result;
  // }

  // createBook(book: BookDTO): Observable<BookDTO> {
  //   const url = `${this.baseUrl}/create`;
  //   const result = this.http.post<BookDTO>(url, book);
  //   return result;
  // }

  // getBookItem(): Observable<ItemDTO[]> {
  //   const url = `${this.baseUrl}/GetItems`;
  //   const result = this.http.get<ItemDTO[]>(url);
  //   return result;
  // }
}
