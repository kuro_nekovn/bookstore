
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { RandomBook,  DataResult, PagingData, ItemDTO, } from 'src/app/models';
import { AuthorBook } from 'src/app/models/author/author-and-book.model';
import { NewBlog, BlogDetail } from 'src/app/models/blog/blog.model';
import { removeSummaryDuplicates } from '@angular/compiler';
import { BlogReview, BlogReviewInput } from 'src/app/models/blog-review/blog-review.model';

@Injectable({
  providedIn: 'root'
})
export class BlogReviewService {
  readonly baseUrl: string = 'api/blogreview';


  constructor(
    private http: HttpClient
  ) {  }

  getBlogReviews(index: number, pagesize: number, blogID: number): Observable<DataResult<PagingData<BlogReview[]>>> {
    const url = `${this.baseUrl}/BlogReviews?CurrentPage=${index}&pageSize=${pagesize}&blogID=${blogID}`;
    const result = this.http.get<DataResult<PagingData<BlogReview[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  createBlogReview(blogReview: BlogReviewInput): Observable<BlogReviewInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<BlogReviewInput>(url, blogReview);
    return result;
  }
}
