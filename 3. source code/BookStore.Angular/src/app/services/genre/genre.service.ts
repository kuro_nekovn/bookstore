import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { DataResult, PagingData, ItemDTO } from 'src/app/models';
import { Genre } from 'src/app/models/genre/genre.model';

@Injectable({
  providedIn: 'root'
})
export class GenreService {
  readonly baseUrl: string = 'api/genre';

  constructor(
    private http: HttpClient
  ) {  }

  getGenre(id: number): Observable<Genre> {
    const url = `${this.baseUrl}/getGenre/${id}`;

    const result = this.http.get<Genre>(url).pipe(
      retry(2)
    );

    return result;
  }

  getGenreItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
