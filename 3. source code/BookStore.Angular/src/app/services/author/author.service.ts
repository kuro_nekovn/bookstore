
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { RandomBook,  DataResult, PagingData, ItemDTO, } from 'src/app/models';
import { AuthorBook } from 'src/app/models/author/author-and-book.model';
import { Author } from 'src/app/models/author/author.model';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  readonly baseUrl: string = 'api/author';


  constructor(
    private http: HttpClient
  ) {  }



  getRandomAuthor(): Observable<AuthorBook> {
    const url = `${this.baseUrl}/GetRandomAuthor`;
    const result = this.http.get<AuthorBook>(url).pipe(
         retry(2)
        );
    return result;
  }

  getAuthors(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Author[]>>> {
    const url = `${this.baseUrl}/Authors?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Author[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getAuthorItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
