import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CustomerInput } from 'src/app/models/customer/customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  readonly baseUrl: string = 'api/customer';

  constructor(
    private http: HttpClient
  ) {  }

  getProfile(): Observable<CustomerInput> {
    const url = `${this.baseUrl}/getCustomer`;

    const result = this.http.get<CustomerInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editCustomer(customer: CustomerInput): Observable<CustomerInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<CustomerInput>(url, customer);
    return result;
  }

  createCustomer(customer: CustomerInput): Observable<CustomerInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<CustomerInput>(url, customer);
    return result;
  }
}
