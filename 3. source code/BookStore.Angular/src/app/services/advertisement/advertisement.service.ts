import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { DataResult, PagingData } from 'src/app/models';
import { Advertisement } from 'src/app/models/advertisement/advertisement.model';

@Injectable({
  providedIn: 'root'
})
export class AdvertisementService {
  readonly baseUrl: string = 'api/advertisement';

  items: Advertisement[];

  constructor(
    private http: HttpClient
  ) {  }

  getAdvertisements(): Observable<Advertisement[]> {
    const url = `${this.baseUrl}/getadvertisements`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<Advertisement[]>(url).pipe(
        retry(2)
    );
    return result;
  }
}
