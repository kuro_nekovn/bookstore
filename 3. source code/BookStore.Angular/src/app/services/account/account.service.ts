import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { DataResult, PagingData, } from 'src/app/models';
import { Md5 } from 'ts-md5/dist/md5';
import { AccountInput } from 'src/app/models/account/account.model';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  readonly baseUrl: string = 'api/account';
  _jwtHelper = new JwtHelperService();
  items: Account[];

  constructor(private http: HttpClient) {}

  encriptPassword(password: string): string {
    return Md5.hashStr(password).toString();
  }

  changePassword(account: AccountInput): Observable<boolean> {
    const url = `${this.baseUrl}/changepassword`;
    return this.http.post<boolean>(url, account);
  }

  login(user: AccountInput): Observable<AccountInput> {
    const headers = new HttpHeaders({'No-Auth': 'True'});
    headers.append('Content-Type', 'application/json');
    const url = `${this.baseUrl}/authenticate`;
    return this.http.post<AccountInput>(url, user,  {headers: headers});
  }

  getUserName(): string {
    const token = localStorage.getItem('UserToken');
    if (token) {
      return this._jwtHelper.decodeToken(token).unique_name;
    } else {
      return '';
    }
  }
}
