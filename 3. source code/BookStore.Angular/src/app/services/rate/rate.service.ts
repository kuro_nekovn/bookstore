
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { DataResult, PagingData, } from 'src/app/models';
import { Rate, NewRate } from 'src/app/models/rate/rate.model';

@Injectable({
  providedIn: 'root'
})
export class RateService {
  readonly baseUrl: string = 'api/rate';


  constructor(
    private http: HttpClient
  ) {  }

  getRatesByBook(index: number, pagesize: number,
    keyword: string, bookID: number, customerName: string)
    : Observable<DataResult<PagingData<Rate[]>>> {
      // tslint:disable-next-line:max-line-length
      const url = `${this.baseUrl}/Shoprates?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}&bookID=${bookID}&customerName=${customerName}`;
      // tslint:disable-next-line:prefer-const
      let result = this.http.get<DataResult<PagingData<Rate[]>>>(url).pipe(
        retry(2)
      );
    return result;
  }

  getNewRate(): Observable<NewRate[]> {
      // tslint:disable-next-line:max-line-length
      const url = `${this.baseUrl}/NewRate`;
      // tslint:disable-next-line:prefer-const
      let result = this.http.get<NewRate[]>(url).pipe(
        retry(2)
      );
    return result;
  }

  createRate(book: Rate): Observable<Rate> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<Rate>(url, book);
    return result;
  }
}
