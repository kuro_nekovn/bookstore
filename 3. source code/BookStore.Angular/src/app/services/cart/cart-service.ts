import { Injectable, Output, EventEmitter } from '@angular/core';
import { Cart, CartNav } from 'src/app/models/cart/cart.model';
import { OrderInput } from 'src/app/models/order/order.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { OrderDetail } from 'src/app/models/order-detail/order-detail.model';

@Injectable()
export class CartService {
  readonly baseUrl: string = 'api/order';
  carts: Cart[] = this.getCarts();
  cartNav: CartNav = new CartNav();

  constructor(
    private http: HttpClient
  ) {  }

  @Output() change: EventEmitter<CartNav> = new EventEmitter();
  @Output() updateTitle: EventEmitter<string> = new EventEmitter();
  @Output() isLogin: EventEmitter<Boolean> = new EventEmitter();

  update(temp: Cart) {
    if (!this.checkAvailable(temp)) {
        this.carts.push(temp);
    }
    this.setCarts();
    this.updateCartNav();
    this.change.emit(this.cartNav);
  }

  remove() {
    localStorage.removeItem('ShoppingCart');
    this.carts = [];
    this.cartNav.TotalItem = 0;
    this.cartNav.TotalPrice = 0;
    this.change.emit(this.cartNav);
  }
  getCarts(): Cart[] {
    const temp = localStorage.getItem('ShoppingCart');
    if (temp) {
        return JSON.parse(temp);
    } else {
        const tempCart: Cart[] = new Array<Cart>();
        return tempCart;
    }

  }

  setCarts() {
    localStorage.setItem('ShoppingCart', JSON.stringify(this.carts));
  }

  checkAvailable(temp: Cart): Boolean {
    for (let index = 0; index < this.carts.length; index++) {
        const element = this.carts[index];
        if (element.ID === temp.ID) {
            element.Quantity++;
            return true;
        }
    }
    return false;
  }

  increaseQuantity(id: number, quantity: number) {
    this.carts.forEach(element => {
      if (element.ID === id) {
        element.Quantity = quantity;
      }
    });
    this.setCarts();
    this.updateCartNav();
    this.change.emit(this.cartNav);
  }

  deleteCart(temp: Cart) {
    this.carts = this.carts.filter(item => item.ID !== temp.ID);
    this.setCarts();
    this.updateCartNav();
    this.change.emit(this.cartNav);
  }
  getTotalPrice(): number {
      let sum = 0;
      this.carts.forEach(element => {
          sum += element.Price * element.Quantity;
      });
      return sum;
  }

  updateCartForNav(): CartNav {
    const temp = new CartNav();
    temp.TotalItem = this.carts ? this.carts.length : 0;
    temp.TotalPrice = this.getTotalPrice();
    return temp;
}
  updateCartNav() {
      this.cartNav.TotalItem = this.carts.length;
      this.cartNav.TotalPrice = this.getTotalPrice();
  }

  changeTitle(title: string) {
    this.updateTitle.emit(title);
  }

  hasLogin(): Boolean {
    const temp = localStorage.getItem('UserToken');
    return temp != null;
  }

  checkLogin() {
    this.isLogin.emit(this.hasLogin());
  }

  checkout(address: string): Observable<OrderInput> {
    const params = new OrderInput();
    params.Address = address;
    params.TotalPrice = this.getTotalPrice();
    params.OrderDetailModel = new Array<OrderDetail>();
    this.carts.forEach(element => {
      const temp = new OrderDetail();
      temp.BookID = element.ID;
      temp.BookName = element.BookName;
      temp.Discount = 0;
      temp.ImgUrl = element.BookImg;
      temp.Price = element.Price;
      temp.Quantity = element.Quantity;
      temp.TotalPrice = element.Price * element.Quantity;
      params.OrderDetailModel.push(temp);
    });
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<OrderInput>(url, params);
    return result;
  }
}
