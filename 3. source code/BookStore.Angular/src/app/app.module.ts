import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './modules/app-routing.module';

import { AppComponent } from './app.component';
import { TopNavbarComponent } from './components/shared/top-navbar/top-navbar.component';
import { MidNavbarComponent } from './components/shared/mid-navbar/mid-navbar.component';
import { BottomNavbarComponent } from './components/shared/bottom-navbar/bottom-navbar.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FooterInformationComponent } from './components/shared/footer-information/footer-information.component';
import { FooterSocialComponent } from './components/shared/footer-social/footer-social.component';
import { FooterSellComponent } from './components/shared/footer-sell/footer-sell.component';
import { FooterSignatureComponent } from './components/shared/footer-signature/footer-signature.component';
import { AuthGuard } from './components/ultities/auth.guard';
import { NoAuthGuard } from './components/ultities/no-auth.guard';
import { AuthInterceptor } from './components/ultities/auth.interceptor';

import { NotifierModule } from 'angular-notifier';


@NgModule({
  declarations: [
    AppComponent,
    TopNavbarComponent,
    MidNavbarComponent,
    BottomNavbarComponent,
    FooterInformationComponent,
    FooterSocialComponent,
    FooterSellComponent,
    FooterSignatureComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NotifierModule
  ],
  exports: [
    TopNavbarComponent,
    MidNavbarComponent,
    BottomNavbarComponent,
  ],
  providers: [
    AuthGuard,
    NoAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
