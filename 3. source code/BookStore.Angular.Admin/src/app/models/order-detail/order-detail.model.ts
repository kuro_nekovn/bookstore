export interface IOrderDetail {
    BookName: string;
    BookID: number;
    ImgUrl: string;
    Quantity: number;
    Price: number;
    Discount: number;
    Total: number;
}

export class OrderDetail implements IOrderDetail {
    OrderID: number;
    BookName: string;
    BookID: number;
    ImgUrl: string;
    Quantity: number;
    Price: number;
    TotalPrice: number;
    Discount: number;
    Total: number;
}
