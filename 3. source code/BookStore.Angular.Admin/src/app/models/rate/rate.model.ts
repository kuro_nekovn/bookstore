
export interface IRate {
  ID: number;
  Title: string;
  Rating: number;
}

export class Rate implements IRate {
  ID: number;
  Title: string;
  Rating: number;
  CustomerID: string;
  BookID: string;
}

export class RateInput implements IRate {
  ID: number;
  Title: string;
  Rating: number;
  CustomerID: number;
  BookID: number;
  Content: string;
}
