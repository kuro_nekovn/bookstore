export interface IReceiptDetail {
    BookName: string;
    BookID: number;
    ImgUrl: string;
    Quantity: number;
    Price: number;
    Total: number;
}

export class ReceiptDetail implements IReceiptDetail {
    ReceiptID: number;
    BookName: string;
    BookID: number;
    ImgUrl: string;
    Quantity: number;
    Price: number;
    TotalPrice: number;
    Total: number;
}
