
export interface IComplain {
  ID: number;
  Status: string;
  OrderID: number;
}

export class Complain implements IComplain {
  ID: number;
  Status: string;
  EmployeeID: string;
  CustomerID: string;
  OrderID: number;
}

export class ComplainInput implements IComplain {
  ID: number;
  Status: string;
  EmployeeID: number;
  CustomerID: number;
  Content: string;
  OrderID: number;
}
