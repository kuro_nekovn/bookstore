export interface IRole {
    ID: number;
    Name: string;
    Description: string;
}

export class Role implements IRole {
    ID: number;
    Name: string;
    Description: string;
}
