export interface IItem {
    ID: number;
    Name: string;
}

export class ItemDTO implements IItem {
    ID: number;
    Name: string;
}

