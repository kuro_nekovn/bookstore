export interface ISidebarItem {
    Name: string;
    Router: string;
    Icon: string;
}
export class SidebarItem implements ISidebarItem {
    Name: string;
    Router: string;
    Icon: string;
    HasSubmenu: boolean;
    Submenu: ISidebarItem[];
}
