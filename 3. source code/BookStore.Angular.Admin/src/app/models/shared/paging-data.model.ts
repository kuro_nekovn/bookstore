import { Pagination } from './pager.model';

export class PagingData<T> {
    Page: Pagination;
    Items: T;
}
