export interface IEmployee {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Email: string;
}

export class Employee implements IEmployee {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Email: string;
}

export class EmployeeInput implements IEmployee {
    ID: number;
    LastName: string;
    FirstName: string;
    ImgUrl: string;
    Sex: boolean;
    Address: string;
    Birthday: Date;
    Phone: string;
    Email: string;
    Password: string;
    RoleID: number;
}
