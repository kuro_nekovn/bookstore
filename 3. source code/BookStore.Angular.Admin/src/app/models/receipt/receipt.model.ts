import { ReceiptDetail  } from '../index';

export interface IReceipt {
  TotalPrice: number;
}

export class Receipt implements IReceipt {
  ID: number;
  TotalPrice: number;
  PublisherID: string;
  EmployeeID: string;
  ReceiptDate: Date;
}

export class ReceiptInput implements IReceipt {
  ReceiptID: number;
  TotalPrice: number;
  PublisherID: number;
  EmployeeID: number;
  Description: string;
  ReceiptDetailModel: ReceiptDetail[];
}
