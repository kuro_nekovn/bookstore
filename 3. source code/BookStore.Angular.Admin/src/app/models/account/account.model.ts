export interface IAccountInput {
  ID: number;
  Password: string;
  Email: string;
}

export class AccountInput implements IAccountInput {
  ID: number;
  Email: string;
  Password: string;
}
