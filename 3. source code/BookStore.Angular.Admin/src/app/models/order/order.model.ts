import { OrderDetail  } from '../index';
export interface IOrder {
  TotalPrice: number;
}

export class Order implements IOrder {
  ID: number;
  TotalPrice: number;
  EmployeeID: string;
  CustomerID: string;
  OrderDate: Date;
}

export class OrderInput implements IOrder {
  OrderID: number;
  TotalPrice: number;
  EmployeeID: number;
  CustomerID: number;
  Address: string;
  OrderDetailModel: OrderDetail[];
}
