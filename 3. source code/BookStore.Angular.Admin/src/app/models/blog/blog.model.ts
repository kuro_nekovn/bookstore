
export interface IBlog {
  ID: number;
  Title: string;
  BlogImg: string;
  Sumary: string;
  UserCreateName: string;
}

export class Blog implements IBlog {
  ID: number;
  Title: string;
  BlogImg: string;
  UserCreateName: string;
  Sumary: string;
}

export class BlogInput implements IBlog {
  ID: number;
  Title: string;
  UserCreateName: string;
  BlogImg: string;
  Sumary: string;
  Content: string;
}
