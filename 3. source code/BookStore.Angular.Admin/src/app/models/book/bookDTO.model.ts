import { BookInput } from './book.model';
import { BookImage } from './book-image.model';

export interface IBookDTO {
    BookDetail: BookInput;
    BookImages: BookImage[];
  }

export class BookDTO {
  BookDetail: BookInput;
  BookImages: BookImage[];
}
