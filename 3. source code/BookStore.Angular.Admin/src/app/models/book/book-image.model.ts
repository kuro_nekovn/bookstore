export interface IBookImage {
  ID: number;
  BookID: number;
  BookUrl: string;
}

export class BookImage implements IBookImage {
  ID: number;
  BookID: number;
  BookUrl: string;
}
