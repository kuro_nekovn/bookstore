
export interface IBook {
  ID: number;
  Name: string;
  ImgUrl: string;
}

export class Book implements IBook {
  ID: number;
  Name: string;
  ImgUrl: string;
  Total: number;
  AuthorName: string;
  PublisherName: string;
  Sumary: string;
}

export class BookInput implements IBook {
  ID: number;
  Price: number;
  Total: number;
  Rating: number;
  Status: number;
  Author: number;
  GenreID: number;
  Publisher: number;
  Name: string;
  ImgUrl: string;
  Sumary: string;
  Content: string;
}

export class CreateBookInput implements IBook {
  ID: number;
  Name: string;
  ImgUrl: string;
  Sumary: string;
  Author: number;
  Publisher: number;
  Content: string;
  Price: number;
}

export class OrderBook implements IBook {
  ID: number;
  Name: string;
  ImgUrl: string;
  Price: number;
  Total: number;
  Discount: number;
}
