export interface IPublisher {
  ID: number;
  Name: string;
  Address: string;
  Phone: string;
  ImgUrl: string;
  DateCreate: Date;
  IsActive: boolean;
}

export class Publisher implements IPublisher {
  ID: number;
  Name: string;
  Address: string;
  Phone: string;
  ImgUrl: string;
  DateCreate: Date;
  IsActive: boolean;
}
