
export interface IBlogReview {
  ID: number;
  Title: string;
  BlogName: string;
  Email: string;
}

export class BlogReview implements IBlogReview {
  ID: number;
  Title: string;
  BlogName: string;
  Email: string;
  DateCreate: Date;
}

export class BlogReviewInput implements IBlogReview {
  ID: number;
  Title: string;
  Email: string;
  BlogName: string;
  Content: string;
}
