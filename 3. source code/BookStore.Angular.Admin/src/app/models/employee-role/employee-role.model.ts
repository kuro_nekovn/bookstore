export interface IEmployeeRole {
  ID: number;
  FirstName: string;
  LastName: string;
  ImgUrl: string;
  IsAdmin: boolean;
  IsHr: boolean;
  IsSaleman: boolean;
  IsStockKeeper: boolean;
}

export class EmployeeRole implements IEmployeeRole {
  ID: number;
  FirstName: string;
  LastName: string;
  ImgUrl: string;
  IsAdmin: boolean;
  IsHr: boolean;
  IsSaleman: boolean;
  IsStockKeeper: boolean;
}
