import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule   } from '@angular/forms';

import { AppRoutingModule } from './modules/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { NavigationComponent, SideBarComponent, PageNotFoundComponent } from './components/shared';
import { MainLayoutComponent } from './components/shared/main-layout/main-layout.component';
import { NoneLayoutComponent } from './components/shared/none-layout/none-layout.component';
import { AuthGuard } from './components/user/auth/auth.guard';
import { NoAuthGuard } from './components/user/auth/no-auth.guard';
import { UnauthorizedComponent } from './components/shared/page-error/unauthorized/unauthorized.component';
import { AuthInterceptor } from './components/user/auth/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SideBarComponent,
    MainLayoutComponent,
    NoneLayoutComponent,
    PageNotFoundComponent,
    UnauthorizedComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
    // NgxPaginationModule
  ],
  exports: [
    NavigationComponent,
    SideBarComponent,
    PageNotFoundComponent,
    UnauthorizedComponent
  ],
  providers: [
    AuthGuard,
    NoAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
