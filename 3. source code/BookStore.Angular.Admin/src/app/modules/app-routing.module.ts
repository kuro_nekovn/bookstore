import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthGuard } from './../components/user/auth/auth.guard';


import { QuillModule } from 'ngx-quill';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import {NgxPaginationModule} from 'ngx-pagination';


import { NotificationComponent } from '../components/shared/notification/notification.component';
import { MainLayoutComponent } from '../components/shared/main-layout/main-layout.component';
import { NoneLayoutComponent } from '../components/shared/none-layout/none-layout.component';
import { PageNotFoundComponent } from '../components/shared';

import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { LoginComponent } from '../components/user/login/login.component';

import { PublisherComponent } from '../components/publisher/publisher/publisher.component';
import { EditPublisherComponent } from '../components/publisher/edit-publisher/edit-publisher.component';
import { CreatePublisherComponent } from '../components/publisher/create-publisher/create-publisher.component';

import { CreateAuthorComponent } from '../components/author/create-author/create-author.component';
import { AuthorComponent } from '../components/author/author/author.component';
import { EditAuthorComponent } from '../components/author/edit-author/edit-author.component';

import { CustomerComponent } from '../components/customer/customer/customer.component';
import { CreateCustomerComponent } from '../components/customer/create-customer/create-customer.component';
import { EditCustomerComponent } from '../components/customer/edit-customer/edit-customer.component';

import { EmployeeComponent } from '../components/employee/employee/employee.component';
import { CreateEmployeeComponent } from '../components/employee/create-employee/create-employee.component';
import { EditEmployeeComponent } from '../components/employee/edit-employee/edit-employee.component';

import { BlogComponent } from '../components/blog/blog/blog.component';
import { CreateBlogComponent } from '../components/blog/create-blog/create-blog.component';
import { EditBlogComponent } from '../components/blog/edit-blog/edit-blog.component';

import { BlogReviewComponent } from '../components/blog-review/blog-review/blog-review.component';
import { CreateBlogReviewComponent } from '../components/blog-review/create-blog-review/create-blog-review.component';
import { EditBlogReviewComponent } from '../components/blog-review/edit-blog-review/edit-blog-review.component';

import { AdvertisementComponent } from '../components/advertisement/advertisement/advertisement.component';
import { CreateAdvertisementComponent } from '../components/advertisement/create-advertisement/create-advertisement.component';
import { EditAdvertisementComponent } from '../components/advertisement/edit-advertisement/edit-advertisement.component';

import { GenreComponent } from '../components/genre/genre.component';

import { BookComponent } from '../components/book/book/book.component';
import { CreateBookComponent } from '../components/book/create-book/create-book.component';
import { EditBookComponent } from '../components/book/edit-book/edit-book.component';

import { RateComponent } from '../components/rate/rate/rate.component';
import { CreateRateComponent } from '../components/rate/create-rate/create-rate.component';
import { EditRateComponent } from '../components/rate/edit-rate/edit-rate.component';

import { OrderComponent } from '../components/order/order/order.component';
import { CreateOrderComponent } from '../components/order/create-order/create-order.component';
import { EditOrderComponent } from '../components/order/edit-order/edit-order.component';

import { ReceiptComponent } from '../components/receipt/receipt/receipt.component';
import { CreateReceiptComponent } from '../components/receipt/create-receipt/create-receipt.component';
import { EditReceiptComponent } from '../components/receipt/edit-receipt/edit-receipt.component';

import { ComplainComponent } from '../components/complain/complain/complain.component';
import { CreateComplainComponent } from '../components/complain/create-complain/create-complain.component';
import { EditComplainComponent } from '../components/complain/edit-complain/edit-complain.component';
import { NoAuthGuard } from '../components/user/auth/no-auth.guard';
import { UnauthorizedComponent } from '../components/shared/page-error/unauthorized/unauthorized.component';
import { RoleRouting } from '../models/shared/role-routing.model';
import { EmployeeRoleComponent } from '../components/employee-role/employee-role.component';

const roleRouting: RoleRouting =  new RoleRouting();
const appRoutes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },

            { path: 'publisher', component: PublisherComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.publisher} },
            { path: 'publisher/edit/:id', component: EditPublisherComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.publisher} },
            { path: 'publisher/create', component: CreatePublisherComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.publisher} },

            { path: 'author', component: AuthorComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.author} },
            { path: 'author/create', component: CreateAuthorComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.author} },
            { path: 'author/edit/:id', component: EditAuthorComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.author} },

            { path: 'customer', component: CustomerComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.customer} },
            { path: 'customer/create', component: CreateCustomerComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.customer} },
            { path: 'customer/edit/:id', component: EditCustomerComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.customer} },

            { path: 'employee', component: EmployeeComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.employee} },
            { path: 'employee/create', component: CreateEmployeeComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.employee} },
            { path: 'employee/edit/:id', component: EditEmployeeComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.employee} },

            { path: 'blog', component: BlogComponent, canActivate: [AuthGuard],
                data: {'roles': ['Admin', 'StockKeeper', 'HR', 'Salesman']} },
            { path: 'blog/create', component: CreateBlogComponent, canActivate: [AuthGuard],
                data: {'roles': ['Admin', 'StockKeeper', 'HR', 'Salesman']} },
            { path: 'blog/edit/:id', component: EditBlogComponent, canActivate: [AuthGuard],
                data: {'roles': ['Admin', 'StockKeeper', 'HR', 'Salesman']} },

            { path: 'advertisement', component: AdvertisementComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.advertisement} },
            { path: 'advertisement/create', component: CreateAdvertisementComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.advertisement} },
            { path: 'advertisement/edit/:id', component: EditAdvertisementComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.advertisement} },

            { path: 'blog-review', component: BlogReviewComponent, canActivate: [AuthGuard],
                 data: {'roles': roleRouting.blogReview} },
            { path: 'blog-review/create', component: CreateBlogReviewComponent,
                canActivate: [AuthGuard], data: {'roles': roleRouting.blogReview} },
            { path: 'blog-review/edit/:id', component: EditBlogReviewComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.blogReview} },

            { path: 'genre', component: GenreComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.genre} },

            { path: 'role', component: EmployeeRoleComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.roles} },

            { path: 'book', component: BookComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.book} },
            { path: 'book/create', component: CreateBookComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.book} },
            { path: 'book/edit/:id', component: EditBookComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.book} },

            { path: 'rate', component: RateComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.rate} },
            { path: 'rate/create', component: CreateRateComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.rate} },
            { path: 'rate/edit/:id', component: EditRateComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.rate} },

            { path: 'order', component: OrderComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.order} },
            { path: 'order/create', component: CreateOrderComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.order} },
            { path: 'order/edit/:id', component: EditOrderComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.order} },

            { path: 'receipt', component: ReceiptComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.receipt} },
            { path: 'receipt/create', component: CreateReceiptComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.receipt} },
            { path: 'receipt/edit/:id', component: EditReceiptComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.receipt} },

            { path: 'complain', component: ComplainComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.complain} },
            { path: 'complain/create', component: CreateComplainComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.complain} },
            { path: 'complain/edit/:id', component: EditComplainComponent, canActivate: [AuthGuard],
                data: {'roles': roleRouting.complain} },


        ]
    },
    { path: 'login', component: LoginComponent, canActivate: [NoAuthGuard]},
    { path: 'unauthorized', component: UnauthorizedComponent, canActivate: [AuthGuard]},
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    declarations: [
        NotificationComponent,
        // components
        DashboardComponent,
        LoginComponent,

        PublisherComponent,
        EditPublisherComponent,
        CreatePublisherComponent,

        AuthorComponent,
        CreateAuthorComponent,
        EditAuthorComponent,

        CustomerComponent,
        CreateCustomerComponent,
        EditCustomerComponent,

        EmployeeComponent,
        CreateEmployeeComponent,
        EditEmployeeComponent,

        BlogComponent,
        CreateBlogComponent,
        EditBlogComponent,

        BlogReviewComponent,
        CreateBlogReviewComponent,
        EditBlogReviewComponent,

        AdvertisementComponent,
        CreateAdvertisementComponent,
        EditAdvertisementComponent,

        GenreComponent,
        EmployeeRoleComponent,

        BookComponent,
        CreateBookComponent,
        EditBookComponent,

        RateComponent,
        CreateRateComponent,
        EditRateComponent,

        OrderComponent,
        CreateOrderComponent,
        EditOrderComponent,

        ReceiptComponent,
        CreateReceiptComponent,
        EditReceiptComponent,

        ComplainComponent,
        CreateComplainComponent,
        EditComplainComponent,

    ],
    imports: [
        RouterModule.forRoot(appRoutes,
            { enableTracing: true }
          ),
        RouterModule,
        CommonModule,
        BrowserModule,
        FormsModule,
        QuillModule,
        ReactiveFormsModule,
        HttpClientModule,
        AngularDateTimePickerModule,
        NgxPaginationModule
    ],
    exports: [
        NotificationComponent,

        // components
        DashboardComponent,
        LoginComponent,

        PublisherComponent,
        EditPublisherComponent,
        CreatePublisherComponent,

        AuthorComponent,
        CreateAuthorComponent,
        EditAuthorComponent,

        CustomerComponent,
        CreateCustomerComponent,
        EditCustomerComponent,

        EmployeeComponent,
        CreateEmployeeComponent,
        EditEmployeeComponent,

        BlogComponent,
        CreateBlogComponent,
        EditBlogComponent,

        BlogReviewComponent,
        CreateBlogReviewComponent,
        EditBlogReviewComponent,

        AdvertisementComponent,
        CreateAdvertisementComponent,
        EditAdvertisementComponent,

        GenreComponent,
        EmployeeRoleComponent,

        BookComponent,
        CreateBookComponent,
        EditBookComponent,

        RateComponent,
        CreateRateComponent,
        EditRateComponent,

        OrderComponent,
        CreateOrderComponent,
        EditOrderComponent,

        ReceiptComponent,
        CreateReceiptComponent,
        EditReceiptComponent,

        ComplainComponent,
        CreateComplainComponent,
        EditComplainComponent,
    ],
})

export class AppRoutingModule {
}
