import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { EmployeeRole, DataResult, PagingData, ItemDTO } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class EmployeeRoleService {
  readonly baseUrl: string = 'admin/api/EmployeeRole';

  items: EmployeeRole[];

  constructor(
    private http: HttpClient
  ) {  }

  getEmployeeRoles(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<EmployeeRole[]>>> {
    const url = `${this.baseUrl}/EmployeeRoles?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<EmployeeRole[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  editEmployeeRole(userRole: EmployeeRole): Observable<EmployeeRole> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<EmployeeRole>(url, userRole);
    return result;
  }
}
