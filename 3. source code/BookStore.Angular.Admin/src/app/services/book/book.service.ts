
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Book,  DataResult, PagingData, BookInput, ItemDTO, CreateBookInput, OrderBook} from 'src/app/models';
import { BookDTO } from 'src/app/models/book/bookDTO.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  readonly baseUrl: string = 'admin/api/Book';

  items: Book[];

  constructor(
    private http: HttpClient
  ) {  }

  getBooks(index: number, pagesize: number,
    userName: string, authorName: string, publisherName: string)
    : Observable<DataResult<PagingData<Book[]>>> {
      const url = `${this.baseUrl}/Books?CurrentPage=
      ${index}&pageSize=${pagesize}&bookName=${userName}&authorName=${authorName}&publisherName=${publisherName}`;
      // tslint:disable-next-line:prefer-const
      let result = this.http.get<DataResult<PagingData<Book[]>>>(url).pipe(
        retry(2)
      );
    return result;
  }

  getBook(id: number): Observable<BookDTO> {
    const url = `${this.baseUrl}/getBook/${id}`;

    const result = this.http.get<BookDTO>(url).pipe(
      retry(2)
    );

    return result;
  }

  editBook(book: BookDTO): Observable<BookDTO> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<BookDTO>(url, book);
    return result;
  }

  deleteBook(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

   deleteImage(ID: number): Observable<boolean> {
    const url = `${this.baseUrl}/deleteImage?ID=${ID}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createBook(book: BookDTO): Observable<BookDTO> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<BookDTO>(url, book);
    return result;
  }

  getBookItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }

  GetOrderBook(): Observable<OrderBook[]> {
    const url = `${this.baseUrl}/GetOrderBooks`;
    const result = this.http.get<OrderBook[]>(url);
    return result;
  }
}
