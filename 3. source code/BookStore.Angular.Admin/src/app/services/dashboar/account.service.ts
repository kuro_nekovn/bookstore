import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { DataResult, PagingData, Role, AccountInput } from 'src/app/models';
import { Md5 } from 'ts-md5/dist/md5';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  _jwtHelper = new JwtHelperService();
  readonly baseUrl: string = 'admin/api/account';

  items: Account[];

  constructor(private http: HttpClient) {}

  encriptPassword(password: string): string {
    return Md5.hashStr(password).toString();
  }

  getRoles(): Observable<Role[]> {
    const url = `${this.baseUrl}/GetRoles`;

    const result = this.http.get<Role[]>(url).pipe(retry(2));

    return result;
  }

  getRole(ID: number, roleName: string): Observable<Role> {
    const url = `${this.baseUrl}/GetRole?ID=${ID}&roleName=${roleName}`;

    const result = this.http.get<Role>(url).pipe(retry(2));

    return result;
  }

  changeCustomerPassword(account: AccountInput): Observable<boolean> {
    const url = `${this.baseUrl}/adminchangecustomerpassword`;
    return this.http.post<boolean>(url, account);
  }

  changeEmployeePassword(account: AccountInput): Observable<boolean> {
    const url = `${this.baseUrl}/adminchangeemployeepassword`;
    return this.http.post<boolean>(url, account);
  }

  changePassword(account: AccountInput): Observable<boolean> {
    const url = `${this.baseUrl}/changepassword`;
    return this.http.post<boolean>(url, account);
  }

  login(user: AccountInput): Observable<AccountInput> {
    const headers = new HttpHeaders({'No-Auth': 'True'});
    headers.append('Content-Type', 'application/json');
    const url = `${this.baseUrl}/authenticate`;
    return this.http.post<AccountInput>(url, user,  {headers: headers});
  }

  roleMatch(allowedRoles: string[]): boolean {
    const token = localStorage.getItem('AuthenToken');
    let result = false;
    if (token != null) {
      const userRole = this._jwtHelper.decodeToken(token).role;
      allowedRoles.forEach(element => {
        if (typeof userRole === 'string') {
          if (userRole === element) {
            result = true;
          }
        } else {
          userRole.forEach(x => {
            if (element === x) {
              result = true;
            }
          });
        }
      });
    }
    return result;
  }
}
