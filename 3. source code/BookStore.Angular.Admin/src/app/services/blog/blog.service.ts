
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Blog,  DataResult, PagingData, BlogInput, ItemDTO} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  readonly baseUrl: string = 'admin/api/Blog';

  items: Blog[];

  constructor(
    private http: HttpClient
  ) {  }

  getBlogs(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Blog[]>>> {
    const url = `${this.baseUrl}/Blogs?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Blog[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getBlog(id: number): Observable<BlogInput> {
    const url = `${this.baseUrl}/getBlog/${id}`;

    const result = this.http.get<BlogInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editBlog(blog: BlogInput): Observable<BlogInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<BlogInput>(url, blog);
    return result;
  }

  deleteBlog(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createBlog(blog: BlogInput): Observable<BlogInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<BlogInput>(url, blog);
    return result;
  }

  getBlogItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
