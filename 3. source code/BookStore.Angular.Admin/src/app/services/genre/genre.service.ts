import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { Genre, DataResult, PagingData, ItemDTO } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class GenreService {
  readonly baseUrl: string = 'admin/api/genre';

  items: Genre[];

  constructor(
    private http: HttpClient
  ) {  }

  getGenres(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Genre[]>>> {
    const url = `${this.baseUrl}/genres?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Genre[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getGenre(id: number): Observable<Genre> {
    const url = `${this.baseUrl}/getGenre/${id}`;

    const result = this.http.get<Genre>(url).pipe(
      retry(2)
    );

    return result;
  }
  getGenreItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }

  editGenre(genre: Genre): Observable<Genre> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<Genre>(url, genre);
    return result;
  }

  deleteGenre(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createGenre(genre: Genre): Observable<Genre> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<Genre>(url, genre);
    return result;
  }
}
