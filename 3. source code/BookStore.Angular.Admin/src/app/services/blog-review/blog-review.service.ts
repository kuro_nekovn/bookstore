
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BlogReview,  DataResult, PagingData, BlogReviewInput} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class BlogReviewService {
  readonly baseUrl: string = 'admin/api/BlogReview';

  items: BlogReview[];

  constructor(
    private http: HttpClient
  ) {  }

  getBlogReviews(index: number, pagesize: number, keyword: string, blogID: number): Observable<DataResult<PagingData<BlogReview[]>>> {
    const url = `${this.baseUrl}/BlogReviews?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}&blogID=${blogID}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<BlogReview[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getBlogReview(id: number): Observable<BlogReviewInput> {
    const url = `${this.baseUrl}/getBlogReview/${id}`;

    const result = this.http.get<BlogReviewInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editBlogReview(blogReview: BlogReviewInput): Observable<BlogReviewInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<BlogReviewInput>(url, blogReview);
    return result;
  }

  deleteBlogReview(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createBlogReview(blogReview: BlogReviewInput): Observable<BlogReviewInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<BlogReviewInput>(url, blogReview);
    return result;
  }
}
