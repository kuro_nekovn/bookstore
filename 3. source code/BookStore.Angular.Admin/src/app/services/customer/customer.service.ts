import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CustomerInput, Customer, DataResult, PagingData, ItemDTO } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  readonly baseUrl: string = 'admin/api/customer';

  items: Customer[];

  constructor(
    private http: HttpClient
  ) {  }

  getCustomers(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Customer[]>>> {
    const url = `${this.baseUrl}/Customers?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    const result = this.http.get<DataResult<PagingData<Customer[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getCustomer(id: number): Observable<CustomerInput> {
    const url = `${this.baseUrl}/getCustomer/${id}`;

    const result = this.http.get<CustomerInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editCustomer(customer: CustomerInput): Observable<CustomerInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<CustomerInput>(url, customer);
    return result;
  }

  deleteCustomer(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createCustomer(customer: CustomerInput): Observable<CustomerInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<CustomerInput>(url, customer);
    return result;
  }

  getCustomerItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
