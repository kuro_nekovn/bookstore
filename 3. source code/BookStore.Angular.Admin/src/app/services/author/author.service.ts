import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Author,  DataResult, PagingData, AuthorInput, ItemDTO} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  readonly baseUrl: string = 'admin/api/Author';

  items: Author[];

  constructor(
    private http: HttpClient
  ) {  }

  getAuthors(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Author[]>>> {
    const url = `${this.baseUrl}/Authors?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Author[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getAuthor(id: number): Observable<AuthorInput> {
    const url = `${this.baseUrl}/getAuthor/${id}`;

    const result = this.http.get<AuthorInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  getAuthorItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }

  editAuthor(author: AuthorInput): Observable<AuthorInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<AuthorInput>(url, author);
    return result;
  }

  deleteAuthor(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createAuthor(author: AuthorInput): Observable<AuthorInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<AuthorInput>(url, author);
    return result;
  }
}
