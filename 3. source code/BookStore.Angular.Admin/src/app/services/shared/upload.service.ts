import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataResult, UploadModel } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    private http: HttpClient
  ) { }

  public uploadImage(file: File, type: string): Observable<string> {
    const uploadData = new FormData();

    uploadData.append('file', file);
    return this.http.post<string>('admin/API/Upload/Image', uploadData);
  }
}
