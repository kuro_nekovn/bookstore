
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Receipt,  DataResult, PagingData, ReceiptInput} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class ReceiptService {
  readonly baseUrl: string = 'admin/api/receipt';

  items: Receipt[];

  constructor(
    private http: HttpClient
  ) {  }

  getReceipts(index: number, pagesize: number,
    employeeName: string, publisherName: string): Observable<DataResult<PagingData<Receipt[]>>> {
    const url = `${this.baseUrl}/Receipts?CurrentPage=${index}
      &pageSize=${pagesize}&employeeName=${employeeName}&publisherName=${publisherName}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Receipt[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getReceipt(id: number): Observable<ReceiptInput> {
    const url = `${this.baseUrl}/getReceipt/${id}`;

    const result = this.http.get<ReceiptInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editReceipt(rate: ReceiptInput): Observable<ReceiptInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<ReceiptInput>(url, rate);
    return result;
  }

  deleteReceipt(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  deleteDetail(receiptID: number, bookID: number): Observable<boolean> {
    const url = `${this.baseUrl}/deletedetail?ReceiptID=${receiptID}&BookID=${bookID}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createReceipt(rate: ReceiptInput): Observable<ReceiptInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<ReceiptInput>(url, rate);
    return result;
  }
}
