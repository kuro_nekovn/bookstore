
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Complain,  DataResult, PagingData, ComplainInput, ItemDTO} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class ComplainService {
  readonly baseUrl: string = 'admin/api/Complain';

  items: Complain[];

  constructor(
    private http: HttpClient
  ) {  }

  getComplains(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Complain[]>>> {
    const url = `${this.baseUrl}/Complains?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Complain[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getComplain(id: number): Observable<ComplainInput> {
    const url = `${this.baseUrl}/getComplain/${id}`;

    const result = this.http.get<ComplainInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editComplain(complain: ComplainInput): Observable<ComplainInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<ComplainInput>(url, complain);
    return result;
  }

  deleteComplain(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createComplain(complain: ComplainInput): Observable<ComplainInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<ComplainInput>(url, complain);
    return result;
  }

  getComplainItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
