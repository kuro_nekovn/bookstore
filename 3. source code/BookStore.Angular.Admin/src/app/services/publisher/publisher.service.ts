import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Publisher, DataResult, PagingData, ItemDTO } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class PublisherService {
  readonly baseUrl: string = 'admin/api/publisher';

  items: Publisher[];

  constructor(
    private http: HttpClient
  ) {  }

  getPublishers(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Publisher[]>>> {
    const url = `${this.baseUrl}/Publishers?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Publisher[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getPublisher(id: number): Observable<Publisher> {
    const url = `${this.baseUrl}/getPublisher/${id}`;

    const result = this.http.get<Publisher>(url).pipe(
      retry(2)
    );

    return result;
  }

  getPublisherItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }

  editPublisher(publisher: Publisher): Observable<Publisher> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<Publisher>(url, publisher);
    return result;
  }

  deletePublisher(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createPublisher(publisher: Publisher): Observable<Publisher> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<Publisher>(url, publisher);
    return result;
  }
}
