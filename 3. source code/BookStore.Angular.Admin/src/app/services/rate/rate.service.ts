
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Rate,  DataResult, PagingData, RateInput} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class RateService {
  readonly baseUrl: string = 'admin/api/Rate';

  items: Rate[];

  constructor(
    private http: HttpClient
  ) {  }

  getRates(index: number, pagesize: number, keyword: string,
      bookName: string, customerName: string): Observable<DataResult<PagingData<Rate[]>>> {
    const url = `${this.baseUrl}/Rates?CurrentPage=${index}
      &pageSize=${pagesize}&keyword=${keyword}&bookID=${bookName}&customerID=${customerName}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Rate[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getRate(id: number): Observable<RateInput> {
    const url = `${this.baseUrl}/getRate/${id}`;

    const result = this.http.get<RateInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editRate(rate: RateInput): Observable<RateInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<RateInput>(url, rate);
    return result;
  }

  deleteRate(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createRate(rate: RateInput): Observable<RateInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<RateInput>(url, rate);
    return result;
  }
}
