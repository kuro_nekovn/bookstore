import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SidebarItem } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  serviceUrl = 'api/sidebar';

  items: SidebarItem[];

  constructor(
    private http: HttpClient
  ) {
    this.items = [
      {
        Name : 'Menu',
        Router : '/menu',
        Icon : '/assets/images/core/menus/menu.svg',
        HasSubmenu: false,
        Submenu: null
      }
    ];
  }
}
