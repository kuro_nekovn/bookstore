import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { EmployeeInput, Employee, DataResult, PagingData, ItemDTO} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  readonly baseUrl: string = 'admin/api/employee';

  items: Employee[];

  constructor(
    private http: HttpClient
  ) {  }

  getEmployees(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Employee[]>>> {
    const url = `${this.baseUrl}/Employees?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    const result = this.http.get<DataResult<PagingData<Employee[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getEmployee(id: number): Observable<EmployeeInput> {
    const url = `${this.baseUrl}/getEmployee/${id}`;

    const result = this.http.get<EmployeeInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  getEmployeeProfile(): Observable<EmployeeInput> {
    const url = `admin/api/account/getEmployeeProfile`;

    const result = this.http.get<EmployeeInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editEmployee(employee: EmployeeInput): Observable<EmployeeInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<EmployeeInput>(url, employee);
    return result;
  }

  deleteEmployee(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createEmployee(employee: EmployeeInput): Observable<EmployeeInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<EmployeeInput>(url, employee);
    return result;
  }

  getEmployeeItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
