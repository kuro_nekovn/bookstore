
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Order,  DataResult, PagingData, OrderInput, ItemDTO} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  readonly baseUrl: string = 'admin/api/order';

  items: Order[];

  constructor(
    private http: HttpClient
  ) {  }

  getOrders(index: number, pagesize: number,
    employeeName: string, customerName: string): Observable<DataResult<PagingData<Order[]>>> {
    const url = `${this.baseUrl}/Orders?CurrentPage=${index}
      &pageSize=${pagesize}&employeeID=${employeeName}&customerID=${customerName}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Order[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getOrder(id: number): Observable<OrderInput> {
    const url = `${this.baseUrl}/getOrder/${id}`;

    const result = this.http.get<OrderInput>(url).pipe(
      retry(2)
    );

    return result;
  }

  editOrder(rate: OrderInput): Observable<OrderInput> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<OrderInput>(url, rate);
    return result;
  }

  deleteOrder(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  deleteDetail(orderID: number, bookID: number): Observable<boolean> {
    const url = `${this.baseUrl}/deletedetail?OrderID=${orderID}&BookID=${bookID}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createOrder(rate: OrderInput): Observable<OrderInput> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<OrderInput>(url, rate);
    return result;
  }
  getOrderItem(): Observable<ItemDTO[]> {
    const url = `${this.baseUrl}/GetItems`;
    const result = this.http.get<ItemDTO[]>(url);
    return result;
  }
}
