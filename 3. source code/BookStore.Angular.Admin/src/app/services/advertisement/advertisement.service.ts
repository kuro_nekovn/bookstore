import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { Advertisement, DataResult, PagingData } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class AdvertisementService {
  readonly baseUrl: string = 'admin/api/advertisement';

  items: Advertisement[];

  constructor(
    private http: HttpClient
  ) {  }

  getAdvertisements(index: number, pagesize: number, keyword: string): Observable<DataResult<PagingData<Advertisement[]>>> {
    const url = `${this.baseUrl}/advertisements?CurrentPage=${index}&pageSize=${pagesize}&keyword=${keyword}`;
    // tslint:disable-next-line:prefer-const
    let result = this.http.get<DataResult<PagingData<Advertisement[]>>>(url).pipe(
        retry(2)
    );
    return result;
  }

  getAdvertisement(id: number): Observable<Advertisement> {
    const url = `${this.baseUrl}/getAdvertisement/${id}`;

    const result = this.http.get<Advertisement>(url).pipe(
      retry(2)
    );

    return result;
  }

  editAdvertisement(advertisement: Advertisement): Observable<Advertisement> {
    const url = `${this.baseUrl}/edit`;
    const result = this.http.post<Advertisement>(url, advertisement);
    return result;
  }

  deleteAdvertisement(id: number): Observable<boolean> {
    const url = `${this.baseUrl}/delete/${id}`;
    const result = this.http.get<boolean>(url);
    return result;
  }

  createAdvertisement(advertisement: Advertisement): Observable<Advertisement> {
    const url = `${this.baseUrl}/create`;
    const result = this.http.post<Advertisement>(url, advertisement);
    return result;
  }
}
