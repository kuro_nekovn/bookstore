import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Book, BookInput, ItemDTO, CreateBookInput, BookDTO } from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { BookService } from 'src/app/services/book/book.service';
import { AuthorService } from 'src/app/services/author/author.service';
import { PublisherService } from 'src/app/services/publisher/publisher.service';
import { GenreService } from 'src/app/services/genre/genre.service';


@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  authors: ItemDTO[];
  publishers: ItemDTO[];
  genres: ItemDTO[];
  thumbnailFile: File;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private bookService: BookService,
    private authorService: AuthorService,
    private publisherService: PublisherService,
    private genreService: GenreService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Name': ['', Validators.required  ],
      'ImgUrl': ['', Validators.required  ],
      'Author': ['', Validators.required ],
      'Publisher': ['', Validators.required  ],
      'Sumary': ['',  Validators.compose([Validators.required, Validators.maxLength(300)])  ],
      'Content': ['', Validators.required ],
      'Price': ['', Validators.required ],
      'Genre': ['', Validators.required ],
      'BookImg': this.formBuilder.array([
        this.formBuilder.group({
        'ID': -1,
        'BookID': -1,
        'BookUrl': [ '', Validators.required]
       })])
    });
    this.getPublisherItem();
    this.getAuthorItem();
    this.getGenreItem();
  }

  get Name() { return this.form.get('Name'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Author() { return this.form.get('Author'); }
  get Publisher() { return this.form.get('Publisher'); }
  get Sumary() { return this.form.get('Sumary'); }
  get Content() { return this.form.get('Content'); }
  get Genre() { return this.form.get('Genre'); }
  get Price() { return this.form.get('Price'); }
  get BookImg() { return <FormArray>this.form.get('BookImg'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'author').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  uploadBookImage(event, formValue) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'author').subscribe(
      (data: string) => {
        formValue.patchValue({
          'BookUrl': data
       });

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  getAuthorItem() {
    this.notificationService.showLoading();
    this.authorService.getAuthorItem().subscribe(
      (data: ItemDTO[]) => {
        this.authors = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getGenreItem() {
    this.notificationService.showLoading();
    this.genreService.getGenreItem().subscribe(
      (data: ItemDTO[]) => {
        this.genres = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getPublisherItem() {
    this.notificationService.showLoading();
    this.publisherService.getPublisherItem().subscribe(
      (data: ItemDTO[]) => {
        this.publishers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/book`);
    });
    if (this.form.valid) {
      // tslint:disable-next-line:prefer-const
      let result: BookDTO = new BookDTO();
      result.BookDetail = new BookInput();
      result.BookDetail.Name = formValue.Name;
      result.BookDetail.ImgUrl = formValue.ImgUrl;
      result.BookDetail.Author = formValue.Author;
      result.BookDetail.Publisher = formValue.Publisher;
      result.BookDetail.Sumary = formValue.Sumary;
      result.BookDetail.Content = formValue.Content;
      result.BookImages = formValue.BookImg;
      result.BookDetail.Price = formValue.Price;
      result.BookDetail.GenreID = formValue.Genre;
      this.bookService.createBook(result).subscribe(
        (data: BookDTO) => {
          this.notificationService.success('Insert ' + result.BookDetail.Name + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '<br />' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }

  deleteBookImage(index) {
    this.BookImg.removeAt(index);
  }

  addBookImage() {
     const control = <FormArray>this.BookImg;
     control.push(
      this.formBuilder.group({
       'ID': -1,
       'BookID': -1,
       'BookUrl': [ '', Validators.required]
      })
    );
   }
}
