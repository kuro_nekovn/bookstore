
import { Component, OnInit } from '@angular/core';
import { Book, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { BookService } from 'src/app/services/book/book.service';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  page: Pagination = new Pagination();
  bookName: string;
  authorName: string;
  publisherName: string;
  books: Book[];
  constructor(
    private notificationService: NotificationService,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.bookService.getBooks(index, this.page.PageSize,
            this.bookName, this.authorName, this.publisherName)
          .subscribe(
            (data: DataResult < PagingData < Book[] >> ) => {
              this.books = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.bookService.deleteBook(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this book!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
