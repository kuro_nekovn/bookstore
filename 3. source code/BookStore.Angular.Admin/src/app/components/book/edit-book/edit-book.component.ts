import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { BookInput, ItemDTO, CreateBookInput, BookDTO } from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { BookService } from 'src/app/services/book/book.service';
import { AuthorService } from 'src/app/services/author/author.service';
import { PublisherService } from 'src/app/services/publisher/publisher.service';
import { GenreService } from 'src/app/services/genre/genre.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  authors: ItemDTO[];
  publishers: ItemDTO[];
  thumbnailFile: File;
  form: FormGroup;
  book: BookDTO;
  isReady = false;
  genres: ItemDTO[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private bookService: BookService,
    private authorService: AuthorService,
    private publisherService: PublisherService,
    private genreService: GenreService,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Name': ['', Validators.required  ],
      'ImgUrl': ['', Validators.required  ],
      'Author': ['', Validators.required ],
      'Publisher': ['', Validators.required  ],
      'Sumary': ['',  Validators.compose([Validators.required, Validators.maxLength(300)])   ],
      'Content': ['', Validators.required ],
      'Price': ['', Validators.required ],
      'Total': '',
      'Rating': '',
      'Genre': ['', Validators.required ],
      'BookImg': this.formBuilder.array([
       ])
    });
    this.getPublisherItem();
    this.getAuthorItem();
    this.getGenreItem();
    this.getBook();
  }

  get Name() { return this.form.get('Name'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Author() { return this.form.get('Author'); }
  get Publisher() { return this.form.get('Publisher'); }
  get Sumary() { return this.form.get('Sumary'); }
  get Content() { return this.form.get('Content'); }
  get Price() { return this.form.get('Price'); }
  get Total() { return this.form.get('Total'); }
  get Rating() { return this.form.get('Rating'); }
  get Genre() { return this.form.get('Genre'); }
  get BookImg() { return <FormArray>this.form.get('BookImg'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'author').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  uploadBookImage(event, formValue) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'author').subscribe(
      (data: string) => {
        formValue.patchValue({
          'BookUrl': data
       });

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  getBook() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.bookService.getBook(id).subscribe(
      (data: BookDTO) => {
        this.book = data;
        this.form.patchValue({
          Name: data.BookDetail.Name,
          ImgUrl: data.BookDetail.ImgUrl,
          Content: data.BookDetail.Content,
          Author: data.BookDetail.Author,
          Publisher: data.BookDetail.Publisher,
          Total: data.BookDetail.Total,
          Price: data.BookDetail.Price,
          Rating: data.BookDetail.Rating,
          Sumary: data.BookDetail.Sumary,
          Genre: data.BookDetail.GenreID,
        });
        const control = <FormArray>this.BookImg;
        if (data.BookImages) {
          data.BookImages.forEach( x => {
            control.push(
              this.formBuilder.group({
                'ID': x.ID,
                'BookID': x.BookID,
                'BookUrl': [ x.BookUrl, Validators.required]
              })
            );
          });
        } else {
          control.push(
          this.formBuilder.group({
            'ID': -1,
            'BookID': -1,
            'BookUrl': [ '', Validators.required]
           }));
        }
        this.form.get('Rating').disable();
        this.form.get('Total').disable();
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  getGenreItem() {
    this.notificationService.showLoading();
    this.genreService.getGenreItem().subscribe(
      (data: ItemDTO[]) => {
        this.genres = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getAuthorItem() {
    this.notificationService.showLoading();
    this.authorService.getAuthorItem().subscribe(
      (data: ItemDTO[]) => {
        this.authors = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getPublisherItem() {
    this.notificationService.showLoading();
    this.publisherService.getPublisherItem().subscribe(
      (data: ItemDTO[]) => {
        this.publishers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();
    if (this.form.valid) {
      const result: BookDTO = new BookDTO();
      result.BookDetail = new BookInput();
      result.BookDetail.Name = formValue.Name;
      result.BookDetail.ImgUrl = formValue.ImgUrl;
      result.BookDetail.Author = formValue.Author;
      result.BookDetail.Publisher = formValue.Publisher;
      result.BookDetail.Sumary = formValue.Sumary;
      result.BookDetail.Content = formValue.Content;
      result.BookDetail.Price = formValue.Price;
      result.BookDetail.ID = this.book.BookDetail.ID;
      result.BookDetail.GenreID = formValue.Genre;
      result.BookImages = formValue.BookImg;
      this.bookService.editBook(result).subscribe(
        (data: BookDTO) => {
          this.notificationService.success('Insert ' + data.BookDetail.Name + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }

  deleteBookImage(bookImg, index) {
    if (bookImg.value.ID < 1) {
      this.BookImg.removeAt(index);
      return;
    }
    this.notificationService.showLoading();
    this.bookService.deleteImage(bookImg.value.ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.BookImg.removeAt(index);
          this.notificationService.success();
        } else {
          this.notificationService.error(`Delete image failed`);
        }
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      }
    );
  }

  addBookImage() {
     const control = <FormArray>this.BookImg;
     control.push(
      this.formBuilder.group({
       'ID': -1,
       'BookID': this.route.snapshot.params['id'],
       'BookUrl': [ '', Validators.required]
      })
    );
   }
}
