import { Component, OnInit } from '@angular/core';
import { Order, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  page: Pagination = new Pagination();
  customerName: string;
  employeeName: string;
  keyword: string;
  orders: Order[];
  constructor(
    private notificationService: NotificationService,
    private orderService: OrderService,
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.orderService.getOrders(index, this.page.PageSize, this.employeeName, this.customerName)
          .subscribe(
            (data: DataResult < PagingData < Order[] >> ) => {
              this.orders = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.orderService.deleteOrder(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this order!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
