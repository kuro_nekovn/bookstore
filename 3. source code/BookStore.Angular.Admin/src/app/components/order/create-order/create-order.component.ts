import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Order, OrderInput, ItemDTO, OrderBook, OrderDetail } from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { OrderService } from 'src/app/services/order/order.service';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { BookService } from 'src/app/services/book/book.service';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit {
  order: OrderInput;
  employees: ItemDTO[];
  customers: ItemDTO[];
  orderBooks: OrderBook[];
  thumbnailFile: File;
  form: FormGroup;
  isDetailReady = false;
  orderDetails: OrderDetail[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private orderService: OrderService,
    private employeeService: EmployeeService,
    private customerService: CustomerService,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'TotalPrice': [ 0, Validators.required  ],
      'EmployeeID': ['', Validators.required  ],
      'CustomerID': ['', Validators.required ],
      'Address': ['', Validators.required ],
      'SelectBook': ['', Validators.required ],
      'OrderBookDetail': this.formBuilder.array([])
    });
    this.getEmployeeItem();
    this.getCustomerItem();
    this.getbookOrder();
    // tslint:disable-next-line:
    this.form.controls['OrderBookDetail'].valueChanges.subscribe(
      value => {
          let sum = 0;
          value.forEach(element => {
            sum += element.TotalPrice;
          });
          this.form.controls['TotalPrice'].setValue(sum);
        });
    }

  get TotalPrice() { return this.form.get('TotalPrice'); }
  get CustomerID() { return this.form.get('CustomerID'); }
  get EmployeeID() { return this.form.get('EmployeeID'); }
  get Address() { return this.form.get('Address'); }
  get SelectBook() { return this.form.get('SelectBook'); }
  get OrderBookDetail() { return <FormArray>this.form.get('OrderBookDetail'); }


  getEmployeeItem() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeItem().subscribe(
      (data: ItemDTO[]) => {
        this.employees = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getbookOrder() {
    this.notificationService.showLoading();
    this.bookService.GetOrderBook().subscribe(
      (data: OrderBook[]) => {
        this.orderBooks = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }


  getCustomerItem() {
    this.notificationService.showLoading();
    this.customerService.getCustomerItem().subscribe(
      (data: ItemDTO[]) => {
        this.customers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();
    if (this.form.valid) {
      const result: OrderInput = new OrderInput();
      result.TotalPrice = formValue.TotalPrice;
      result.CustomerID = formValue.CustomerID;
      result.EmployeeID = formValue.EmployeeID;
      result.Address = formValue.Address;
      result.OrderDetailModel = formValue.OrderBookDetail;
      this.orderService.createOrder(result).subscribe(
        (data: OrderInput) => {
          this.notificationService.success('Create Order successfully');
          this.router.navigateByUrl(`/order/edit/${data.OrderID}`);
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }

  addOderDetail() {
   if (this.SelectBook.valid) {
    const control = <FormArray>this.OrderBookDetail;
    const isSame = false;
    // tslint:disable-next-line:prefer-const
    for (let ctrl of control.controls) {
      // tslint:disable-next-line:triple-equals
      if (ctrl.value.BookID == this.SelectBook.value.ID) {
        this.notificationService.showLoading();
        this.notificationService.error('Cant add same book!');
        return;
      }
    }
    const total = this.SelectBook.value.Total;
    const totalPrice = this.SelectBook.value.Price * (100 - this.SelectBook.value.Discount) / 100;
    control.push(
      this.formBuilder.group({
       'BookName': this.SelectBook.value.Name,
       'BookID': this.SelectBook.value.ID,
       'ImgUrl': this.SelectBook.value.ImgUrl,
       'Price': this.SelectBook.value.Price,
       'Total': total,
       'Discount': [ this.SelectBook.value.Discount,  Validators.compose([Validators.min(0), Validators.max(100)])],
       'Quantity': [ 1, Validators.compose([Validators.min(0), Validators.max(total)])],
       'TotalPrice': [totalPrice, Validators.min(0)]
      })
    );
    this.isDetailReady = true;
   } else {
    this.notificationService.showLoading();
    this.notificationService.error('Invalid Book. please choose book for order');
   }
  }

  deleteOderDetail(index) {
    this.OrderBookDetail.removeAt(index);
  }

  updateTotalPrice(formValue) {
    const Price = formValue.value.Price;
    const Quantity = formValue.value.Quantity;
    const Discount = formValue.value.Discount;
    const TotalPrice = Quantity * Price - (Quantity * Price * Discount) / 100;
    formValue.patchValue({
       'TotalPrice': TotalPrice
    });
  }
}
