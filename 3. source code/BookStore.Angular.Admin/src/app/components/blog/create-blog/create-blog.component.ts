import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Blog, BlogInput} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { BlogService } from 'src/app/services/blog/blog.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  blog: Blog;
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'Sumary': ['', Validators.required  ],
      'ImgUrl': '',
      'Content': ['', Validators.required  ]
    });
  }

  get Title() { return this.form.get('Title'); }
  get Sumary() { return this.form.get('Sumary'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Content() { return this.form.get('Content'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'blog').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/blog`);
    });

    if (this.form.valid) {
      const result: BlogInput = new BlogInput();
      result.Title = formValue.Title;
      result.Content = formValue.Content;
      result.Sumary = formValue.Sumary;
      result.BlogImg = formValue.ImgUrl;
      this.blogService.createBlog(result).subscribe(
        (data: BlogInput) => {
          this.notificationService.success('Insert ' + data.Title + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
