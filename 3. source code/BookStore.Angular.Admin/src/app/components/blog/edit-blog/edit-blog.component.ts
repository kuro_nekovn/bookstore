import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Blog, BlogInput} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { BlogService } from 'src/app/services/blog/blog.service';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  blog: BlogInput;
  thumbnailFile: File;
  form: FormGroup;
  isReady = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Sumary': ['', Validators.required  ],
      'ImgUrl': '',
      'Content': ['', Validators.required  ],
      'Title': [ '', Validators.required  ],
      'Creator': ''
    });
    this.getBlog();
  }

  get Title() { return this.form.get('Title'); }
  get Sumary() { return this.form.get('Sumary'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Content() { return this.form.get('Content'); }
  get Creator() { return this.form.get('Creator'); }


  getBlog() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.blogService.getBlog(id).subscribe(
      (data: BlogInput) => {
        this.blog = data;
        this.form.setValue({
          Title: data.Title,
          ImgUrl: data.BlogImg,
          Content: data.Content,
          Sumary: data.Sumary,
          Creator: data.UserCreateName
        });
        this.form.get('Creator').disable();
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'blog').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new BlogInput();
      result.Title = formValue.Title;
      result.Content = formValue.Content;
      result.Sumary = formValue.Sumary;
      result.BlogImg = formValue.ImgUrl;
      result.ID = this.blog.ID;
      this.blogService.editBlog(result).subscribe(
        (data: Blog) => {
          this.notificationService.success('Edited blog ' + result.Title + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
