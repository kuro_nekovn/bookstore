import { Component, OnInit } from '@angular/core';
import { Blog, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { BlogService } from 'src/app/services/blog/blog.service';


@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  blogs: Blog[];
  constructor(
    private notificationService: NotificationService,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.blogService.getBlogs(index, this.page.PageSize, this.searchInput)
          .subscribe(
            (data: DataResult < PagingData < Blog[] >> ) => {
              this.blogs = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.blogService.deleteBlog(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this blog!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
