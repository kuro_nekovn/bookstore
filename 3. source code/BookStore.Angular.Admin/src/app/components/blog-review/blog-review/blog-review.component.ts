import { Component, OnInit } from '@angular/core';
import { BlogReview, PagingData, DataResult, Pagination, ItemDTO } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { BlogReviewService } from 'src/app/services/blog-review/blog-review.service';
import { BlogService } from 'src/app/services/blog/blog.service';


@Component({
  selector: 'app-blog-review',
  templateUrl: './blog-review.component.html',
  styleUrls: ['./blog-review.component.css']
})
export class BlogReviewComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  blogID = 0;
  blogs: ItemDTO[];
  blogReviews: BlogReview[];
  constructor(
    private notificationService: NotificationService,
    private blogReviewService: BlogReviewService,
    private blogService: BlogService,
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.getBlogItem();
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.blogReviewService.getBlogReviews(index, this.page.PageSize, this.searchInput, this.blogID)
          .subscribe(
            (data: DataResult < PagingData < BlogReview[] >> ) => {
              this.blogReviews = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

    getBlogItem() {
      this.notificationService.showLoading();
      this.blogService.getBlogItem().subscribe(
        (data: ItemDTO[]) => {
          this.blogs = data;
          this.notificationService.success();
        },
        error => {
          let message: string;
            if (error.error.Message) {
              message = error.message + '\n' + error.error.Message;
            } else { message = error.message; }
            this.notificationService.error(message);
        }
      );
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.blogReviewService.deleteBlogReview(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this blogReview!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
