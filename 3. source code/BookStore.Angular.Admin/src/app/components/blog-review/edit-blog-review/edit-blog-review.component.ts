import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { BlogReviewInput} from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { BlogReviewService } from 'src/app/services/blog-review/blog-review.service';

@Component({
  selector: 'app-edit-blog-review',
  templateUrl: './edit-blog-review.component.html',
  styleUrls: ['./edit-blog-review.component.css']
})
export class EditBlogReviewComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  blog: BlogReviewInput;
  thumbnailFile: File;
  form: FormGroup;
  isReady = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private blogService: BlogReviewService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'BlogName': ['', Validators.required  ],
      'Email': ['', Validators.compose([Validators.required, Validators.email]) ],
      'Content': [ '', Validators.required  ]
    });
    this.getBlog();
  }

  get Title() { return this.form.get('Title'); }
  get BlogName() { return this.form.get('BlogName'); }
  get Email() { return this.form.get('Email'); }
  get Content() { return this.form.get('Content'); }


  getBlog() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.blogService.getBlogReview(id).subscribe(
      (data: BlogReviewInput) => {
        this.blog = data;
        this.form.setValue({
          Title: data.Title,
          Email: data.Email,
          Content: data.Content,
          BlogName: data.BlogName
        });
        this.form.get('BlogName').disable();
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }



  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new BlogReviewInput();
      result.Title = formValue.Title;
      result.Content = formValue.Content;
      result.BlogName = formValue.BlogName;
      result.ID = this.blog.ID;
      this.blogService.editBlogReview(result).subscribe(
        (data: BlogReviewInput) => {
          this.notificationService.success('Edited blog ' + result.Title + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
