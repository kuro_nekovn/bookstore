import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { BlogReview, BlogReviewInput, ItemDTO } from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { BlogReviewService } from 'src/app/services/blog-review/blog-review.service';
import { BlogService } from 'src/app/services/blog/blog.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-create-blog-review',
  templateUrl: './create-blog-review.component.html',
  styleUrls: ['./create-blog-review.component.css']
})
export class CreateBlogReviewComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  blogReview: BlogReview;
  blogs: ItemDTO[];
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private blogReviewService: BlogReviewService,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'BlogName': ['', Validators.required  ],
      'Email': ['', Validators.compose([Validators.required, Validators.email]) ],
      'Content': ['', Validators.required  ]
    });
    this. getBlogItem();
  }

  get Title() { return this.form.get('Title'); }
  get BlogName() { return this.form.get('BlogName'); }
  get Email() { return this.form.get('Email'); }
  get Content() { return this.form.get('Content'); }


  getBlogItem() {
    this.notificationService.showLoading();
    this.blogService.getBlogItem().subscribe(
      (data: ItemDTO[]) => {
        this.blogs = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/blog-review`);
    });
    if (this.form.valid) {
      const result: BlogReviewInput = new BlogReviewInput();
      result.Title = formValue.Title;
      result.BlogName = formValue.BlogName;
      alert(result.BlogName);
      result.Email = formValue.Email;
      result.Content = formValue.Content;
      this.blogReviewService.createBlogReview(result).subscribe(
        (data: BlogReviewInput) => {
          this.notificationService.success('Insert ' + data.Title + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
