import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AccountService } from 'src/app/services/account/account.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  _helper = new JwtHelperService();
  constructor(
    private _http: Router,
    private _accountService: AccountService
  ) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('AuthenToken')) {
      if (!this._helper.isTokenExpired(localStorage.getItem('AuthenToken'))) {
        const allowRoles = next.data['roles'] as Array<string>;
        if (!allowRoles) {
          return true;
        }
        const result = this._accountService.roleMatch(allowRoles);
        if (!result) {
          this._http.navigateByUrl('/unauthorized');
        }
        return result;
      }
      localStorage.removeItem('AuthenToken');
    }
    this._http.navigateByUrl('/login');
    return false;
  }
}
