﻿import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/services/account/account.service';
import { AccountInput } from 'src/app/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  fullHeight: string;
  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.setFullHeight();
    this.form = this.formBuilder.group({
      'Email': ['', Validators.required  ],
      'Password': ['', Validators.required  ],
    });
  }
  get Password() { return this.form.get('Password'); }
  get Email() { return this.form.get('Email'); }

  setFullHeight() {
    this.fullHeight = window.innerHeight + 'px';
  }

  loginUser() {
    if (!this.form.invalid) {
      this.notificationService.showLoading();

      const user = new AccountInput();
      user.Email = this.Email.value;
      user.Password = this.accountService.encriptPassword(this.Password.value);

      this.accountService.login(user).subscribe(
        (data: AccountInput) => {
          localStorage.setItem('AuthenToken', data.Password);
          this.notificationService.success();
          this.router.navigateByUrl(`/dashboard`);
        },
        error => {
          if (error.error) {
            this.notificationService.error(error.error);
          } else {
          let message = '';
          if (error.error) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        }
        });
      }
  }
}
