import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeRole, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { EmployeeRoleService } from 'src/app/services/employee-role/employee-role.service';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-employee-role',
  templateUrl: './employee-role.component.html',
  styleUrls: ['./employee-role.component.css']
})

export class EmployeeRoleComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  userRoles: EmployeeRole[];
  userRoleID = 0;
  isCreate = false;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private userRoleService: EmployeeRoleService
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.userRoleService.getEmployeeRoles(index, this.page.PageSize, this.searchInput)
          .subscribe(
            (data: DataResult < PagingData < EmployeeRole[] >> ) => {
              this.userRoles = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(message);
            });
    }

  editRole(userRole: EmployeeRole) {
    this.notificationService.showLoading();
        this.userRoleService.editEmployeeRole(userRole)
          .subscribe(
            (data: EmployeeRole ) => {
              this.notificationService.success('Change user ' + data.FirstName + ' successfully!');
            },
            error => {
              let message = '';
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(message);
            });
  }
}
