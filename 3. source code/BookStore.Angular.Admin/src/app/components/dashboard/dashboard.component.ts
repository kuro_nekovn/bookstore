import { Component, OnInit } from '@angular/core';
import { EmployeeInput } from 'src/app/models';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { NotificationService, UploadService } from 'src/app/services';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  employee: EmployeeInput;
  isReady = false;
  thumbnailFile: File;
  constructor(
    private employeeService: EmployeeService,
    private notificationService: NotificationService,
    private accountService: AccountService,
    private uploadService: UploadService,
    private route: ActivatedRoute,
    ) { }

  ngOnInit(
  ) {
    this.getEmployee();
  }
  getEmployee() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeProfile().subscribe(
      (data: EmployeeInput) => {
        this.employee = data;
        this.isReady = true;
        this.notificationService.success();
        },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }
  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();
    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'employee').subscribe(
      (data: string) => {
        this.accountService.changeEmployeeImage(data).subscribe(
          (data1: Boolean) => {
            if (data1) {
              location.reload();
              this.notificationService.success();
            } else {
              this.notificationService.error('Change avatar failed');
            }
          },
          error => {
            let message: string;
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(message);
          });
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  getRole(): string {
    return this.accountService.getUserRole();
  }
}
