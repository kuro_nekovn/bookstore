import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Author, AuthorInput} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { AuthorService } from 'src/app/services/author/author.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-create-author',
  templateUrl: './create-author.component.html',
  styleUrls: ['./create-author.component.css']
})
export class CreateAuthorComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  author: Author;
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private authorService: AuthorService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'LastName': ['', Validators.required  ],
      'FirstName': ['', Validators.required  ],
      'Phone': '',
      'Sex': 'true',
      'Sumary': ['', Validators.compose([Validators.required, Validators.maxLength(299)])  ],
      'ImgUrl': 'Shared/core/author.png',
      'Address': '',
      'Content': '',
      'Birthday': this.dateTime,
    });
  }

  get LastName() { return this.form.get('LastName'); }
  get FirstName() { return this.form.get('FirstName'); }
  get Sumary() { return this.form.get('Sumary'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Address() { return this.form.get('Address'); }
  get Content() { return this.form.get('Content'); }
  get Phone() { return this.form.get('Phone'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Sex() { return this.form.get('Sex'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'author').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/author`);
    });

    if (this.form.valid) {
      const result: AuthorInput = new AuthorInput();
      result.Address = formValue.Address;
      result.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      result.LastName = formValue.LastName;
      result.FirstName = formValue.FirstName;
      result.Sumary = formValue.Sumary;
      result.ImgUrl = formValue.ImgUrl;
      result.Content = formValue.Content;
      result.Phone = formValue.Phone;
      result.Sex = formValue.Sex;
      this.authorService.createAuthor(result).subscribe(
        (data: AuthorInput) => {
          this.notificationService.success('Insert ' + data.FirstName + ' ' + data.LastName + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
