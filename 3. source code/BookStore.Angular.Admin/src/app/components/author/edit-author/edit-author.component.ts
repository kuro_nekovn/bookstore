import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Author, AuthorInput} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { AuthorService } from 'src/app/services/author/author.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.css']
})
export class EditAuthorComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  author: AuthorInput;
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();
  isReady = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private authorService: AuthorService
  ) { }

  ngOnInit() {
    this.getAuthor();
  }

  get LastName() { return this.form.get('LastName'); }
  get FirstName() { return this.form.get('FirstName'); }
  get Sumary() { return this.form.get('Sumary'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Address() { return this.form.get('Address'); }
  get Content() { return this.form.get('Content'); }
  get Phone() { return this.form.get('Phone'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Sex() { return this.form.get('Sex'); }

  getAuthor() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.authorService.getAuthor(id).subscribe(
      (data: AuthorInput) => {
        this.author = data;
        this.form = this.formBuilder.group({
          'LastName': [this.author.LastName , Validators.required  ],
          'FirstName': [this.author.FirstName , Validators.required  ],
          'Phone': this.author.Phone ,
          'Sex': this.author.Sex ? 'true' : 'false' ,
          'Sumary': [this.author.Sumary, Validators.compose([Validators.required, Validators.maxLength(299)])   ],
          'ImgUrl': this.author.ImgUrl,
          'Address': this.author.Address,
          'Content': this.author.Content,
          'Birthday': data.Birthday ? data.Birthday : this.dateTime,
        });
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'author').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new AuthorInput();
      result.Address = formValue.Address;
      result.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      result.LastName = formValue.LastName;
      result.FirstName = formValue.FirstName;
      result.Sumary = formValue.Sumary;
      result.ImgUrl = formValue.ImgUrl;
      result.Content = formValue.Content;
      result.Phone = formValue.Phone;
      result.Sex = formValue.Sex;
      result.ID = this.author.ID;
      this.authorService.editAuthor(result).subscribe(
        (data: Author) => {
          this.notificationService.success('Edited author ' + result.FirstName + ' ' + result.LastName + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
