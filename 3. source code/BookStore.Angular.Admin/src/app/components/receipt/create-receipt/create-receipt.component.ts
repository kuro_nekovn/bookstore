import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Receipt, ReceiptInput, ItemDTO, OrderBook, ReceiptDetail } from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { ReceiptService } from 'src/app/services/receipt/receipt.service';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { PublisherService } from 'src/app/services/publisher/publisher.service';
import { BookService } from 'src/app/services/book/book.service';

@Component({
  selector: 'app-create-receipt',
  templateUrl: './create-receipt.component.html',
  styleUrls: ['./create-receipt.component.css']
})
export class CreateReceiptComponent implements OnInit {
  receipt: ReceiptInput;
  employees: ItemDTO[];
  publishers: ItemDTO[];
  receiptBooks: OrderBook[];
  thumbnailFile: File;
  form: FormGroup;
  isDetailReady = false;
  receiptCreateID = -1;
  receiptDetails: ReceiptDetail[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private receiptService: ReceiptService,
    private employeeService: EmployeeService,
    private publisherService: PublisherService,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'TotalPrice': [ 0, Validators.required  ],
      'EmployeeID': ['', Validators.required  ],
      'PublisherID': ['', Validators.required ],
      'Description': ['', Validators.compose([Validators.required, Validators.maxLength(300)]) ],
      'SelectBook': '',
      'ReceiptBookDetail': this.formBuilder.array([])
    });
    this.getEmployeeItem();
    this.getPublisherItem();
    this.getbookReceipt();
    // tslint:disable-next-line:
    this.form.controls['ReceiptBookDetail'].valueChanges.subscribe(
      value => {
          let sum = 0;
          value.forEach(element => {
            sum += element.TotalPrice;
          });
          this.form.controls['TotalPrice'].setValue(sum);
        });
    }

  get TotalPrice() { return this.form.get('TotalPrice'); }
  get PublisherID() { return this.form.get('PublisherID'); }
  get EmployeeID() { return this.form.get('EmployeeID'); }
  get Description() { return this.form.get('Description'); }
  get SelectBook() { return this.form.get('SelectBook'); }
  get ReceiptBookDetail() { return <FormArray>this.form.get('ReceiptBookDetail'); }


  getEmployeeItem() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeItem().subscribe(
      (data: ItemDTO[]) => {
        this.employees = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getbookReceipt() {
    this.notificationService.showLoading();
    this.bookService.GetOrderBook().subscribe(
      (data: OrderBook[]) => {
        this.receiptBooks = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }


  getPublisherItem() {
    this.notificationService.showLoading();
    this.publisherService.getPublisherItem().subscribe(
      (data: ItemDTO[]) => {
        this.publishers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
        this.router.navigateByUrl(`/receipt/edit/${this.receiptCreateID}`);
    });
    if (this.form.valid) {
      const result: ReceiptInput = new ReceiptInput();
      result.TotalPrice = formValue.TotalPrice;
      result.PublisherID = formValue.PublisherID;
      result.EmployeeID = formValue.EmployeeID;
      result.Description = formValue.Description;
      result.ReceiptDetailModel = formValue.ReceiptBookDetail;
      this.receiptService.createReceipt(result).subscribe(
        (data: ReceiptInput) => {
          this.receiptCreateID = data.ReceiptID;
          this.notificationService.success('Create Receipt successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }

  addOderDetail() {
   if (this.SelectBook.valid) {
    const control = <FormArray>this.ReceiptBookDetail;
    const isSame = false;
    // tslint:disable-next-line:prefer-const
    for (let ctrl of control.controls) {
      // tslint:disable-next-line:triple-equals
      if (ctrl.value.BookID == this.SelectBook.value.ID) {
        this.notificationService.showLoading();
        this.notificationService.error('Cant add same book!');
        return;
      }
    }
    const total = this.SelectBook.value.Total;
    control.push(
      this.formBuilder.group({
       'BookName': this.SelectBook.value.Name,
       'BookID': this.SelectBook.value.ID,
       'ImgUrl': this.SelectBook.value.ImgUrl,
       'Price': [0, Validators.min(1)],
       'Total': total,
       'Quantity': [0, Validators.min(1)],
       'TotalPrice': [0, Validators.min(1)]
      })
    );
    this.isDetailReady = true;
   } else {
    this.notificationService.showLoading();
    this.notificationService.error('Invalid Book. please choose book for receipt');
   }
  }

  deleteOderDetail(index) {
    this.ReceiptBookDetail.removeAt(index);
  }

  updateTotalPrice(formValue) {
    const Price = formValue.value.Price;
    const Quantity = formValue.value.Quantity;
    const TotalPrice = Quantity * Price;
    formValue.patchValue({
       'TotalPrice': TotalPrice
    });
  }
}
