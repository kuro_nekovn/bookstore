import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Receipt, ReceiptInput, ItemDTO, OrderBook, ReceiptDetail } from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { ReceiptService } from 'src/app/services/receipt/receipt.service';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { PublisherService } from 'src/app/services/publisher/publisher.service';
import { BookService } from 'src/app/services/book/book.service';

@Component({
  selector: 'app-edit-receipt',
  templateUrl: './edit-receipt.component.html',
  styleUrls: ['./edit-receipt.component.css']
})
export class EditReceiptComponent implements OnInit {
  receipt: ReceiptInput;
  employees: ItemDTO[];
  publishers: ItemDTO[];
  receiptBooks: OrderBook[];
  thumbnailFile: File;
  form: FormGroup;
  isDetailReady = false;
  receiptDetails: ReceiptDetail[] = [];
  isReady = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private receiptService: ReceiptService,
    private employeeService: EmployeeService,
    private publisherService: PublisherService,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'TotalPrice': [ 0, Validators.required  ],
      'EmployeeID': ['', Validators.required  ],
      'PublisherID': ['', Validators.required ],
      'Description': ['', Validators.compose([Validators.required, Validators.maxLength(300)])  ],
      'SelectBook': '',
      'ReceiptBookDetail': this.formBuilder.array([])
    });
    this.getEmployeeItem();
    this.getPublisherItem();
    this.getbookReceipt();
    this.getReceipt();
    // tslint:disable-next-line:
    this.form.controls['ReceiptBookDetail'].valueChanges.subscribe(
      value => {
          let sum = 0;
          value.forEach(element => {
            sum += element.TotalPrice;
          });
          this.form.controls['TotalPrice'].setValue(sum);
        });
    }

  get TotalPrice() { return this.form.get('TotalPrice'); }
  get PublisherID() { return this.form.get('PublisherID'); }
  get EmployeeID() { return this.form.get('EmployeeID'); }
  get Description() { return this.form.get('Description'); }
  get SelectBook() { return this.form.get('SelectBook'); }
  get ReceiptBookDetail() { return <FormArray>this.form.get('ReceiptBookDetail'); }


  getReceipt() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.receiptService.getReceipt(id).subscribe(
      (data: ReceiptInput) => {
        this.receipt = data;
        this.form.patchValue({
          'TotalPrice': data.TotalPrice,
          'EmployeeID': data.EmployeeID,
          'PublisherID': data.PublisherID,
          'Description': data.Description
        });
        const control = <FormArray>this.ReceiptBookDetail;
        data.ReceiptDetailModel.forEach(x => {
          control.push(
            this.formBuilder.group({
             'ReceiptID': id,
             'BookName': x.BookName,
             'BookID': x.BookID,
             'ImgUrl': x.ImgUrl,
             'Price': x.Price,
             'Total': x.Total,
             'Quantity': [ x.Quantity,  Validators.min(1)],
             'TotalPrice': [ x.TotalPrice, Validators.min(1)]
            })
          );
        });
        this.isReady = true;
        this.isDetailReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  getEmployeeItem() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeItem().subscribe(
      (data: ItemDTO[]) => {
        this.employees = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getbookReceipt() {
    this.notificationService.showLoading();
    this.bookService.GetOrderBook().subscribe(
      (data: OrderBook[]) => {
        this.receiptBooks = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }


  getPublisherItem() {
    this.notificationService.showLoading();
    this.publisherService.getPublisherItem().subscribe(
      (data: ItemDTO[]) => {
        this.publishers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return true;
      };
  });
    if (this.form.valid) {
      const result: ReceiptInput = new ReceiptInput();
      result.ReceiptID = this.route.snapshot.params['id'];
      result.TotalPrice = formValue.TotalPrice;
      result.PublisherID = formValue.PublisherID;
      result.EmployeeID = formValue.EmployeeID;
      result.Description = formValue.Description;
      result.ReceiptDetailModel = formValue.ReceiptBookDetail;
      this.receiptService.editReceipt(result).subscribe(
        (data: ReceiptInput) => {
          this.notificationService.success('Edit Receipt successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }

  addOderDetail() {
   if (this.SelectBook.valid) {
    const control = <FormArray>this.ReceiptBookDetail;
    const isSame = false;
    // tslint:disable-next-line:prefer-const
    for (let ctrl of control.controls) {
      // tslint:disable-next-line:triple-equals
      if (ctrl.value.BookID == this.SelectBook.value.ID) {
        this.notificationService.showLoading();
        this.notificationService.error('Cant add same book!');
        return;
      }
    }
    const total = this.SelectBook.value.Total;
    control.push(
      this.formBuilder.group({
       'ReceiptID': -1,
       'BookName': this.SelectBook.value.Name,
       'BookID': this.SelectBook.value.ID,
       'ImgUrl': this.SelectBook.value.ImgUrl,
       'Price': 0,
       'Total': total,
       'Quantity': [ 0, Validators.min(1)],
       'TotalPrice': [0, Validators.min(1)]
      })
    );
    this.isDetailReady = true;
   } else {
    this.notificationService.showLoading();
    this.notificationService.error('Invalid Book. please choose book for receipt');
   }
  }

  deleteOderDetail(bookDetail, index) {
    if (!bookDetail.ReceiptID) {
      this.ReceiptBookDetail.removeAt(index);
      return;
    }
    this.notificationService.showLoading();
    this.receiptService.deleteDetail(bookDetail.ReceiptID, bookDetail.BookID).subscribe(
      (data: boolean) => {
        if (data) {
          this.ReceiptBookDetail.removeAt(index);
          this.notificationService.success();
        } else {
          this.notificationService.error(`Delete ${bookDetail.BookName} in receipts failed`);
        }
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      }
    );
  }

  updateTotalPrice(formValue) {
    const Price = formValue.value.Price;
    const Quantity = formValue.value.Quantity;
    const TotalPrice = Quantity * Price;
    formValue.patchValue({
       'TotalPrice': TotalPrice
    });
  }
}
