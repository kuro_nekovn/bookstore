import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Complain, ComplainInput, ItemDTO} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { ComplainService } from 'src/app/services/complain/complain.service';
import { formatDate } from '@angular/common';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-create-complain',
  templateUrl: './create-complain.component.html',
  styleUrls: ['./create-complain.component.css']
})
export class CreateComplainComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  complain: Complain;
  form: FormGroup;
  employees: ItemDTO[];
  customers: ItemDTO[];
  orders: ItemDTO[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private complainService: ComplainService,
    private employeeService: EmployeeService,
    private customerService: CustomerService,
    private orderService: OrderService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'ID': '',
      'EmployeeID': ['', Validators.required  ],
      'CustomerID': ['', Validators.required  ],
      'Content': '',
      'Status': '',
      'OrderID': ['', Validators.required  ]
    });
    this.getEmployeeItem();
    this.getCustomerItem();
    this.getOrderItem();
  }

  get Status() { return this.form.get('Status'); }
  get EmployeeID() { return this.form.get('EmployeeID'); }
  get CustomerID() { return this.form.get('CustomerID'); }
  get Content() { return this.form.get('Content'); }
  get OrderID() { return this.form.get('OrderID'); }
  get ID() { return this.form.get('ID'); }


  getEmployeeItem() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeItem().subscribe(
      (data: ItemDTO[]) => {
        this.employees = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getCustomerItem() {
    this.notificationService.showLoading();
    this.customerService.getCustomerItem().subscribe(
      (data: ItemDTO[]) => {
        this.customers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getOrderItem() {
    this.notificationService.showLoading();
    this.orderService.getOrderItem().subscribe(
      (data: ItemDTO[]) => {
        this.orders = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/complain`);
    });

    if (this.form.valid) {
      const result: ComplainInput = new ComplainInput();
      result.EmployeeID = formValue.EmployeeID;
      result.CustomerID = formValue.CustomerID;
      result.Status = formValue.Status;
      result.OrderID = formValue.OrderID;
      result.Content = formValue.Content;
      this.complainService.createComplain(result).subscribe(
        (data: ComplainInput) => {
          this.notificationService.success('Insert complain successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
