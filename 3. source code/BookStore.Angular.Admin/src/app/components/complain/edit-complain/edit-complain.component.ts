import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Complain, ComplainInput, ItemDTO} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { ComplainService } from 'src/app/services/complain/complain.service';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-edit-complain',
  templateUrl: './edit-complain.component.html',
  styleUrls: ['./edit-complain.component.css']
})
export class EditComplainComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  complain: ComplainInput;
  form: FormGroup;
  isReady = false;
  employees: ItemDTO[];
  customers: ItemDTO[];
  orders: ItemDTO[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private complainService: ComplainService,
    private employeeService: EmployeeService,
    private customerService: CustomerService,
    private orderService: OrderService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'ID': '',
      'EmployeeID': ['', Validators.required  ],
      'CustomerID': ['', Validators.required  ],
      'Content': '',
      'Status': '',
      'OrderID': ['', Validators.required  ]
    });
    this.getEmployeeItem();
    this.getCustomerItem();
    this.getOrderItem();
    this.getComplain();
  }

  get Status() { return this.form.get('Status'); }
  get EmployeeID() { return this.form.get('EmployeeID'); }
  get CustomerID() { return this.form.get('CustomerID'); }
  get Content() { return this.form.get('Content'); }
  get OrderID() { return this.form.get('OrderID'); }
  get ID() { return this.form.get('ID'); }


  getComplain() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.complainService.getComplain(id).subscribe(
      (data: ComplainInput) => {
        this.complain = data;
        this.form.patchValue({
          EmployeeID: data.EmployeeID,
          CustomerID: data.CustomerID,
          Content: data.Content,
          Status: data.Status,
          OrderID: data.OrderID,
          ID: id,
        });
        this.form.get('OrderID').disable();
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  getOrderItem() {
    this.notificationService.showLoading();
    this.orderService.getOrderItem().subscribe(
      (data: ItemDTO[]) => {
        this.orders = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getEmployeeItem() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeItem().subscribe(
      (data: ItemDTO[]) => {
        this.employees = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getCustomerItem() {
    this.notificationService.showLoading();
    this.customerService.getCustomerItem().subscribe(
      (data: ItemDTO[]) => {
        this.customers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new ComplainInput();
      result.EmployeeID = formValue.EmployeeID;
      result.CustomerID = formValue.CustomerID;
      result.Status = formValue.Status;
      result.OrderID = formValue.OrderID;
      result.Content = formValue.Content;
      result.ID = formValue.ID;
      this.complainService.editComplain(result).subscribe(
        (data: ComplainInput) => {
          this.notificationService.success('Edited complain sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
