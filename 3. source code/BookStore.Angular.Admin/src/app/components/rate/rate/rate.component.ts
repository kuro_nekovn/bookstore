import { Component, OnInit } from '@angular/core';
import { Rate, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { RateService } from 'src/app/services/rate/rate.service';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.css']
})
export class RateComponent implements OnInit {
  page: Pagination = new Pagination();
  customerName: string;
  bookName: string;
  keyword: string;
  rates: Rate[];
  constructor(
    private notificationService: NotificationService,
    private rateService: RateService,
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.rateService.getRates(index, this.page.PageSize, this.keyword, this.bookName, this.customerName)
          .subscribe(
            (data: DataResult < PagingData < Rate[] >> ) => {
              this.rates = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.rateService.deleteRate(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this rate!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
