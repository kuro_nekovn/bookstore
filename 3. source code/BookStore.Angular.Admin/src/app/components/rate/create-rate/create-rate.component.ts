import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Rate, RateInput, ItemDTO } from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { RateService } from 'src/app/services/rate/rate.service';
import { BookService } from 'src/app/services/book/book.service';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-create-rate',
  templateUrl: './create-rate.component.html',
  styleUrls: ['./create-rate.component.css']
})
export class CreateRateComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  rate: RateInput;
  books: ItemDTO[];
  customers: ItemDTO[];
  thumbnailFile: File;
  form: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private rateService: RateService,
    private bookService: BookService,
    private customerService: CustomerService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'Rating': ['', Validators.required  ],
      'CustomerID': ['', Validators.required ],
      'BookID': ['', Validators.required ],
      'Content': ['', Validators.required  ]
    });
    this.getBookItem();
    this.getCustomerItem();
  }

  get Title() { return this.form.get('Title'); }
  get CustomerID() { return this.form.get('CustomerID'); }
  get BookID() { return this.form.get('BookID'); }
  get Rating() { return this.form.get('Rating'); }
  get Content() { return this.form.get('Content'); }



  getBookItem() {
    this.notificationService.showLoading();
    this.bookService.getBookItem().subscribe(
      (data: ItemDTO[]) => {
        this.books = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getCustomerItem() {
    this.notificationService.showLoading();
    this.customerService.getCustomerItem().subscribe(
      (data: ItemDTO[]) => {
        this.customers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/rate`);
    });
    if (this.form.valid) {
      const result: RateInput = new RateInput();
      result.Title = formValue.Title;
      result.CustomerID = formValue.CustomerID;
      result.BookID = formValue.BookID;
      result.Content = formValue.Content;
      result.Rating = formValue.Rating;
      this.rateService.createRate(result).subscribe(
        (data: RateInput) => {
          this.notificationService.success('Insert ' + data.Title + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
