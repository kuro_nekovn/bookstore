import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Rate, RateInput, ItemDTO, BookInput } from 'src/app/models';
import {  NotificationService } from 'src/app/services';
import { RateService } from 'src/app/services/rate/rate.service';
import { BookService } from 'src/app/services/book/book.service';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-edit-rate',
  templateUrl: './edit-rate.component.html',
  styleUrls: ['./edit-rate.component.css']
})
export class EditRateComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  rate: RateInput;
  books: ItemDTO[];
  customers: ItemDTO[];
  thumbnailFile: File;
  form: FormGroup;
  isReady = false;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private rateService: RateService,
    private bookService: BookService,
    private customerService: CustomerService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Title': ['', Validators.required  ],
      'Rating': ['', Validators.required  ],
      'CustomerID': ['', Validators.required ],
      'BookID': ['', Validators.required ],
      'Content': ['', Validators.required  ]
    });
    this.getBookItem();
    this.getCustomerItem();
    this.getRate();
  }

  get Title() { return this.form.get('Title'); }
  get CustomerID() { return this.form.get('CustomerID'); }
  get BookID() { return this.form.get('BookID'); }
  get Rating() { return this.form.get('Rating'); }
  get Content() { return this.form.get('Content'); }


  getRate() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.rateService.getRate(id).subscribe(
      (data: RateInput) => {
        this.rate = data;
        this.form.setValue({
          Title: data.Title,
          CustomerID: data.CustomerID,
          BookID: data.BookID,
          Content: data.Content,
          Rating: data.Rating
        });
        this.form.get('BookID').disable();
        this.form.get('CustomerID').disable();
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  getBookItem() {
    this.notificationService.showLoading();
    this.bookService.getBookItem().subscribe(
      (data: ItemDTO[]) => {
        this.books = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }

  getCustomerItem() {
    this.notificationService.showLoading();
    this.bookService.getBookItem().subscribe(
      (data: ItemDTO[]) => {
        this.customers = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      }
    );
  }



  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new RateInput();
      result.Title = formValue.Title;
      result.CustomerID = formValue.CustomerID;
      result.BookID = formValue.BookID;
      result.Content = formValue.Content;
      result.Rating = formValue.Rating;
      result.ID = this.rate.ID;
      this.rateService.editRate(result).subscribe(
        (data: RateInput) => {
          this.notificationService.success('Edited blog ' + result.Title + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
