import { Component, OnInit } from '@angular/core';
import { Customer, PagingData, DataResult, Pagination, AccountInput } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { AccountService } from 'src/app/services/account/account.service';
declare var $: any;


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  customers: Customer[];
  form: FormGroup;
  isAdmin = false;
  constructor(
    private notificationService: NotificationService,
    private customerService: CustomerService,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
    this.form = this.formBuilder.group({
      'CustomerID': -1,
      'ConfirmPassword': ['', Validators.required  ],
      'Password': ['', Validators.required  ],
    },
    {
      validator: passwordMatchValidator
    });
    this.isAdmin = this.accountService.roleMatch(['Admin']);
  }

  get CustomerID() { return this.form.get('CustomerID'); }
  get ConfirmPassword() { return this.form.get('ConfirmPassword'); }
  get Password() { return this.form.get('Password'); }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.customerService.getCustomers(index, this.page.PageSize, this.searchInput)
          .subscribe(
            (data: DataResult < PagingData < Customer[] >> ) => {
              this.customers = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.customerService.deleteCustomer(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this customer!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }

  setID(ID) {
    this.form.patchValue({
      'CustomerID': ID
    });
  }

  changePassword(fromVaule) {
    if (!this.form.invalid && this.isAdmin) {
      this.notificationService.showLoading();
      const accountInput = new AccountInput();
      accountInput.ID = fromVaule.CustomerID;
      accountInput.Password = this.accountService.encriptPassword(this.Password.value);
      $('#change-password').modal('hide');
      this.accountService.changeCustomerPassword(accountInput).subscribe(
        (data: boolean) => {
            if (data) {
              this.notificationService.success('Mật khẩu thay đổi thành công!');
            } else { this.notificationService.error('Mật khẩu không chính xác'); }
        },
        error => {
          let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
        }
      );
    }
  }

  resetInput() {
    this.form.patchValue({
      'CustomerID': -1,
      'Password': '',
      'ConfirmPassword': ''
    });
  }

  onPasswordInput() {
    if (this.form.hasError('passwordMismatch')) {
      this.ConfirmPassword.setErrors([{'passwordMismatch': true}]);
    } else {
      this.ConfirmPassword.setErrors(null);
    }
  }
}

export const passwordMatchValidator: ValidatorFn = (form: FormGroup): ValidationErrors | null => {
  if (form.get('Password').value === form.get('ConfirmPassword').value) {
    return null;
  } else {
    return {passwordMismatch: true};
  }
};
