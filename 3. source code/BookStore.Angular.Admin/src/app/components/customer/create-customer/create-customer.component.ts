import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { DataResult, CustomerInput } from 'src/app/models';
import { NotificationService, UploadService } from 'src/app/services';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { formatDate } from '@angular/common';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;

  thumbnailFile: File;
  id: number;
  form: FormGroup;
  customer: CustomerInput;
  dateTime: Date = new Date();

  constructor(
    private router: Router,
    private customerService: CustomerService,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
  ) { }

  ngOnInit() {
      this.form = this.formBuilder.group({
        'FirstName': ['', Validators.required  ],
        'LastName': ['', Validators.required  ],
        'Email': ['', Validators.compose([Validators.required, Validators.email])],
        'Sex': 'true',
        'Birthday': this.dateTime,
        'Phone': '',
        'Address': '',
        'ImgUrl' : '',
        'Password': ['', Validators.required  ],
      });
  }

  get FirstName() { return this.form.get('FirstName'); }
  get LastName() { return this.form.get('LastName'); }
  get Email() { return this.form.get('Email'); }
  get Sex() { return this.form.get('Sex'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Phone() { return this.form.get('Phone'); }
  get Address() { return this.form.get('Address'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Password() { return this.form.get('Password'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'customer').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/customer`);
    });

    if (this.form.valid) {
      // tslint:disable-next-line:prefer-const
      let customerInput = new CustomerInput();
      customerInput.FirstName = formValue.FirstName;
      customerInput.LastName = formValue.LastName;
      customerInput.Email = formValue.Email;
      customerInput.Phone = formValue.Phone;
      customerInput.Sex = formValue.Sex;
      customerInput.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      customerInput.ImgUrl = formValue.ImgUrl;
      customerInput.Point = 0;
      customerInput.Address = formValue.Address;
      customerInput.Password = this.accountService.encriptPassword(formValue.Password);
      this.customerService.createCustomer(customerInput).subscribe(
        (data: CustomerInput) => {
          this.notificationService.success('Insert ' + data.FirstName + ' ' + data.LastName + ' successfully');
        },
        error => {
          let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
