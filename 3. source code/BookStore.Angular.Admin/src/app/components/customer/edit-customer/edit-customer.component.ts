import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer, CustomerInput} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
  customer: CustomerInput;
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();
  isReady = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private customerService: CustomerService
  ) { }

  ngOnInit() {
    this.getCustomer();
  }

  get FirstName() { return this.form.get('FirstName'); }
  get LastName() { return this.form.get('LastName'); }
  get Email() { return this.form.get('Email'); }
  get Sex() { return this.form.get('Sex'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Phone() { return this.form.get('Phone'); }
  get Address() { return this.form.get('Address'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }

  getCustomer() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.customerService.getCustomer(id).subscribe(
      (data: CustomerInput) => {
        this.customer = data;
        this.form = this.formBuilder.group({
          'LastName': [data.LastName , Validators.required  ],
          'FirstName': [data.FirstName , Validators.required  ],
          'Email': [data.Email ,  Validators.compose([Validators.required, Validators.email])  ],
          'Phone': data.Phone ,
          'Sex': data.Sex ? 'true' : 'false' ,
          'ImgUrl': data.ImgUrl,
          'Address': data.Address,
          'Birthday': data.Birthday ? data.Birthday : this.dateTime,
        });
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'customer').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new CustomerInput();
      result.Address = formValue.Address;
      result.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      result.LastName = formValue.LastName;
      result.FirstName = formValue.FirstName;
      result.ImgUrl = formValue.ImgUrl;
      result.Phone = formValue.Phone;
      result.Sex = formValue.Sex;
      result.ID = this.customer.ID;
      result.Email = formValue.Email;
      this.customerService.editCustomer(result).subscribe(
        (data: Customer) => {
          this.notificationService.success('Edited customer ' + result.FirstName + ' ' + result.LastName + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
