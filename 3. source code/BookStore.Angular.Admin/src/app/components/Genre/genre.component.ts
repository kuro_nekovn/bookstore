import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Genre, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { GenreService } from 'src/app/services/genre/genre.service';
import {  Router } from '@angular/router';


@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})
export class GenreComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  genres: Genre[];
  genreID = 0;
  isCreate = false;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private genreService: GenreService
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
    this.initialize();
  }

  initialize() {
    this.form = this.formBuilder.group({
      'Name': ['', Validators.required  ],
      'Description': ['', Validators.compose([Validators.required, Validators.maxLength(299)])],
    });
  }

  get Name() { return this.form.get('Name'); }
  get Description() { return this.form.get('Description'); }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.genreService.getGenres(index, this.page.PageSize, this.searchInput)
          .subscribe(
            (data: DataResult < PagingData < Genre[] >> ) => {
              this.genres = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading(true, 0, () => {
      this.refreshData(this.page.Currentpage);
    });
    this.genreService.deleteGenre(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this genre!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }

  getGenre(ID: number) {
    this.notificationService.showLoading();
    if (ID > 0) {
      this.isCreate = false;
      this.genreService.getGenre(ID).subscribe(
        (data: Genre) => {
          this.form.setValue({
            Name: data.Name,
            Description: data.Description
          });
          this.genreID = data.ID;
          this.notificationService.success();
        },
        error => {
          let message;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        }
      );
    } else {
      this.isCreate = true;
      this.form.setValue({
        Name: '',
        Description: ''
      });
    }
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.refreshData(this.page.Currentpage);
    });

    if (this.form.valid) {
      const result: Genre = new Genre();
      result.Name = formValue.Name;
      result.Description = formValue.Description;
      result.ID = this.genreID;
      if (this.isCreate) {
        this.genreService.createGenre(result).subscribe(
          (data: Genre) => {
            this.notificationService.success('Insert ' + data.Name + ' successfully');
          },
          error => {
            let message: string;
            if (error.error.Message) {
              message = error.message + '\n' + error.error.Message;
            } else { message = error.message; }
            this.notificationService.error(message);
          });
      } else {
        this.genreService.editGenre(result).subscribe(
          (data: Genre) => {
            this.notificationService.success('Edit ' + data.Name + ' successfully');
          },
          error => {
            let message: string;
            if (error.error.Message) {
              message = error.message + '\n' + error.error.Message;
            } else { message = error.message; }
            this.notificationService.error(message);
          });
      }
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
