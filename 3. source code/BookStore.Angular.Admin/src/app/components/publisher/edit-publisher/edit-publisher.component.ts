import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { QuillEditorComponent } from 'ngx-quill';
import { Publisher, DataResult} from 'src/app/models';
import { PublisherService, NotificationService, UploadService } from 'src/app/services';

@Component({
  selector: 'app-edit-publisher',
  templateUrl: './edit-publisher.component.html',
  styleUrls: ['./edit-publisher.component.css']
})
export class EditPublisherComponent implements OnInit {
  publisher: Publisher;
  isReady = false;
  // @ViewChild('editor') editor: QuillEditorComponent;

  thumbnailFile: File;


  form: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private publisherService: PublisherService
  ) { }

  ngOnInit() {
    this.getPublisher();
  }

  get Company() { return this.form.get('Company'); }
  get Address() { return this.form.get('Address'); }
  get Phone() { return this.form.get('Phone'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }

  getPublisher() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.publisherService.getPublisher(id).subscribe(
      (data: Publisher) => {
        this.publisher = data;
        this.form = this.formBuilder.group({
          'Company': [this.publisher.Name,
            Validators.required
            ],
          'Address': [this.publisher.Address,
          Validators.required
          ],
          'ImgUrl': this.publisher.ImgUrl,
          'Phone': [this.publisher.Phone,
          Validators.required
          ]
        });
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'publisher').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {

        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      this.publisher.Name = this.Company.value;
      this.publisher.Address = this.Address.value;
      this.publisher.Phone = this.Phone.value;
      this.publisher.ImgUrl = this.ImgUrl.value;
      this.publisherService.editPublisher(this.publisher).subscribe(
        (data: Publisher) => {
          this.notificationService.success('Edited publisher ' + data.Name + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
