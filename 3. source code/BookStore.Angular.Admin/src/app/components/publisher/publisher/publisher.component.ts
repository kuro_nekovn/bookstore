import { Component, OnInit } from '@angular/core';
import { Publisher, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService, PublisherService } from 'src/app/services';
import { Router } from '@angular/router';


@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})
export class PublisherComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  publishers: Publisher[];
  constructor(
    private notificationService: NotificationService,
    private publisherService: PublisherService,
    private router: Router

  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.publisherService.getPublishers(index, this.page.PageSize, this.searchInput)
          .subscribe(
            (data: DataResult < PagingData < Publisher[] >> ) => {
              this.publishers = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

    refreshDataAndPage(index: number = 1, message: string = null) {
      this.router.navigateByUrl(window.location.href);
      this.notificationService.showLoading();
      this.publisherService.getPublishers(index, this.page.PageSize, this.searchInput)
        .subscribe(
          (data: DataResult < PagingData < Publisher[] >> ) => {
            this.publishers = data.Data.Items;
            this.page = data.Data.Page;
            this.page.Currentpage = index;
            this.notificationService.success(message);
          },
          // tslint:disable-next-line:no-shadowed-variable
          error => {
            if (error.error.Message) {
              message = error.message + '\n' + error.error.Message;
            } else { message = error.message; }
            this.notificationService.error(error.message);
          });
  }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.publisherService.deletePublisher(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this publisher!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
