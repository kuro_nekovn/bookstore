import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { QuillEditorComponent } from 'ngx-quill';
import { Publisher, DataResult} from 'src/app/models';
import { PublisherService, NotificationService, UploadService } from 'src/app/services';


@Component({
  selector: 'app-create-publisher',
  templateUrl: './create-publisher.component.html',
  styleUrls: ['./create-publisher.component.css']
})
export class CreatePublisherComponent implements OnInit {
  publisher: Publisher;
  // @ViewChild('editor') editor: QuillEditorComponent;

  thumbnailFile: File;


  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private publisherService: PublisherService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Company': ['', Validators.required  ],
      'Address': ['', Validators.required  ],
      'Phone': ['', Validators.required  ],
      'ImgUrl': 'Shared/core/publisher.png'
    });
  }

  get Company() { return this.form.get('Company'); }
  get Address() { return this.form.get('Address'); }
  get Phone() { return this.form.get('Phone'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'publisher').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/publisher`);
    });

    if (this.form.valid) {
      const result: Publisher = new Publisher();
      result.Name = formValue.Company;
      result.Address = formValue.Address;
      result.Phone = formValue.Phone;
      result.ImgUrl = formValue.ImgUrl;
      this.publisherService.createPublisher(result).subscribe(
        (data: Publisher) => {
          this.notificationService.success('Create publisher ' + data.Name + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
