import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { DataResult, EmployeeInput, Role } from 'src/app/models';
import { NotificationService, UploadService } from 'src/app/services';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { AccountService } from 'src/app/services/account/account.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;

  thumbnailFile: File;
  id: number;
  form: FormGroup;
  employee: EmployeeInput;
  dateTime: Date = new Date();
  roles: Role[];

  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private accountService: AccountService,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
      this.form = this.formBuilder.group({
        'FirstName': ['', Validators.required  ],
        'LastName': ['', Validators.required  ],
        'Email': ['', Validators.compose([Validators.required, Validators.email])],
        'Sex': 'true',
        'Birthday': this.dateTime,
        'Phone': '',
        'Address': '',
        'ImgUrl' : '',
        'Password': ['', Validators.required  ]
      });
     this.getRoles();
  }

  get FirstName() { return this.form.get('FirstName'); }
  get LastName() { return this.form.get('LastName'); }
  get Email() { return this.form.get('Email'); }
  get Sex() { return this.form.get('Sex'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Phone() { return this.form.get('Phone'); }
  get Address() { return this.form.get('Address'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get Password() { return this.form.get('Password'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'employee').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  getRoles() {
    this.notificationService.showLoading();
    this.accountService.getRoles().subscribe(
      (data: Role[]) => {
        this.roles = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else {
          message = error.message;
        }
        this.notificationService.error(message);
        }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/employee`);
    });

    if (this.form.valid) {
      // tslint:disable-next-line:prefer-const
      let employeeInput = new EmployeeInput();
      employeeInput.FirstName = formValue.FirstName;
      employeeInput.LastName = formValue.LastName;
      employeeInput.Email = formValue.Email;
      employeeInput.Phone = formValue.Phone;
      employeeInput.Sex = formValue.Sex;
      employeeInput.Address = formValue.Address;
      employeeInput.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      employeeInput.ImgUrl = formValue.ImgUrl;
      employeeInput.Password = this.accountService.encriptPassword(formValue.Password);
      employeeInput.RoleID = formValue.RoleID;
      this.employeeService.createEmployee(employeeInput).subscribe(
        (data: EmployeeInput) => {
          this.notificationService.success('Insert ' + data.FirstName + ' ' + data.LastName + ' successfully');
        },
        error => {
          let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
