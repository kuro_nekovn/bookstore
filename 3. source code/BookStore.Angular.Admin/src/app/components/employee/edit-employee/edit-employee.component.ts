import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee, EmployeeInput, Role} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { formatDate } from '@angular/common';
import { AccountService } from 'src/app/services/account/account.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  employee: EmployeeInput;
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();
  isReady = false;
  roles: Role[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private employeeService: EmployeeService,
    private accountService: AccountService,
  ) { }

  ngOnInit() {
    this.getEmployee();
    this.getRoles();
  }

  get FirstName() { return this.form.get('FirstName'); }
  get LastName() { return this.form.get('LastName'); }
  get Email() { return this.form.get('Email'); }
  get Sex() { return this.form.get('Sex'); }
  get Birthday() { return this.form.get('Birthday'); }
  get Phone() { return this.form.get('Phone'); }
  get Address() { return this.form.get('Address'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }

  getEmployee() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.employeeService.getEmployee(id).subscribe(
      (data: EmployeeInput) => {
        this.employee = data;
        this.form = this.formBuilder.group({
          'LastName': [data.LastName , Validators.required  ],
          'FirstName': [data.FirstName , Validators.required  ],
          'Email': [data.Email ,  Validators.compose([Validators.required, Validators.email])  ],
          'Phone': data.Phone ,
          'Sex': data.Sex ? 'true' : 'false' ,
          'ImgUrl': data.ImgUrl,
          'Address': data.Address,
          'Birthday': data.Birthday ? data.Birthday : this.dateTime
        });
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'employee').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  getRoles() {
    this.notificationService.showLoading();
    this.accountService.getRoles().subscribe(
      (data: Role[]) => {
        this.roles = data;
        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else {
          message = error.message;
        }
        this.notificationService.error(message);
        }
    );
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new EmployeeInput();
      result.Address = formValue.Address;
      result.Birthday = new Date(formatDate(formValue.Birthday, 'yyyy-MM-dd', 'en-US'));
      result.LastName = formValue.LastName;
      result.FirstName = formValue.FirstName;
      result.ImgUrl = formValue.ImgUrl;
      result.Phone = formValue.Phone;
      result.Sex = formValue.Sex;
      result.ID = this.employee.ID;
      result.Email = formValue.Email;
      this.employeeService.editEmployee(result).subscribe(
        (data: Employee) => {
          this.notificationService.success('Edited employee ' + result.FirstName + ' ' + result.LastName + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
