import { Component, OnInit } from '@angular/core';
import { Advertisement, PagingData, DataResult, Pagination } from 'src/app/models';
import { NotificationService } from 'src/app/services';
import { AdvertisementService } from 'src/app/services/advertisement/advertisement.service';


@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.css']
})
export class AdvertisementComponent implements OnInit {
  page: Pagination = new Pagination();
  searchInput: string;
  advertisements: Advertisement[];
  constructor(
    private notificationService: NotificationService,
    private advertisementService: AdvertisementService
  ) { }

  ngOnInit() {
    this.page.PageSize = 10;
    this.refreshData();
  }

  refreshData(index: number = 1, message: string = null) {
        this.notificationService.showLoading();
        this.advertisementService.getAdvertisements(index, this.page.PageSize, this.searchInput)
          .subscribe(
            (data: DataResult < PagingData < Advertisement[] >> ) => {
              this.advertisements = data.Data.Items;
              this.page = data.Data.Page;
              this.page.Currentpage = index;
              this.notificationService.success(message);
            },
            // tslint:disable-next-line:no-shadowed-variable
            error => {
              if (error.error.Message) {
                message = error.message + '\n' + error.error.Message;
              } else { message = error.message; }
              this.notificationService.error(error.message);
            });
    }

  delete(ID: number) {
    this.notificationService.showLoading();
    this.advertisementService.deleteAdvertisement(ID).subscribe(
      (data: boolean) => {
        if (data) {
          this.refreshData(this.page.Currentpage, 'Delete sucessfully');
        } else {
          this.notificationService.error('Cannot delete this advertisement!');
        }
      },
      // tslint:disable-next-line:no-shadowed-variable
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
      }
    );
  }
}
