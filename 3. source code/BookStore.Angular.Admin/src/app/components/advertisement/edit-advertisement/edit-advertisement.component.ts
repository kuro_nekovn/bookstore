import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Advertisement} from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { AdvertisementService } from 'src/app/services/advertisement/advertisement.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-edit-advertisement',
  templateUrl: './edit-advertisement.component.html',
  styleUrls: ['./edit-advertisement.component.css']
})
export class EditAdvertisementComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  advertisement: Advertisement;
  thumbnailFile: File;
  form: FormGroup;
  isReady = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private advertisementService: AdvertisementService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Description': ['', Validators.required  ],
      'ImgUrl': [ '', Validators.required  ],
      'DateRelease': [ '', Validators.required  ],
      'DateExpire': [ '', Validators.required  ]
    });
    this.getAdvertisement();
  }

  get Description() { return this.form.get('Description'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get DateRelease() { return this.form.get('DateRelease'); }
  get DateExpire() { return this.form.get('DateExpire'); }


  getAdvertisement() {
    this.notificationService.showLoading();
    const id = this.route.snapshot.params['id'];
    this.advertisementService.getAdvertisement(id).subscribe(
      (data: Advertisement) => {
        this.advertisement = data;
        this.form.setValue({
          Description: data.Description,
          ImgUrl: data.ImgUrl,
          DateRelease: data.DateRelease,
          DateExpire: data.DateExpire
        });
        this.isReady = true;
        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'advertisement').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading();

    if (this.form.valid) {
      const result = new Advertisement();
      result.Description = formValue.Description;
      result.DateRelease = new Date(formatDate(formValue.DateRelease, 'yyyy-MM-dd', 'en-US'));
      result.DateExpire = new Date(formatDate(formValue.DateExpire, 'yyyy-MM-dd', 'en-US'));
      result.ID = this.advertisement.ID;
      result.ImgUrl = formValue.ImgUrl;
      this.advertisementService.editAdvertisement(result).subscribe(
        (data: Advertisement) => {
          this.notificationService.success('Edited advertisement ' + result.Description + ' sucessfully' );
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
