import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuillEditorComponent } from 'ngx-quill';
import { Advertisement } from 'src/app/models';
import {  NotificationService, UploadService } from 'src/app/services';
import { AdvertisementService } from 'src/app/services/advertisement/advertisement.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-create-advertisement',
  templateUrl: './create-advertisement.component.html',
  styleUrls: ['./create-advertisement.component.css']
})
export class CreateAdvertisementComponent implements OnInit {
  @ViewChild('editor') editor: QuillEditorComponent;
  advertisement: Advertisement;
  thumbnailFile: File;
  form: FormGroup;
  dateTime: Date = new Date();


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uploadService: UploadService,
    private notificationService: NotificationService,
    private advertisementService: AdvertisementService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Description': ['', Validators.required  ],
      'ImgUrl': ['', Validators.required  ],
      'DateRelease': [this.dateTime, Validators.required  ],
      'DateExpire': [this.dateTime, Validators.required  ]
    });
  }

  get DateRelease() { return this.form.get('DateRelease'); }
  get Description() { return this.form.get('Description'); }
  get ImgUrl() { return this.form.get('ImgUrl'); }
  get DateExpire() { return this.form.get('DateExpire'); }

  onThumbnailFileChanged(event) {
    this.notificationService.showLoading();

    this.thumbnailFile = event.target.files[0];

    this.uploadService.uploadImage(this.thumbnailFile, 'advertisement').subscribe(
      (data: string) => {
        this.form.controls['ImgUrl'].setValue(data);

        this.notificationService.success();
      },
      error => {
        let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(message);
      });
  }

  onSubmit(formValue) {
    this.notificationService.showLoading(true, 0, () => {
      this.router.navigateByUrl(`/advertisement`);
    });

    if (this.form.valid) {
      const result: Advertisement = new Advertisement();
      result.DateRelease = new Date(formatDate(formValue.DateRelease, 'yyyy-MM-dd', 'en-US'));
      result.DateExpire = new Date(formatDate(formValue.DateExpire, 'yyyy-MM-dd', 'en-US'));
      result.ImgUrl = formValue.ImgUrl;
      result.Description = formValue.Description;
      this.advertisementService.createAdvertisement(result).subscribe(
        (data: Advertisement) => {
          this.notificationService.success('Insert ' + data.Description + ' successfully');
        },
        error => {
          let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
        });
    } else {
      this.notificationService.error('Invalid inputs!');
    }
  }
}
