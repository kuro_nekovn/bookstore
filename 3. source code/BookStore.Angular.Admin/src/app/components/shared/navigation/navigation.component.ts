import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { NotificationService } from 'src/app/services';
import { AccountInput, EmployeeInput } from 'src/app/models';
import { AccountService } from 'src/app/services/account/account.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-navigation, .app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private accountService: AccountService,
    private router: Router
  ) { }
  form: FormGroup;
  employee: EmployeeInput = new EmployeeInput();
  ngOnInit() {
    this.form = this.formBuilder.group({
      'ConfirmPassword': ['', Validators.required  ],
      'Password': ['', Validators.required  ],
    },
    {
      validator: passwordMatchValidator
    });
    this.getEmployee();
  }

  get ConfirmPassword() { return this.form.get('ConfirmPassword'); }
  get Password() { return this.form.get('Password'); }

  logOut() {
    localStorage.removeItem('AuthenToken');
    this.router.navigate(['/login']);
  }

  changePassword(fromVaule) {
    if (!this.form.invalid) {
      this.notificationService.showLoading();
      const accountInput = new AccountInput();
      accountInput.Password = this.accountService.encriptPassword(this.Password.value);
      $('#change-password-nav').modal('hide');
      this.resetInput();
      this.accountService.changePassword(accountInput).subscribe(
        (data: boolean) => {
            if (data) {
              this.notificationService.success('Mật khẩu thay đổi thành công!');
            } else { this.notificationService.error('Mật khẩu không chính xác'); }
        },
        error => {
          let message: string;
        if (error.error.Message) {
          message = error.message + '\n' + error.error.Message;
        } else { message = error.message; }
        this.notificationService.error(error.message);
        }
      );
    }
  }

  resetInput() {
    this.form.patchValue({
      'Password': '',
      'ConfirmPassword': ''
    });
  }

  onPasswordInput() {
    if (this.form.hasError('passwordMismatch')) {
      this.ConfirmPassword.setErrors([{'passwordMismatch': true}]);
    } else {
      this.ConfirmPassword.setErrors(null);
    }
  }

  getEmployee() {
    this.notificationService.showLoading();
    this.employeeService.getEmployeeProfile().subscribe(
      (data: EmployeeInput) => {
        this.employee = data;
        this.notificationService.success();
        },
      error => {
        let message: string;
          if (error.error.Message) {
            message = error.message + '\n' + error.error.Message;
          } else { message = error.message; }
          this.notificationService.error(message);
      });
  }
}

export const passwordMatchValidator: ValidatorFn = (form: FormGroup): ValidationErrors | null => {
  if (form.get('Password').value === form.get('ConfirmPassword').value) {
    return null;
  } else {
    return {passwordMismatch: true};
  }
};

