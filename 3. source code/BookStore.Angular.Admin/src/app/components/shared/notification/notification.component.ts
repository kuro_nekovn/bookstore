import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services';

declare var $: any;

@Component({
  selector: 'app-notification, .app-notification, [app-notification]',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  constructor(
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    // tslint:disable-next-line:prefer-const
    let self = this;
    $('#notification-modal').on('hidden.bs.modal', function (e) {
      self.notificationService.hideLoading();
    });
  }
}
