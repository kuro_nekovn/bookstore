import { Component, OnInit } from '@angular/core';

import { SidebarService } from './../../../services/sidebar/sidebar.service';
import { Router } from '@angular/router';
import { RoleRouting } from 'src/app/models';
import { AccountService } from 'src/app/services/account/account.service';
@Component({
  selector: 'app-side-bar, .app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  roleRouting: RoleRouting = new RoleRouting();
  constructor(
    private sidebarService: SidebarService,
    private accountService: AccountService,
    private router: Router
  ) { }
  ngOnInit() {
  }
  showRoute(roles: string[]): Boolean {
    return this.accountService.roleMatch(roles);
  }
}
