﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Commons.Helpers
{
    /// <summary>
    /// Verify fields of entity correct with required.
    /// </summary>
    public static class ValidationHelpers
    {
        /// <summary>
        /// Verify fields for objects.
        /// </summary>
        /// <param name="entity">Represent for objects.</param>
        /// <returns>Returns status check validate for entity</returns>
        public static bool IsValid(object entity)
        {
            if (entity == null)
            {
                return false;
            }

            var context = new ValidationContext(entity, serviceProvider: null, items: null);

            var result = new List<ValidationResult>();
            return Validator.TryValidateObject(entity, context, result, validateAllProperties: true
            );
        }
    }
}