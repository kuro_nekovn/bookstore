﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BookStore.Commons.Helpers
{
    public static class EncryptionHelpers
    {
        public static string Encryption(string strText)
        {
            var publicKey = "<RSAKeyValue><Modulus>pR34oeVqou5ATXQ+hIaIKpbT2yitbS+hQwV32+s/o1y2yI2VOaUbkFGqYAjhCTedVWe+WEncUZ5fSUhW/RC6f3tezxmnSFn4uH4Y8lN8KSp8YiMkPERFglI6NvcEKvUaBomoKgduhnUV17tkwLQX2cckuJmXVgL29mR29rid078=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

            var testData = Encoding.UTF8.GetBytes(strText);

            using (var rsa = new RSACryptoServiceProvider(1024))
            {
                try
                {
                    // client encrypting data with public key issued by server                    
                    rsa.FromXmlString(publicKey.ToString());

                    var encryptedData = rsa.Encrypt(testData, true);

                    var base64Encrypted = Convert.ToBase64String(encryptedData);

                    return base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
        public static string Decryption(string strText)
        {
            var privateKey = "<RSAKeyValue><Modulus>pR34oeVqou5ATXQ+hIaIKpbT2yitbS+hQwV32+s/o1y2yI2VOaUbkFGqYAjhCTedVWe+WEncUZ5fSUhW/RC6f3tezxmnSFn4uH4Y8lN8KSp8YiMkPERFglI6NvcEKvUaBomoKgduhnUV17tkwLQX2cckuJmXVgL29mR29rid078=</Modulus><Exponent>AQAB</Exponent><P>4oS260UhMStTAmY0H2TJoxNHizo+iQBHaIRA2IpLgG2KF74yI0WhWwct8G+rs/g0qV1rEDGdFGFKjdJthS0ydQ==</P><Q>uptykejwvbnH633CrYtHf7q2ZyMkancSsCQunCmK3Tc8jFKx3CpUn4vxtlhkZbpPe1FwWA/CXRPjUXF2IFL+4w==</Q><DP>D7H8bq7ajuqelNL6F5bHNep3EZmWja2cRQys5ZQniR2N7Pk7xh/j/0evEjLapdyz4Wxcp8GYTbo0DUGgXxLzpQ==</DP><DQ>Nn1n0iWEDtKuBxzc+RIfmbOqyakgXBpTEa4vEhirXPdhLdSD7TdLKJ+TwFxFTNM/nFHmjUBk0AsbqM+J39x8Yw==</DQ><InverseQ>VQNZHiRBXEphL4sLclLXBJ7gTMRBHxD/6LXecQv85xVWnptQHzI2YFXKEUfBrLGz+Twpq3Y3u22Q6vrL0+EYBA==</InverseQ><D>AmGBQUxekkTfNn6znBOYiQytt15JyeZC7AV7KfelbkSa+h9YhIFF+XnD/CiEni5oHWAmlX1TKPYgp5zcbD++gKwguSCdetRmbwIyUWIqSPprYFR3uZB5uIjD+l19rQqWDTrRSJg66nhpxB+/9qfzSB3Ul0Dr3WdRuMN67g57gek=</D></RSAKeyValue>";

            var testData = Encoding.UTF8.GetBytes(strText);

            using (var rsa = new RSACryptoServiceProvider(1024))
            {
                try
                {
                    var base64Encrypted = strText;

                    // server decrypting data with private key                    
                    rsa.FromXmlString(privateKey);

                    var resultBytes = Convert.FromBase64String(base64Encrypted);
                    var decryptedBytes = rsa.Decrypt(resultBytes, true);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData.ToString();
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
    }
}
