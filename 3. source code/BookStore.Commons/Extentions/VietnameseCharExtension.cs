﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BookStore.Commons.Extentions
{
    public static class VietnameseCharExtension
    {
        public static string RemoveVNChar(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = str.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public static bool CompareVNChar(string source, string compare)
        {
            if (string.IsNullOrEmpty(compare))
            {
                return true;
            }
            return RemoveVNChar(source.Trim().ToLower()).Contains(RemoveVNChar(compare.Trim().ToLower()));
        }
    }
}
