﻿using BookStore.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.Commons.Extentions
{
    public static class PagingExtensions
    {
        public static List<T> Paging<T>(this List<T> source, ref Pagination page)
        {
            page.TotalItems = source.Count();
            return source.Skip((page.CurrentPage - 1) * page.PageSize).Take(page.PageSize).ToList();
        }
    }
}
