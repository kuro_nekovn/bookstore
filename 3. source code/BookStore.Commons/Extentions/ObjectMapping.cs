﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Commons.Extentions
{
    public static class ObjectMapping
    {
        public static TResult Map<T, TResult>(this T objectToMap)
        {
            TResult returnVal = (TResult)Activator.CreateInstance(typeof(TResult));

            var sourseProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var desProperties = typeof(TResult).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo desProperty in desProperties)
            {
                foreach (PropertyInfo sourceProperty in sourseProperties)
                {
                    if (sourceProperty.Name == desProperty.Name)
                    {
                        desProperty.SetValue(returnVal, sourceProperty.GetValue(objectToMap));
                        break;
                    }
                }
            }
            return returnVal;
        }
    }
}
