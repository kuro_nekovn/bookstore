﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace BookStore.Commons.Extentions
{
    /// <summary>
    /// Extend <see cref="object"/> class so that can be convert to Json ActionResult.
    /// </summary>
    public static class JsonExtensions
    {
        /// <summary>
        /// Convert an <see cref="object"/> to <see cref="JObject"/>.
        /// </summary>
        /// <param name="obj">The object which is converted.</param>
        /// <returns>The <see cref="JObject"/> object.</returns>
        public static JObject ToJObject(this object obj)
        {
            JObject result = JObject.FromObject(obj, new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return result;
        }

        /// <summary>
        /// Convert an <see cref="IEnumerable"/> to <see cref="JArray"/>.
        /// </summary>
        /// <param name="obj">The object which is converted.</param>
        /// <returns>The <see cref="JArray"/> object.</returns>
        public static JArray ToJArray(this IEnumerable obj)
        {
            JArray result = JArray.FromObject(obj, new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return result;
        }
    }
}